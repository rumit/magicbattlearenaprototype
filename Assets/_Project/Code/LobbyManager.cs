﻿using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Zenject;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Logger = MagicBattleArena.Logger;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    private string _roomName;
    
    public Action ConnectedToMaster;
    public Action JoinToRoom;
    public Action AllPlayersInRoom;

    [Inject] private DebugLogUI _debugOutput;

    private void OnDestroy()
    {
        JoinToRoom -= OnStartJoinToRoom;
    }

    private void Start()
    {
        
#if DEBUG || UNITY_EDITOR
        _roomName = "Arena_1111";
#else
        _roomName = "Arena_2748";
#endif
        JoinToRoom += OnStartJoinToRoom;
        TryConnect();
    }

    private void TryConnect()
    {
        PhotonNetwork.NickName = $"Player {UnityEngine.Random.Range(1000, 10000)}";
        _debugOutput.Print($"{PhotonNetwork.NickName} connecting...");
        
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "1.0.0";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        ConnectedToMaster?.Invoke();
        _debugOutput.Print($"{PhotonNetwork.NickName} connected");
    }

    private void OnStartJoinToRoom()
    {
        var success = PhotonNetwork.JoinOrCreateRoom(_roomName, new RoomOptions {MaxPlayers = 2}, null);
        _debugOutput.Print($"JoinOrCreateRoom, is success operation: {success}");
    }

    // public override void OnLeftRoom()
    // {
    //     _debugOutput.Print($"OnLeftRoom: {PhotonNetwork.CurrentRoom.Name}");
    // }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        _debugOutput.Print($"OnJoinRandomFailed");
        Debug.Log($"OnJoinRandomFailed");
    }
    
    public override void OnJoinedRoom()
    {
        _debugOutput.Print($"Joined to: {PhotonNetwork.CurrentRoom.Name}, CountOfPlayers: {PhotonNetwork.CurrentRoom.PlayerCount}");
        _debugOutput.Print($"IsConnectedAndReady: {PhotonNetwork.IsConnectedAndReady}");
        StartCoroutine(CheckPlayersInRoom());
    }

    private IEnumerator CheckPlayersInRoom()
    {
        while (PhotonNetwork.CurrentRoom.PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers)
        {
           // Logger.Log($"CountOfPlayers: {PhotonNetwork.CountOfPlayersInRooms}");
            yield return null;
        }
        
        _debugOutput.Print($"Joined to: {PhotonNetwork.CurrentRoom.Name}, CountOfPlayers: {PhotonNetwork.CurrentRoom.PlayerCount}");
        
        AllPlayersInRoom?.Invoke();
        StopCoroutine(CheckPlayersInRoom());
        yield return new WaitForSeconds(1);
        PhotonNetwork.LoadLevel(Scenes.GetSceneName(Scenes.ScenesEnum.BattleScene));
    }
}