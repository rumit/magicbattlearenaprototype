﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class LobbyUI : MonoBehaviour
{
    [SerializeField] private Button _startGameBtn;
    [SerializeField] private TextMeshProUGUI _waitingStateField;

    [Inject] private LobbyManager _lobbyManager;
    
    private void OnDestroy()
    {
        _startGameBtn.onClick.RemoveListener(StartGameHandler);
        _lobbyManager.ConnectedToMaster -= OnConnectedToMaster;
        _lobbyManager.AllPlayersInRoom -= AllPlayersInRoomHandler;
    }
    
    private void Start()
    {
        _startGameBtn.gameObject.SetActive(false);
        _waitingStateField.text = "connecting...";
        
        _lobbyManager.ConnectedToMaster += OnConnectedToMaster;
        _lobbyManager.AllPlayersInRoom += AllPlayersInRoomHandler;
        _startGameBtn.onClick.AddListener(StartGameHandler);
    }

    private void OnConnectedToMaster()
    {
        _waitingStateField.text = "connected to server...";
        _startGameBtn.gameObject.SetActive(true);
    }

    private void AllPlayersInRoomHandler()
    {
        _waitingStateField.text = "connection successful";
    }

    private void StartGameHandler()
    {
        _startGameBtn.gameObject.SetActive(false);
        _waitingStateField.text = "waiting for players...";
        _lobbyManager.JoinToRoom?.Invoke();
    }
}
