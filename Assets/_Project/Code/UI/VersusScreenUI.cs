﻿﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using MagicBattleArena.Network;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class VersusScreenUI : MonoBehaviour
{
    [Inject] private GameState _gameState;
    [Inject] private BattleBaseController _battleBaseController;
    
    [SerializeField] private Image _block;
    [SerializeField] private CanvasGroup _infoBlock;
    [Space(10)]
    [SerializeField] private CharPreview _ownerPreviewRef;
    [SerializeField] private CharPreview _enemyPreviewRef;
    [Space(10)]
    [SerializeField] private Transform _ownerPreviewsContainer;
    [SerializeField] private Transform _enemyPreviewsContainer;

    private Action _onScreenComplete;
    private CommonBattleConfig _battleConfig;

    private void Awake()
    {
        _infoBlock.alpha = 0;
        _gameState.BattleInitialized += OnPlayersInitialized;
        _battleConfig = _battleBaseController.BattleConfig;
    }

    private void OnDestroy()
    {
        _gameState.BattleInitialized -= OnPlayersInitialized;
    }

    private void OnPlayersInitialized(Action callback, LevelFieldView levelField)
    {
        _onScreenComplete = callback;
        FadeIn();
    }

    private async void FadeIn()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(.5f));
        _infoBlock.DOFade(1, 1.5f);

        await UniTask.Delay(TimeSpan.FromSeconds(.5f));
        
        foreach (var player in Player.Players)
        {
            if (player == Player.LocalPlayer)
            {
                for (var i = 0; i < 3; i++)
                {
                    var prev = Instantiate(_ownerPreviewRef, _ownerPreviewsContainer);
                    var ch = player.TeamConfigs[i];
                    prev.Init(ch, _battleConfig.GetHeroPreviewTexture(ch));
                }
            }
            else
            {
                for (var i = 0; i < 3; i++)
                {
                    var prev = Instantiate(_enemyPreviewRef, _enemyPreviewsContainer);
                    var ch = player.TeamConfigs[i];
                    prev.Init(ch, _battleConfig.GetHeroPreviewTexture(ch));
                }
            }
        }

        await UniTask.Delay(TimeSpan.FromSeconds(5f));
        FadeOut();
    }

    private void FadeOut()
    {
        _block.DOFade(0, 1.5f);
        _infoBlock.DOFade(0, 1.5f)
            .OnComplete(() =>
            {
                _ownerPreviewsContainer.DOMoveX(-Screen.width, 1);
                _enemyPreviewsContainer.DOMoveX(Screen.width * 1.5f, 1)
                    .OnComplete(() =>
                    {
                        foreach (var tex in _battleConfig.GetHeroPreviewsTextures())
                            tex.Release();
                        _onScreenComplete?.Invoke();
                    });
            });
    }
}