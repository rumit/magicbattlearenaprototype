﻿using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using MagicBattleArena.Network;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] private CommonBattleConfig _battleConfig;
    [SerializeField] private Image _block;
    [SerializeField] private CanvasGroup _infoBlock;
    [Space(10)] [SerializeField] private CharPreview _ownerPreviewRef;
    [SerializeField] private CharPreview _enemyPreviewRef;
    [Space(10)] [SerializeField] private Transform _winnerPreviewsContainer;
    [Space(10)] [SerializeField] private TextMeshProUGUI _title;
    [Space(10)] [SerializeField] private Button _leaveGameBtn;
    
    private BattleBaseController _battleController;

    private void Awake()
    {
        _infoBlock.alpha = 0;
        _block.color = Color.clear;
        _leaveGameBtn.onClick.AddListener(OnLeaveGameBtnTap);
    }

    private void OnDestroy()
    {
        _leaveGameBtn.onClick.RemoveListener(OnLeaveGameBtnTap);
    }

    public void Init(BattleBaseController battleController, Player winner)
    {
        _battleController = battleController;

        _infoBlock.DOFade(1, 1.5f);
        _block.DOFade(0.7f, 1.5f);
        
        _title.text = winner.IsLocal ? $"<color=#0088FF>Blue Team</color> Wins!" : "<color=#DD2747>Red Team</color> Wins!";
        
        if (winner.IsLocal)
        {
            for (var i = 0; i < 3; i++)
            {
                var prev = Instantiate(_ownerPreviewRef, _winnerPreviewsContainer);
                var ch = winner.TeamConfigs[i];
                prev.Init(ch, _battleConfig.GetHeroPreviewTexture(ch));
            }
        }
        else
        {
            for (var i = 0; i < 3; i++)
            {
                var prev = Instantiate(_enemyPreviewRef, _winnerPreviewsContainer);
                var ch = winner.TeamConfigs[i];
                prev.Init(ch, _battleConfig.GetHeroPreviewTexture(ch));
            }
        }
    }

    private void OnLeaveGameBtnTap() => _battleController.LeaveBattle();
}
