﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharPreview : MonoBehaviour
{
    [SerializeField] private RawImage _image;
    [SerializeField] private TextMeshProUGUI _nameField;

    public void Init(CharConfig config, RenderTexture texture)
    {
        _image.texture = texture;
        _nameField.text = config.Name;
    }
}
