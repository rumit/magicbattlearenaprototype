﻿using System;
using System.Text;
using TMPro;
using TouchScript.Gestures;
using UnityEngine;

public class DebugLogUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _tField;
    private GameObject _outputContainer;
    private TapGesture _hitAreaGesture;
    private string _currentString = "";

    private void Start()
    {
        _outputContainer = _tField.transform.parent.gameObject;
        _hitAreaGesture = GetComponentInChildren<TapGesture>();
        _hitAreaGesture.Tapped += TapHandler;

#if !UNITY_EDITOR
        Hide();
#endif
    }

    private void OnDestroy()
    {
        _hitAreaGesture.Tapped -= TapHandler;
    }

    private void Hide() => _outputContainer.SetActive(false);
    
    private void Show() => _outputContainer.SetActive(true);
    
    private void TapHandler(object sender, EventArgs e)
    {
        if (_outputContainer.activeSelf)
            Hide();
        else Show();
    }

    public void Print(object message)
    {
        var d = DateTime.Now;
        var t = $"{d.Minute:00}:{d.Second:00}:{d.Millisecond:00}";
        _tField.text = _currentString.Length > 0 ? $"{_currentString}\n{t} {message}" : $"{t} {message}";
        _currentString = _tField.text;
    }

    public void Clear()
    {
        _tField.text = _currentString = "";
    }
}
