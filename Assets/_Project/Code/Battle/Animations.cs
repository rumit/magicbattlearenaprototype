﻿public enum BaseAnimations
{
    Idle,
    Walk,
    Damage,
    Fall,
    Die,
}
public enum SpellAnimationsIds
{
    Spell_0,
    Spell_1,
    Spell_2,
    Spell_3,
    Spell_4
}
