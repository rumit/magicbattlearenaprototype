﻿using System;
using DG.Tweening;
using UnityEngine;

public class LavaProcessor : MonoBehaviour
{
    private Material _material;
    

    private void Awake()
    {
        _material = GetComponent<MeshRenderer>().sharedMaterial;
    }

    private void Start()
    {
        // float smoothness = 0;
        // DOTween.To(() => smoothness, x => smoothness = x, 1, 3)
        //     .SetLoops(-1, LoopType.Yoyo)
        //     .SetEase(Ease.InOutSine)
        //     .OnUpdate(() =>
        //     {
        //         _material.SetFloat(ShadersConstants.Smoothness, smoothness);
        //     });

        float parallax = 0.05f;
        DOTween.To(() => parallax, x => parallax = x, 0.15f, 3)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine)
            .OnUpdate(() =>
            {
                _material.SetFloat(ShadersConstants.Parallax, parallax);
            });
    }
}
