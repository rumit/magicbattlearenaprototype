﻿using System;
using UnityEngine;

public class ObjectShadingSetup : MonoBehaviour
{
    [SerializeField] private Color _color;
    [SerializeField] private Color _emissionColor;
    [SerializeField] private Renderer _renderer;
    private void Start()
    {
        _renderer.material.SetColor(ShadersConstants.EmissionColor, _emissionColor);
        _renderer.material.SetColor(ShadersConstants.BaseColor, _color);
    }
}
