﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using Logger = MagicBattleArena.Logger;

namespace LevelField
{
    public class Node : IBuffable
    {
        public enum States
        {
            LiveState,
            CollapsedState,
            CursedState
        }
        
        /// <summary>
        /// Sum of G and H.
        /// </summary>
        public int F => G + H;
        
        /// <summary>
        /// Cost from start tile to this tile.
        /// </summary>
        public int G;
    
        /// <summary>
        /// Estimated cost from this tile to destination tile.
        /// </summary>
        public int H;
        
        /// <summary>
        /// References to all adjacent tiles.
        /// </summary>
        public List<Node> Neighbours = new List<Node>();
        
        public readonly List<IBuff> Buffs = new List<IBuff>();
    
        public FieldTile Tile { get; private set; }
        public readonly Vector3Int Position;
        public readonly bool BelongsPlayer;
        
        public IObstacle Obstacle { get; private set; }
        public bool HasObstacle { get; private set; }
        public SpellActions.TrapsAction.TrapConfig Trap { get; private set; }
        
        public bool IsSelected { get; private set; }
        
        public bool Walkable = true;
        public States State { get; private set; }
        
        private GameObject _crackedView;

        public Node(FieldTile tile, Vector3Int position, bool isLocal)
        {
            Tile = tile;
            Position = position;
            BelongsPlayer = isLocal;
            State = States.LiveState;
        }

        public void AddBuff(IBuff buff)
        {
            if(State == States.CollapsedState)
                return;
            Buffs.Add(buff);
        }
        
        public void RemoveBuff(IBuff buff) => Buffs.Remove(buff);

        public void SetTrap(SpellActions.TrapsAction.TrapConfig trap) => Trap = trap;

        public void RemoveTrap() => Trap = null;

        public void Curse(GameObject view)
        {
            State = States.CursedState;
            _crackedView = view;
        }

        public void Collapse()
        {
            State = States.CollapsedState;
            RemoveObstacle();
            if(_crackedView != null)
                Object.Destroy(_crackedView);
            Walkable = IsSelected = false;
            foreach (var b in Buffs)
                b.End();
            Buffs.Clear();
        }
    
        public void Select(FieldTile tile)
        {
            Tile = tile;
            IsSelected = true;
        }
        
        public void Deselect(FieldTile tile)
        {
            Tile = tile;
            IsSelected = false;
        }
    
        public void SetObstacle<T>(T unit) where T : IObstacle
        {
            Obstacle = unit;
            HasObstacle = true;
        }

        public void RemoveObstacle()
        {
            Obstacle = null;
            HasObstacle = false;
        }
        
        public T GetObstacle<T>() where T : IObstacle => (T)Obstacle;

        public T GetBuff<T>() where T : IBuff => (T) Buffs.FirstOrDefault(b => b is T);

        public override string ToString()
        {
            var obstacle = Obstacle == null ? "null" : Obstacle.name;
            var tile = Tile == null ? "null" : Tile.name;
            return $"[Tile: {tile}, Position: {Position}, Walkable: {Walkable}, Obstacle: {obstacle}," +
                   $" BelongsPlayer: {BelongsPlayer}, State: {State}]";
        }
    }
}

