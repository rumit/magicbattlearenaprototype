﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using LevelField;
using Photon.Pun;
using UnityEngine;
using Zenject;
using Logger = MagicBattleArena.Logger;
using Random = UnityEngine.Random;

public class FieldObjectsEffects : MonoBehaviour, IPunObservable
{
    [Inject] private CommonBattleConfig _battleConfig;
    [Inject] private BattleBaseController _battleBaseController;
    
    private PhotonView _photonView;

    private void Start()
    {
        _photonView = GetComponent<PhotonView>();
    }

    public void SetTrap(Node node, SpellActions.TrapsAction.TrapConfig config)
    {
        node.SetTrap(config);
        var nodeWorldPos = _battleBaseController.LevelField.NavigationMap.GetWorldPositionByNode(node);
        _photonView.RPC(nameof(_SetTrapOnEnemyField), RpcTarget.Others, nodeWorldPos, 
            config.DamageProRound, config.MoveRestrictionDuration, config.BindedSpellConfig.name, config.Spell.GUID);
    }

    public async void CollapseCell(Node node)
    {
        await UniTask.Delay(TimeSpan.FromSeconds(Random.Range(0f, 1f)));
        var pos = _battleBaseController.LevelField.NavigationMap.GetWorldPositionByNode(node);
        _photonView.RPC(nameof(_CollapseCell), RpcTarget.AllViaServer, pos);
    }
    
    public void RemoveTrap(Vector3 pos, string spellGuid)
    {
        _photonView.RPC(nameof(_RemoveTrap), RpcTarget.AllViaServer, pos, spellGuid);
    }
    
    public void ExplodeTrap(Vector3 pos)
    {
        _photonView.RPC(nameof(_ExplodeTrap), RpcTarget.AllViaServer, pos);
    }
    
    public void CurseCell(Node node)
    {
        var pos = _battleBaseController.LevelField.NavigationMap.GetWorldPositionByNode(node);
        _photonView.RPC(nameof(_CurseCell), RpcTarget.AllViaServer, pos);
    }
    
    #region Photon
    [PunRPC]
    private void _SetTrapOnEnemyField(Vector3 nodeWorldPos, int damageProRound, int moveRestrictionDuration, string spellId, string spellGUID)
    {
        var node = _battleBaseController.LevelField.NavigationMap.GetNodeByWorldPosition(nodeWorldPos);
        var spellConfig = _battleBaseController.BattleConfig.GetSpellConfigById(spellId);
        var config = new SpellActions.TrapsAction.TrapConfig
        {
            DamageProRound = damageProRound,
            MoveRestrictionDuration = moveRestrictionDuration,
            BindedSpellConfig = spellConfig,
            Spell = new SpellsPlanningPhase.PlannedSpell
            {
                Config = spellConfig,
                AoeCellsData = null,
                GUID = spellGUID
            }
        };
        node.SetTrap(config);
    }

    [PunRPC]
    private void _RemoveTrap(Vector3 nodeWorldPos, string spellGuid)
    {
        var node = _battleBaseController.LevelField.NavigationMap.GetNodeByWorldPosition(nodeWorldPos);
        var key = SpellsFxController.GetFXViewKey(node.Trap.BindedSpellConfig, spellGuid, nodeWorldPos);
        _battleBaseController.SpellsFxController.RemoveFXView(key);
        node.RemoveTrap();
    }

    [PunRPC]
    private void _ExplodeTrap(Vector3 pos)
    {
        var node = _battleBaseController.LevelField.NavigationMap.GetNodeByWorldPosition(pos);
        var trapBuff = node.GetBuff<TrapBuff>();
        trapBuff?.End();
        var explodeFx = Instantiate(_battleBaseController.BattleConfig.ExplodeTrapFxRef, pos, Quaternion.identity, transform);
        explodeFx.Init();
    }

    [PunRPC]
    private void _CurseCell(Vector3 pos)
    {
        var node = _battleBaseController.LevelField.NavigationMap.GetNodeByWorldPosition(pos);
        var rot = _battleConfig.CrackedCell.TopPart.transform.rotation;
        var crackedCell = Instantiate(_battleConfig.CrackedCell.TopPart, pos, rot, transform);
        //Logger.Log($"#Test cell cursed: {node}");
        node.Curse(crackedCell);
    }

    [PunRPC]
    private void _CollapseCell(Vector3 pos)
    {
        var map = _battleBaseController.LevelField.NavigationMap;
        var node = map.GetNodeByWorldPosition(pos);
        //Logger.Log($"#Test cell collapsed: {node}");
        map.CollapseTile(node);
        //_battleBaseController.DebugOutput.Print($"CollapseCell: {node}");

        var collapsedRef = node.BelongsPlayer
            ? _battleConfig.CrackedCell.CollapsedPlayerCell
            : _battleConfig.CrackedCell.CollapsedEnemyCell;
        var collapsed = Instantiate(collapsedRef, pos, Quaternion.identity, transform);
        collapsed.transform.localRotation = Quaternion.Euler(0, 0, 12);
        
        collapsed.transform.DOLocalRotate(new Vector3(0, 0, Random.Range(0f, 120f)), 1);
        collapsed.transform.DOMoveY(-20, 1)
            .SetEase(Ease.InCubic)
            .OnComplete(() => Destroy(collapsed));
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
    #endregion
}