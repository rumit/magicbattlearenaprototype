﻿using System;
using System.Collections.Generic;
using System.Linq;
using LevelField;
using UnityEngine;
using UnityEngine.Tilemaps;
using Logger = MagicBattleArena.Logger;

public partial class NavigationMap
{
    private readonly Tilemap _tilemap;
    private readonly FieldTilesPalette _fieldTilesPalette;

    public readonly List<LevelField.Node> Nodes;
    public List<LevelField.Node> PlayerNodes { get; }
    public List<LevelField.Node> EnemyNodes { get; }

    public NavigationMap(LevelConfig config, List<LevelField.Node> nodes, Tilemap tilemap)
    {
        Nodes = nodes;
        _tilemap = tilemap;
        _fieldTilesPalette = config.FieldTilesPalette;
        
        foreach (var node in Nodes)
            node.Neighbours = GetNeighbours(node.Position);
        PlayerNodes = Nodes.Where(n => n.BelongsPlayer).ToList();
        EnemyNodes = Nodes.Where(n => !n.BelongsPlayer).ToList();
    }
    
    public Bounds GetCellBounds(Vector3Int nodePosition) => _tilemap.layoutGrid.GetBoundsLocal(nodePosition);

    public List<LevelField.Node> SelectAvailableForMovementCells(int distance, LevelField.Node center)
    {
        var result = new List<LevelField.Node>();
        //Logger.Log($"------------ available cells -------------");
        foreach (var node in PlayerNodes)
        {
            //Logger.Log($"node: {node}");
            if(node.Equals(center) || node.State == Node.States.CollapsedState || node.HasObstacle)
                continue;
            
            var path = Pathfinding.FindPath(center, node);
            if(path.Count <= 0 || path.Count > distance)
                continue;
            
            SelectTile(node);
            if(!result.Contains(node)) result.Add(node);
        }
        //Logger.Log($"----------------------------------");
        return result;
    }

    private void SelectTile(LevelField.Node node)
    {
        if(node.State == Node.States.CollapsedState)
            return;
        
        var tile = node.BelongsPlayer ? _fieldTilesPalette.GetPlayerSelectedTile() : _fieldTilesPalette.GetEnemySelectedTile();
        _tilemap.SetTile(node.Position, tile);
        node.Select(tile);
    }

    public void DeselectPlayerCells()
    {
        foreach (var node in PlayerNodes) DeselectTile(node);
    }
    
    public void DeselectPlayerCell(LevelField.Node node) => DeselectTile(node);
    
    private void DeselectTile(LevelField.Node node)
    {
        if(node.State == Node.States.CollapsedState)
            return;
        var tile = node.BelongsPlayer ? _fieldTilesPalette.GetPlayerDefaultTile() : _fieldTilesPalette.GetEnemyDefaultTile();
        _tilemap.SetTile(node.Position, tile);
        node.Deselect(tile);
    }
    
    public List<Node> GetRandomNodesOnFieldPart(List<Node> nodes, SpellConfig.AllowedNodesStatesForPlacement allowedOn, int count)
    {
        var nodesForSelection = allowedOn == SpellConfig.AllowedNodesStatesForPlacement.AllExcludeCharacters
                ? nodes.Where(n => !(n.Obstacle is CharacterView) && n.State == Node.States.LiveState).ToList()
                : nodes.Where(n => n.State == Node.States.LiveState).ToList();

        var result = nodesForSelection.GetRandomElements(count);
        // var result = new List<Node>();
        //
        // var centerNode = GetCenterPartOfMap(nodesForSelection);
        // var centerNodeNeighbours = centerNode.Neighbours;
        // var centerArea = allowedOn == SpellConfig.AllowedNodesStatesForPlacement.AllExcludeCharacters
        //     ? centerNodeNeighbours.Where(n => !(n.Obstacle is CharacterView) && n.State == Node.States.LiveState).ToList()
        //     : centerNodeNeighbours.Where(n => n.State == Node.States.LiveState).ToList();
        // centerArea.Add(centerNode);
        //
        // var randomCenterNode = centerArea.GetRandomElements(1).First();
        // result.Add(randomCenterNode);
        //
        // if (count == 1)
        //     return result;
        //
        // var edgeNodes = nodesForSelection.Where(n => n != randomCenterNode);
        // edgeNodes = edgeNodes.GetRandomElements(count - 1);
        // result.AddRange(edgeNodes);
        
        // Logger.Log($"=============================");
        // foreach (var node in result)
        // {
        //     Logger.Log($"#Test cell node: {node}");
        // }
        // Logger.Log($"=============================");
        return result;
    }

    public Node GetCenterPartOfMap(List<Node> nodes)
    {
        var worldPositions = new List<Vector3>();
        foreach (var n in nodes)
            worldPositions.Add(GetWorldPositionByNode(n));
        var center = AoeCellData.GetAoeAreaCenter(worldPositions, 0);
        return GetNodeByWorldPosition(center);
    }
    
    public Vector3 GetWorldPositionByNode(LevelField.Node node) => _tilemap.CellToWorld(node.Position);
    
    public Vector3Int WorldToCell(Vector3 worldPosition) => _tilemap.WorldToCell(worldPosition);
    
    public Tile GetTile<T>(Vector3Int cellPosition)where T : Tile  => _tilemap.GetTile<T>(cellPosition);

    public void CollapseTile(Node node)
    {
        if (node.Obstacle is CharacterView hero)
            hero.CellUnderfootCollapsed?.Invoke();
        node.Collapse();
        _tilemap.SetTile(node.Position, null);
    }

    public Vector3 CellToWorld(Vector3Int cellPosition) => _tilemap.CellToWorld(cellPosition);

    public LevelField.Node GetNodeByPosition(Vector3Int position)
    {
        return Nodes.FirstOrDefault(n => n.Position.Equals(position));
    }
    
    public LevelField.Node GetNodeByWorldPosition(Vector3 worldPosition)
    {
        var tilePos = _tilemap.WorldToCell(worldPosition);
        return Nodes.FirstOrDefault(n => n != null && n.Position.x == tilePos.x && n.Position.y == tilePos.y);;
    }

    public List<Node> GetCollapsedCells()
    {
        return Nodes.Where(c => c.State == Node.States.CollapsedState).ToList();
    }

    public List<Node> GetNodesByWorldPositions(List<Vector3> positions)
    {
        var result = new List<Node>();
        foreach (var p in positions)
        {
            var pos = _tilemap.WorldToCell(p);
           var node = Nodes.FirstOrDefault(n => n.Position.Equals(pos));
           if (node != null)
               result.Add(node);
        }
        return result;
    }

    public Node GetNearestFreeNode(Node node)
    {
        return node.Neighbours.FirstOrDefault(n => !(n.Obstacle is CharacterView) && n.State != Node.States.CollapsedState);
    }

    public Vector3 ConvertToCenterCell(Vector3 worldPosition)
    {
        var tilePos = _tilemap.WorldToCell(worldPosition);
        return _tilemap.CellToWorld(tilePos);
    }

    private LevelField.Node GetNodeByTilePosition(Vector3Int tilePos)
    {
        return Nodes.FirstOrDefault(n => n.Position.x == tilePos.x && n.Position.y == tilePos.y);
    }

    public bool IsPositionOccupied(IObstacle unit, Vector3 worldPosition)
    {
        var node = GetNodeByWorldPosition(worldPosition);
        return node.Obstacle is CharacterView && node.Obstacle != unit;
    }
    
    public void SetObstacle<T>(T unit, Vector3 worldPosition) where T : IObstacle
    {
        var node = GetNodeByWorldPosition(worldPosition);
        node.SetObstacle(unit);
    }
    
    public void SetObstacle<T>(T unit, LevelField.Node node) where T : IObstacle
    {
        node.SetObstacle(unit);
    }
    
    public void RemoveObstacle(Vector3 worldPosition)
    {
        var node = GetNodeByWorldPosition(worldPosition);
        node.RemoveObstacle();
    }

    public void HighlightPath(List<LevelField.Node> path)
    {
        foreach (var node in path)
        {
            if(!node.IsSelected) SelectTile(node);
        }
    }

    private List<LevelField.Node> GetNeighbours(Vector3Int position)
    {
        var result = new List<LevelField.Node>();
        
        var top = GetNodeByTilePosition(new Vector3Int(position.x + 1, position.y, 0));
        if(top != null) result.Add(top);

        var rightTop = position.y % 2 != 0
            ? GetNodeByTilePosition(new Vector3Int(position.x + 1, position.y + 1, 0))
            : GetNodeByTilePosition(new Vector3Int(position.x, position.y + 1, 0));
        if(rightTop != null) result.Add(rightTop);

        var rightBottom = position.y % 2 != 0
            ? GetNodeByTilePosition(new Vector3Int(position.x, position.y + 1, 0))
            : GetNodeByTilePosition(new Vector3Int(position.x - 1, position.y + 1, 0));
        if(rightBottom != null) result.Add(rightBottom);
        
        var bottom = GetNodeByTilePosition(new Vector3Int(position.x - 1, position.y, 0));
        if(bottom != null) result.Add(bottom);

        var leftBottom = position.y % 2 != 0
            ? GetNodeByTilePosition(new Vector3Int(position.x, position.y - 1, 0))
            : GetNodeByTilePosition(new Vector3Int(position.x - 1, position.y - 1, 0));
        if(leftBottom != null) result.Add(leftBottom);

        var leftTop = position.y % 2 != 0
            ? GetNodeByTilePosition(new Vector3Int(position.x + 1, position.y - 1, 0))
            : GetNodeByTilePosition(new Vector3Int(position.x, position.y - 1, 0));
        if(leftTop != null) result.Add(leftTop);
        
        return result;
    }
}