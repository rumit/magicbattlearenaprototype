﻿using System;
using UnityEditor;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

[ExecuteInEditMode]
public abstract class AbstractSpawnPoint : MonoBehaviour
{
    [SerializeField] protected CharConfig _config;

    [Space(5)]
    [HideInInspector][SerializeField] protected LevelConfig _levelConfig;
    
    protected Rect _fieldSize;
    protected CharacterView _char;

    public CharConfig GetConfig => _config;

#if UNITY_EDITOR
    private void Awake()
    {
        if (_config != null && transform.childCount > 0)
        {
            _char = transform.GetChild(0).gameObject.GetComponent<CharacterView>();
            _char.gameObject.hideFlags = HideFlags.HideInHierarchy;
        }
    }

    protected void Update()
    {
        if(Application.isPlaying)
            return;
        
        //Debug.Log($"char {_char}");
        if (_fieldSize.Equals(default))
        {
            var halfW = (float)_levelConfig.BattleFieldSize.X / 2;
            var halfH = (float)_levelConfig.BattleFieldSize.Y / 2;
            _fieldSize = new Rect(new Vector2(-halfW, -halfH), 
                new Vector2(_levelConfig.BattleFieldSize.X, _levelConfig.BattleFieldSize.Y));
        }
        
        CheckPosition();

        if (_config == null)
        {
            if (_char != null)
                DestroyChar();
            return;
        }

        if (_char == null)
        {
            if (!CheckConfig())
                return;
            InstantiateChar();
        }
        else
        {
            if (!CheckConfig())
                return;
            
            if (_config != null && _config.name != _char.Config.name)
            {
                DestroyChar();
                InstantiateChar();
            }
        }
    }

    protected abstract bool CheckConfig();

    protected virtual Vector3 CheckPosition()
    {
        var currentPos = transform.localPosition;
        
        if (currentPos.y > 0 || currentPos.y < 0 )
            currentPos = new Vector3(currentPos.x, 0, currentPos.z);
        
        var absY = Mathf.Abs(_fieldSize.y * _levelConfig.BattleFieldSize.HeightReductCoeff);
        if (currentPos.z > absY)
            currentPos = new Vector3(currentPos.x, 0, absY);
        
        if (currentPos.z < _fieldSize.y)
            currentPos = new Vector3(currentPos.x, 0, _fieldSize.y);
        
        return currentPos;
    }

    protected virtual void InstantiateChar()
    {
        _char = Instantiate(_config.View, transform);
        //_char.gameObject.hideFlags = HideFlags.HideInHierarchy;
    }

    protected virtual void DestroyChar()
    {
        DestroyImmediate(_char.gameObject);
        _char = null;
    }
#endif
}
