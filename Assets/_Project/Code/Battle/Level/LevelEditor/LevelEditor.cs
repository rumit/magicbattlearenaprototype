﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditor : MonoBehaviour
{
    [SerializeField] private LevelConfig _config;
    [SerializeField] private GameObject _spawnPointsContainer;
    //[SerializeField] private BoxCollider _hitZone;

    public Dictionary<CharConfig, Vector3> GetCharsPositions()
    {
        var points = GetComponentsInChildren<AbstractSpawnPoint>();
        var result = new Dictionary<CharConfig, Vector3>(6);
        foreach (var p in points)
        {
            result.Add(p.GetConfig, p.transform.localPosition);
        }

        _spawnPointsContainer.SetActive(false);
        return result;
    }

    public void OnDrawGizmos()
    {
        DrawFieldRect();
    }

    private void DrawFieldRect()
    {
        var rect = _config.BattleFieldSize;
        Gizmos.color = Color.magenta;
         var halfWidth = (float)rect.X / 2;
         var halfHeight = (float)rect.Y / 2;
         var p0 = new Vector3(-halfWidth, 0, -halfHeight);
         var p1 = new Vector3(halfWidth, 0, -halfHeight);
         var p2 = new Vector3(halfWidth, 0, halfHeight * rect.HeightReductCoeff);
         var p3 = new Vector3(-halfWidth, 0, halfHeight * rect.HeightReductCoeff);
         Gizmos.DrawLine(p0, p1);
         Gizmos.DrawLine(p1, p2);
         Gizmos.DrawLine(p0, p3);
         Gizmos.DrawLine(p2, p3);
        
        //_hitZone.size = new Vector3(rect.X, _hitZone.size.y, rect.Y);
    }
}
