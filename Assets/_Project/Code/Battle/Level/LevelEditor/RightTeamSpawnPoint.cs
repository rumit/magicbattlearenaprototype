﻿using UnityEngine;

public class RightTeamSpawnPoint : AbstractSpawnPoint
{
#if UNITY_EDITOR
    protected override void InstantiateChar()
    {
        base.InstantiateChar();
        
        _char.transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));
        
        if(!_levelConfig.RightTeam.Contains(_config))
            _levelConfig.RightTeam.Add(_config);
    }

    protected override void DestroyChar()
    {
        if (_levelConfig.RightTeam.Contains(_char.Config))
            _levelConfig.RightTeam.Remove(_char.Config);
        
        base.DestroyChar();
    }

    protected override bool CheckConfig()
    {
        var res = !_levelConfig.RightTeam.Contains(_config);
        if (!res && _char != null) _config = _char.Config;
        return res;
    }

    protected override Vector3 CheckPosition()
    {
        var currentPos = base.CheckPosition();
        
        if (currentPos.x < 0)
            currentPos = transform.localPosition = new Vector3(0, 0, currentPos.z);

        if (currentPos.x > Mathf.Abs(_fieldSize.x))
            currentPos = new Vector3(Mathf.Abs(_fieldSize.x), 0, currentPos.z);

        transform.localPosition = currentPos;
        return currentPos;
    }
#endif
}