﻿using UnityEngine;

public class LeftTeamSpawnPoint : AbstractSpawnPoint
{
#if UNITY_EDITOR
    protected override void InstantiateChar()
    {
        base.InstantiateChar();
        
        _char.transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
        
        if(!_levelConfig.LeftTeam.Contains(_config))
            _levelConfig.LeftTeam.Add(_config);
    }

    protected override void DestroyChar()
    {
        if (_levelConfig.LeftTeam.Contains(_char.Config))
            _levelConfig.LeftTeam.Remove(_char.Config);
        
        base.DestroyChar();
    }

    protected override bool CheckConfig()
    {
        var res = !_levelConfig.LeftTeam.Contains(_config);
        if (!res && _char != null) _config = _char.Config;
        return res;
    }

    protected override Vector3 CheckPosition()
    {
        var currentPos = base.CheckPosition();
        
        if (currentPos.x > 0)
            currentPos = transform.localPosition = new Vector3(0, 0, currentPos.z);

        if (currentPos.x < _fieldSize.x)
            currentPos = new Vector3(_fieldSize.x, 0, currentPos.z);

        transform.localPosition = currentPos;
        return currentPos;
    }
#endif
}
