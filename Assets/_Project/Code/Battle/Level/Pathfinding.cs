﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class Pathfinding
{
    /// <summary>
    /// Finds path from given start point to end point. Returns an empty list if the path couldn't be found.
    /// </summary>
    /// <param name="startPoint">Start tile.</param>
    /// <param name="endPoint">Destination tile.</param>
    public static List<LevelField.Node> FindPath(LevelField.Node startPoint, LevelField.Node endPoint)
    {
        var finalPathTiles = new List<LevelField.Node>();

        if (startPoint.Equals(endPoint))
            return finalPathTiles;
        
        var openPathTiles = new List<LevelField.Node>();
        var closedPathTiles = new List<LevelField.Node>();

        // Prepare the start tile.
        var currentTile = startPoint;

        currentTile.G = 0;
        currentTile.H = GetEstimatedPathCost(startPoint.Position, endPoint.Position);

        // Add the start tile to the open list.
        openPathTiles.Add(currentTile);

        while (openPathTiles.Count != 0)
        {
            // Sorting the open list to get the tile with the lowest F.
            openPathTiles = openPathTiles.OrderBy(x => x.F).ThenByDescending(x => x.G).ToList();
            currentTile = openPathTiles[0];

            // Removing the current tile from the open list and adding it to the closed list.
            openPathTiles.Remove(currentTile);
            closedPathTiles.Add(currentTile);

            var g = currentTile.G + 1;

            // If there is a target tile in the closed list, we have found a path.
            if (closedPathTiles.Contains(endPoint))
            {
                break;
            }

            // Investigating each adjacent tile of the current tile.
            foreach (var adjacentTile in currentTile.Neighbours)
            {
                // Ignore not walkable adjacent tiles.
                if (!adjacentTile.Walkable)
                {
                    continue;
                }

                // Ignore the tile if it's already in the closed list.
                if (closedPathTiles.Contains(adjacentTile))
                {
                    continue;
                }
                //Logger.Log($"{node}");
                // If it's not in the open list - add it and compute G and H.
                if (!openPathTiles.Contains(adjacentTile))
                {
                    adjacentTile.G = g;
                    adjacentTile.H = GetEstimatedPathCost(adjacentTile.Position, endPoint.Position);
                    openPathTiles.Add(adjacentTile);
                }
                // Otherwise check if using current G we can get a lower value of F, if so update it's value.
                else if (adjacentTile.F > g + adjacentTile.H)
                {
                    adjacentTile.G = g;
                }
            }
        }
        
        // Backtracking - setting the final path.
        if (closedPathTiles.Contains(endPoint))
        {
            currentTile = endPoint;
            finalPathTiles.Add(currentTile);

            for (var i = endPoint.G - 1; i >= 0; i--)
            {
                currentTile = closedPathTiles.Find(x => x.G == i && currentTile.Neighbours.Contains(x));
                finalPathTiles.Add(currentTile);
            }

            finalPathTiles.Reverse();
            finalPathTiles.RemoveAt(0);
        }

        return finalPathTiles;
    }

    /// <summary>
    /// Returns estimated path cost from given start position to target position of hex tile using Manhattan distance.
    /// </summary>
    /// <param name="startPosition">Start position.</param>
    /// <param name="targetPosition">Destination position.</param>
    private static int GetEstimatedPathCost(Vector3Int startPosition, Vector3Int targetPosition)
    {
        //return Mathf.Max(Mathf.Abs(startPosition.z - targetPosition.z), Mathf.Max(Mathf.Abs(startPosition.x - targetPosition.x), Mathf.Abs(startPosition.y - targetPosition.y)));
        return Mathf.Max(Mathf.Max(Mathf.Abs(startPosition.x - targetPosition.x), Mathf.Abs(startPosition.y - targetPosition.y)));
    }
}
