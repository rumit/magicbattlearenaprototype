﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

public class Pin : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _renderer;

    private void Awake()
    {
        transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

    public void Show(PinData pin)
    {
        _renderer.color = pin.Color;
        _renderer.sprite = pin.Icon;

        transform.DOScale(1f, 0.6f)
            .SetEase(Ease.OutElastic)
            .OnComplete(() =>
            {
                UniTask.Delay(TimeSpan.FromSeconds(3))
                    .ContinueWith(() =>
                    {
                        transform.DOScale(0, 0.4f);
                    });
            });
    }

    public struct PinData
    {
        public Color Color;
        public Sprite Icon;
        public override string ToString()
        {
            return $"[Color: {Color}, Icon: {Icon}]";
        }
    }
}
