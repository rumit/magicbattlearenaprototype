﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using Cysharp.Threading.Tasks;
using MagicBattleArena.Network;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.Tilemaps;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class LevelFieldView : MonoBehaviour
{
    public Action<Pin.PinData> PinSelected;
    public Action<LevelField.Node, NavigationMap> FieldTapped;

    [SerializeField] private LevelConfig _config;

    [SerializeField] private TapGesture _tapGesture;
    [SerializeField] private TransformGesture _transformGesture;
    
    public BattleBaseController BattleController { get; private set; }
    public Camera UnitsCamera { get; private set; }

    public LevelConfig Config => _config;
    public NavigationMap NavigationMap { get; private set; }
    public TransformGesture TransformGesture => _transformGesture;

    private Pin.PinData _selectedPin;
    private Tilemap _tilemap;

    private void Awake()
    {
        _tilemap = GetComponentInChildren<Tilemap>();
        _tilemap.transform.parent.position = new Vector3(1000, 0, 0);
        
        _tapGesture.Tapped += OnHitZoneTapped;
        PinSelected += OnPinSelected;
    }

    private void OnDestroy()
    {
        _tapGesture.Tapped -= OnHitZoneTapped;
        PinSelected -= OnPinSelected;
    }

    private void OnPinSelected(Pin.PinData pin)
    {
        _selectedPin = pin;
    }

    public void Init(BattleBaseController battleController, Camera unitsCamera)
    {
        BattleController = battleController;
        UnitsCamera = unitsCamera;
        
        _tilemap.transform.parent.position = Vector3.zero;
        var nodes = new List<LevelField.Node>();
        var mapTransform = _tilemap.transform;
        
        var tilesPosition = new List<Vector3Int>();
        for (var i = 0; i < mapTransform.childCount; i++)
        {
            var t = mapTransform.GetChild(i);
            var tilePos = _tilemap.WorldToCell(t.position);
            tilesPosition.Add(tilePos);
        }
        
        var leftSideBelongToPlayer = Player.LocalPlayer.IsMasterClient;
        var playerTileRef = _config.FieldTilesPalette.GetPlayerDefaultTile();
        var enemyTileRef = _config.FieldTilesPalette.GetEnemyDefaultTile();

        foreach (var tilePos in tilesPosition)
        {
            var tile = _tilemap.GetTile<FieldTile>(tilePos);
            var belongsToPlayer = true;
            if (leftSideBelongToPlayer)
            {
                if (tilePos.y < 0 && !tile.IsPlayerTile)
                {
                    _tilemap.SetTile(tilePos, playerTileRef);
                    tile = _tilemap.GetTile<FieldTile>(tilePos);
                }
                
                if (tilePos.y > 0 && tile.IsPlayerTile)
                {
                    _tilemap.SetTile(tilePos, enemyTileRef);
                    tile = _tilemap.GetTile<FieldTile>(tilePos);
                    belongsToPlayer = false;
                }
            }
            else
            {
                if (tilePos.y > 0 && !tile.IsPlayerTile)
                {
                    _tilemap.SetTile(tilePos, playerTileRef);
                    tile = _tilemap.GetTile<FieldTile>(tilePos);
                }
                    
                
                if (tilePos.y < 0)
                {
                    if (tile.IsPlayerTile)
                    {
                        _tilemap.SetTile(tilePos, enemyTileRef);
                        tile = _tilemap.GetTile<FieldTile>(tilePos);
                        belongsToPlayer = false;
                    }
                    else
                        belongsToPlayer = false;
                }
            }

            var node = new LevelField.Node(tile, tilePos, belongsToPlayer);
            nodes.Add(node);
        }

        NavigationMap = new NavigationMap(_config, nodes, _tilemap);
    }

    private void OnHitZoneTapped(object sender, EventArgs e)
    {
        var hitData = _tapGesture.GetScreenPositionHitData();
        var node = NavigationMap.GetNodeByWorldPosition(hitData.Point);
        if(node == null)
            return;

        FieldTapped?.Invoke(node, NavigationMap);
        if (_selectedPin.Icon == null)
            return;
        
        var pinRef = BattleController.BattleConfig.PinRef;
        var pin = Instantiate(pinRef, NavigationMap.GetWorldPositionByNode(node), Quaternion.identity);
        pin.Show(_selectedPin);
    }
}
