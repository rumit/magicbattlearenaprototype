﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;
using Player = MagicBattleArena.Network.Player;

public class BattleInitializer : MonoBehaviour
{
    [Inject] private DebugLogUI _debugOutput;
    [Inject] private BattleBaseController _battleBaseController;

    [SerializeField] private LevelFieldView _levelFieldRef;
    [SerializeField] private GameObject _charactersCameras;
    
    [Space(10)]
    [SerializeField] private GameObject _versusScreenUI;
    [SerializeField] private GameObject _battleUI;
    [SerializeField] private Camera _unitsCamera;

    public LevelFieldView LevelField { get; private set; }

    private void Awake()
    {
        _battleUI.SetActive(false);
        _versusScreenUI.SetActive(true);
        
        var level = FindObjectOfType<LevelEditor>();
        if (level != null)
            Destroy(level.gameObject);
    }

    private async void Start()
    {
        _debugOutput.Clear();
        
        InitializePlayers();
        await InitLevelField();
        CreateCharacters();
        await UniTask.WaitUntil(() => Player.LocalPlayer.Team.Count == 3 && Player.EnemyPlayer.Team.Count == 3);

        _battleBaseController.GameState.BattleInitialized?.Invoke(() =>
        {
            ToggleHeroCameras(false);
            InitializeBattleUI();
        }, LevelField);
    }

    public void ToggleHeroCameras(bool value) => _charactersCameras.SetActive(value);

    private async UniTask<bool> InitLevelField()
    {
        LevelField = Instantiate(_levelFieldRef);
        await UniTask.DelayFrame(1);
        LevelField.Init(_battleBaseController, _unitsCamera);
        await UniTask.DelayFrame(1);
        return true;
    }

    private void InitializePlayers()
    {
        var players = PhotonNetwork.CurrentRoom.Players.Values.ToList();
        Player.Players.Clear();
        foreach (var p in players)
        {
            var player = new Player
            {
                TeamConfigs = p.IsMasterClient ? _levelFieldRef.Config.LeftTeam : _levelFieldRef.Config.RightTeam,
                PhotonPlayer = p
            };

            Player.Players.Add(player);
        }
    }

    private void CreateCharacters()
    {
        var levelEditor = LevelField.GetComponent<LevelEditor>();
        var positionsHash = levelEditor.GetCharsPositions();
        Destroy(levelEditor);
        
        foreach (var kvp in positionsHash)
        {
            var position = kvp.Value;
            //Logger.Log($"char {kvp.Key}, pos: {kvp.Value}");
            var config = kvp.Key;
            if(!Player.LocalPlayer.TeamConfigs.Contains(config))
                continue;
            
            //_debugOutput.Print(config.name);
            var go = PhotonNetwork.Instantiate(config.View.name, position, Quaternion.identity);
            var charView = go.GetComponent<CharacterView>();
            charView.Init(LevelField, position, _unitsCamera);
            if(charView.PhotonView.IsMine)
                Player.LocalPlayer.AddTeamMember(charView);
        }
    }
    
    private void InitializeBattleUI()
    {
        Destroy(_versusScreenUI);
        _battleUI.SetActive(true);
        _battleBaseController.Init(this, _battleUI.GetComponent<BattleBaseUI>());
    }
}
