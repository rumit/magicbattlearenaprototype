﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class RoundPhasesPanel : MonoBehaviour
{
    [Inject] private BattleBaseController _battleBaseController;
    [Inject] private CommonBattleConfig _battleConfig;
    
    [SerializeField] private SerializedDictionary<GameState.RoundPhases, Image> _iconsHash;
    [SerializeField] private TextMeshProUGUI _phaseNameField;
    [SerializeField] private TextMeshProUGUI _timerField;
    [SerializeField] private TextMeshProUGUI _roundNumberField;
    
    private List<Image> _icons;

    private void OnDestroy()
    {
        _battleBaseController.GameState.RoundPhaseChanged -= OnRoundPhaseChanged;
        _battleBaseController.GameState.RoundChanged -= OnRoundChanged;
    }

    private void Awake()
    {
        _icons = _iconsHash.Values.ToList();
        _battleBaseController.GameState.RoundPhaseChanged += OnRoundPhaseChanged;
        _battleBaseController.GameState.RoundChanged += OnRoundChanged;
    }

    private void OnRoundPhaseChanged(AbstractRoundPhase<IRoundPhase> prevPhase, AbstractRoundPhase<IRoundPhase> newPhase)
    {
        if (prevPhase is PlannableRoundPhase prev)
            prev.TickedUp -= OnPhaseTicked;

        if (newPhase is PlannableRoundPhase next)
        {
            next.TickedUp += OnPhaseTicked;
            var phaseDuration = _battleConfig.RoundPhasesDuration[newPhase.Type];
            _timerField.text = phaseDuration.ToString();
        }
        else
        {
            _timerField.text = "";
        }

        _phaseNameField.text = _battleConfig.RoundPhasesNames[newPhase.Type];
        
        var phaseIndex = (int) newPhase.Type;
        for (var i = 0; i < _icons.Count; i++)
        {
            var icon = _icons[i];
            icon.color = i <= phaseIndex ? Color.white : new Color(1, 1, 1, .2f);
        }
    }
    
    private void OnRoundChanged()
    {
        _roundNumberField.text = $"Round #{_battleBaseController.GameState.CurrentRoundNumber}";
    }

    private void OnPhaseTicked(int remainingTime)
    {
        _timerField.text = remainingTime.ToString();
    }
}