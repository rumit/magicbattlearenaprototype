﻿using System;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class PinsPanel : MonoBehaviour
{
    public enum Pins
    {
        Attack,
        Healing,
        Alarm
    }
    
    [Inject] private BattleBaseController _battleBaseController;
    [SerializeField] private SerializedDictionary<Pins, TapGesture> _iconsHash;
    
    private Image _selected;
    
    private void OnDestroy()
    {
        foreach (var tap in _iconsHash.Values)
        {
            tap.Tapped -= OnIconTapped;
        }
        _battleBaseController.SelectCharacter -= OnCharactedSelected;
    }

    private void Awake()
    {
        foreach (var tap in _iconsHash.Values)
        {
            tap.Tapped += OnIconTapped;
        }
        _battleBaseController.SelectCharacter += OnCharactedSelected;
    }

    private void Select(Image icon)
    {
        if (_selected != null)
            Deselect();
        
        Logger.Log($"selected Hero: {_battleBaseController.SelectedCharacter.Config.name}");
        icon.color = _battleBaseController.SelectedCharacter.Config.ColorId;
        _selected = icon;
        _battleBaseController.LevelField.PinSelected?.Invoke(new Pin.PinData{Color = icon.color, Icon = icon.sprite});
    }

    private void Deselect()
    {
        _selected.color = Color.white;
        _battleBaseController.LevelField.PinSelected?.Invoke(default);
        _selected = null;
    }

    private void OnIconTapped(object sender, EventArgs e)
    {
        var gest = (TapGesture) sender;
        var icon = gest.GetComponent<Image>();
        if (_selected == icon)
        {
            Deselect();
            return;
        }
        Select(icon);
    }
    
    private void OnCharactedSelected(CharacterView hero)
    {
        if (_selected != null)
            Deselect();
    }
}
