﻿using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using MagicBattleArena.Network;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class BattleBaseUI : MonoBehaviour
{
    public Action<bool, SpellConfig> ToggleSpellCards;
    public Action<bool> ToggleCompleteActionConfirmBtn;
    
    [Inject] private BattleBaseController _battleBaseController;
    
    [SerializeField] private TeamInfoPanel _leftTeamInfoPanel;
    [SerializeField] private TeamInfoPanel _rightTeamInfoPanel;
    [SerializeField] private Button _completeActionConfirmBtn;
    [SerializeField] private Button _leaveBattleBtn;
    
    public bool IsInited { get; private set; }

    public SpellsPanel SpellsPanel { get; private set; }

    private void OnDestroy()
    {
        _completeActionConfirmBtn.onClick.RemoveListener(OnCompleteActionConfirmBtnTap);
        _leaveBattleBtn.onClick.RemoveListener(OnLeaveBattleBtn);

        _battleBaseController.SelectCharacter -= OnCharacterSelected;
        _battleBaseController.NextCharacterSelected -= OnNextCharacterSelected;
        _battleBaseController.GameState.RoundPhaseChanged -= OnRoundPhaseChanged;
        
        ToggleCompleteActionConfirmBtn -= OnToggleCompleteActionConfirmBtn;
        ToggleSpellCards -= OnToggleSpellCards;
    }

    private void Awake()
    {
        SpellsPanel = GetComponent<SpellsPanel>();
    }

    public void Init()
    {
        SpellsPanel.Init();
        
        _completeActionConfirmBtn.onClick.AddListener(OnCompleteActionConfirmBtnTap);
        _leaveBattleBtn.onClick.AddListener(OnLeaveBattleBtn);

        var userTeam = Player.LocalPlayer.Team;
        if (userTeam.First().Model.Position.x < 0)
        {
            _leftTeamInfoPanel.Init(true);
            _rightTeamInfoPanel.Init(false);
        }
        else
        {
            _leftTeamInfoPanel.Init(false);
            _rightTeamInfoPanel.Init(true);
        }
        
        ToggleCompleteActionConfirmBtn += OnToggleCompleteActionConfirmBtn;
        _battleBaseController.NextCharacterSelected += OnNextCharacterSelected;
        _battleBaseController.SelectCharacter += OnCharacterSelected;
        _battleBaseController.GameState.RoundPhaseChanged += OnRoundPhaseChanged;

        IsInited = true;
    }

    private void OnToggleCompleteActionConfirmBtn(bool value) => _completeActionConfirmBtn.interactable = value;

    private void OnCharacterSelected(CharacterView character)
    {
        SpellsPanel.ShowCards(character);
    }

    private void OnRoundPhaseChanged(AbstractRoundPhase<IRoundPhase> prevPhase, AbstractRoundPhase<IRoundPhase> newPhase)
    {
        _completeActionConfirmBtn.interactable = true;

        if (newPhase.Type == GameState.RoundPhases.SpellsPhase)
            ToggleSpellCards += OnToggleSpellCards;
        else
            ToggleSpellCards -= OnToggleSpellCards;
        
        if(newPhase.Type == GameState.RoundPhases.BattleSimulationPhase)
            _completeActionConfirmBtn.interactable = false;
    }

    private void OnToggleSpellCards(bool value, SpellConfig spellConfig)
    {
        if(!value)_completeActionConfirmBtn.interactable = false;
    }

    private void OnNextCharacterSelected()
    {
        _completeActionConfirmBtn.interactable = true;
    }
    
    private void OnCompleteActionConfirmBtnTap()
    {
        _battleBaseController.CurrentCharacterPlanningComplete?.Invoke(value =>
        {
            if (value) _completeActionConfirmBtn.interactable = false;
        });
    }
    
    private void OnLeaveBattleBtn() => _battleBaseController.LeaveBattle();
}