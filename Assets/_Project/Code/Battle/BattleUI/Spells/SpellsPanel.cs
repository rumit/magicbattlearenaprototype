﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class SpellsPanel : MonoBehaviour
{
    [Inject] private BattleBaseController _battleController;
    [SerializeField] private List<SpellCard> _cards;
    [SerializeField] private SpellInfoPanel _spellInfoPanel;
    private GameState _gameState;

    private void OnDestroy()
    {
        if(_battleController.BattleBaseUI == null) //Hack
            return;
        
        _battleController.BattleBaseUI.ToggleSpellCards -= OnToggleSpellCards;
        _battleController.SelectCharacter -= OnCharacterSelected;
        _battleController.GameState.RoundPhaseChanged -= OnRoundPhaseChanged;
        _battleController.BattleSimulator.SpellApplied -= OnSpellApplied;
    }

    private void Start()
    {
        var ultCard = _cards.Last();
        ultCard.gameObject.SetActive(false);
    }

    public void Init()
    {
        _gameState = _battleController.GameState;
        _battleController.SelectCharacter += OnCharacterSelected;
        _battleController.BattleBaseUI.ToggleSpellCards += OnToggleSpellCards;
        _battleController.GameState.RoundPhaseChanged += OnRoundPhaseChanged;
        _battleController.BattleSimulator.SpellApplied += OnSpellApplied;
        
        ShowCards(_battleController.SelectedCharacter);
        DisableCards();
    }

    public void HighlightCard(SpellConfig config)
    {
        SpellCard.DeselectCurrent();
        var card = _cards.FirstOrDefault(c => c.Config == config);
        card.Highlight();
    }

    private void OnRoundPhaseChanged(AbstractRoundPhase<IRoundPhase> prevPhase, AbstractRoundPhase<IRoundPhase> newPhase)
    {
        if (_gameState.CurrentRoundNumber == _battleController.BattleConfig.RoundWhenUltSpellsBegin)
        {
            var spell = _battleController.SelectedCharacter.Config.Spells
                .OrderByDescending(s => s.ExecuteType == SpellConfig.ExecuteTypes.Light)
                .Last();
            var ultCard = _cards.Last();
            ultCard.gameObject.SetActive(true);
            ultCard.Init(spell, _spellInfoPanel);
        }
            
        //Logger.Log($"OnRoundPhaseChanged spells panel: prevPhase: {prevPhase}, newPhase: {newPhase}");
        switch (newPhase.Type)
        {
            case GameState.RoundPhases.MovementPhase:
                DisableCards();
                break;
            case GameState.RoundPhases.SpellsPhase:
                EnableCards();
                break;
        }
    }

    private void DisableCards()
    {
        foreach (var c in _cards)
        {
            if(!c.gameObject.activeSelf)
                continue;
            c.Disable();
        }
    }
    
    private void DisableCards(SpellConfig except)
    {
        foreach (var c in _cards)
        {
            if(c.Config == except)
                c.DisableAndHighlight();
            else
                c.Disable();
        }
    }
    
    private void EnableCards()
    {
        foreach (var c in _cards)
        {
            if(!c.gameObject.activeSelf)
                continue;
            if(!c.CheckCooldownState())
                c.Enable();
        }
    }

    public void ShowCards(CharacterView character)
    {
        var spells = character.Config.Spells
            .OrderByDescending(s => s.ExecuteType == SpellConfig.ExecuteTypes.Light).ToList();
        
        for (var i = 0; i < _cards.Count; i++)
        {
            var card = _cards[i];
            if(!card.gameObject.activeSelf)
                continue;
            card.Init(spells[i], _spellInfoPanel);
        }
    }

    // private bool CollapsedCellsOnFieldExists(SpellConfig config)
    // {
    //     if (config.ExecuteType == SpellConfig.ExecuteTypes.Ultra &&
    //         config.AOESettings.AllowedPlacementOn == SpellConfig.AllowedNodesStatesForPlacement.Collapsed)
    //     {
    //         return _battleController.LevelField.NavigationMap.GetCollapsedCells().Count >= 0;
    //     }
    //     return false;
    // }

    private void OnCharacterSelected(CharacterView character)
    {
        SpellCard.DeselectCurrent();
    }
    
    private void OnToggleSpellCards(bool value, SpellConfig spellConfig)
    {
        SpellCard.DeselectCurrent();
        if(value)
        {
            EnableCards();
        }
        else
        {
            DisableCards(spellConfig);
        }
    }
    
    private void OnSpellApplied(SpellConfig config)
    {
        var card = _cards.FirstOrDefault(c => c.Config == config);
        if(card != null)
            card.StartCooldownState();
    }
}