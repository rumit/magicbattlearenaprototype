﻿using System;
using System.Collections.Generic;
using Coffee.UIEffects;
using TMPro;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static SpellConfig;
using Logger = MagicBattleArena.Logger;

public class SpellCard : MonoBehaviour
{
    #region Static
    private static SpellCard _selected;
    private static SpellCard _highlighted;
    
    public static void DeselectCurrent()
    {
        if (_selected != null)
            _selected.Deselect();
    }
    #endregion
    
    
    [Inject] private BattleBaseController _battleController;
    
    [SerializeField] private Image _iconHolder;
    [SerializeField] private Image _cooldownDisplay;
    [SerializeField] private Image _border;
    [SerializeField] private Color _selectionColor;
    [SerializeField] private TextMeshProUGUI _cooldownValueField;
    
    [SerializeField] private TextMeshProUGUI _debugTypeField;

    public SpellConfig Config { get; private set; }
    
    private Dictionary<SpellConfig, CooldownController> CooldownControlersMap = new Dictionary<SpellConfig, CooldownController>();
    private UIEffect _uiEffect;
    private PressGesture _pressGesture;
    private LongPressGesture _longPressGesture;
    private SpellInfoPanel _spellInfoPanel;
    private ReleaseGesture _releaseGesture;
    private bool _disabled;

    private void OnDestroy()
    {
        _pressGesture.Pressed -= OnPress;
        _longPressGesture.LongPressed -= OnLongPressed;
        _releaseGesture.Released -= OnReleased;
        
        _selected = _highlighted = null;

        foreach (var kvp in CooldownControlersMap)
            kvp.Value?.Dispose();
    }

    private void Awake()
    {
        _cooldownValueField.gameObject.SetActive(false);

        _uiEffect = GetComponentInChildren<UIEffect>();
        
        _pressGesture = GetComponent<PressGesture>();
        _pressGesture.Pressed += OnPress;
        
        _releaseGesture = GetComponent<ReleaseGesture>();
        _releaseGesture.Released += OnReleased;
        
        _longPressGesture = GetComponent<LongPressGesture>();
        _longPressGesture.LongPressed += OnLongPressed;
    }

    public void Init(SpellConfig config, SpellInfoPanel spellInfoPanel)
    {
        Config = config;
        _spellInfoPanel = spellInfoPanel;
        _iconHolder.sprite = Config.Icon;
        
        if (!CooldownControlersMap.ContainsKey(Config))
            CooldownControlersMap.Add(Config, null);

        if (CooldownControlersMap.TryGetValue(Config, out var cdController))
            UpdateCooldownState(cdController);
    }

    public bool CheckCooldownState()
    {
        CooldownControlersMap.TryGetValue(Config, out var controller);
        if (controller == null)
            return false;
        return controller.RemainRounds > 0;
    }

    public void StartCooldownState()
    {
        var cdController = new CooldownController(_battleController.GameState, Config);
        CooldownControlersMap[Config] = cdController;
        _cooldownValueField.gameObject.SetActive(true);
        UpdateCooldownState(cdController);
    }

    private void UpdateCooldownState(CooldownController controller)
    {
        if (controller == null)
        {
            _cooldownValueField.gameObject.SetActive(false);
            _cooldownDisplay.fillAmount = 0;
            return;
        }
        
        var remainRounds = controller.RemainRounds;
        if (remainRounds > 0)
        {
            if (!_disabled) Disable();
            _cooldownValueField.gameObject.SetActive(true);
            _cooldownValueField.text = remainRounds.ToString();
            _cooldownDisplay.fillAmount = controller.Percent;
        }
        else
        {
            _cooldownValueField.gameObject.SetActive(false);
            _cooldownDisplay.fillAmount = 0;
            controller.Dispose();
            CooldownControlersMap[Config] = null;
        }
    }

    public void Highlight()
    {
        if (_highlighted != null)
        {
            _highlighted._uiEffect.effectFactor = 1;
        }
        _uiEffect.effectFactor = 0;
        _highlighted = this;
    }

    public void Disable()
    {
        _uiEffect.effectFactor = 1;
        _disabled = true;
        _highlighted = null;
    }
    
    public void DisableAndHighlight()
    {
        _uiEffect.effectFactor = 0;
        _disabled = true;
    }
    
    public void Enable()
    {
        _uiEffect.effectFactor = 0;
        _disabled = false;
        _highlighted = null;
    }

    private void Select()
    {
        if (Config.ExecuteType == ExecuteTypes.Ultra 
            && Config.AOESettings.AllowedPlacementOn == AllowedNodesStatesForPlacement.Collapsed)
        {
            if(_battleController.LevelField.NavigationMap.GetCollapsedCells().Count <= 0)
                return;
        }
        
        if (_selected != null)
            _selected.Deselect();
        
        var phase = GameState.GetConcreteRoundPhase<SpellsPlanningPhase>(_battleController.CurrentRoundPhaseController);
        if (phase == null || phase.Type != GameState.RoundPhases.SpellsPhase)
        {
            Debug.LogError($"false round phase: {_battleController.GameState.CurrentRoundPhase}");
            return;
        }

        _border.color = _selectionColor;
        phase.SpellSelected?.Invoke(Config);
        _selected = this;
    }

    private void Deselect()
    {
        _border.color = Color.white;
        _selected = null;
    }

    private void OnPress(object sender, EventArgs e)
    {
        if(_disabled)
            return;
        
        Select();
    }
    
    private void OnLongPressed(object sender, EventArgs e)
    {
        if (_selected != this)
        {
            if(!_disabled)
                Select();
        }
        _spellInfoPanel.gameObject.SetActive(true);
        _spellInfoPanel.Show(Config);
    }
    
    private void OnReleased(object sender, EventArgs e)
    {
        _spellInfoPanel.Hide();
    }
}