﻿using System;
using TMPro;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;

public class SpellInfoPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _cooldownField;
    [SerializeField] private TextMeshProUGUI _titleField;
    [SerializeField] private TextMeshProUGUI _infoField;
    [SerializeField] private Image _aoeIconHolder;

    private void Awake()
    {
        Hide();
    }

    public void Show(SpellConfig config)
    {
        _cooldownField.text = config.Cooldown.ToString();
        _titleField.text = config.Name;
        _infoField.text = config.Description;
        _aoeIconHolder.sprite = config.AOESettings.Icon;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
