﻿using System;
using DG.Tweening;
using MagicBattleArena.Network;
using Photon.Pun;
using TMPro;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class CharInfoItem : MonoBehaviourPun
{
    [Inject] private GameState _gameState;
    
    [SerializeField] private Image _avatarFrame;
    [SerializeField] private Image _playerColorIdFrame;
    [SerializeField] private TextMeshProUGUI _turnOrderField;

    private CharacterView _character;
    private TapGesture _tapGesture;
    private PhotonView _photonView;

    private void Awake()
    {
        _tapGesture = GetComponent<TapGesture>();
        _tapGesture.Tapped += OnTapped;
        _photonView = GetComponent<PhotonView>();
    }

    private void OnDestroy()
    {
        _tapGesture.Tapped -= OnTapped;
        if(_character == null)
            return;
        _character.RaffleAction -= OnUpdateQueueNum;
        _character.UpdateQueueNum -= OnUpdateQueueNum;
        _character.HealthUpdated -= OnHealthUpdated;
    }

    public void Init(CharacterView character)
    {
        _character = character;

        _character.RaffleAction += OnUpdateQueueNum;
        _character.UpdateQueueNum += OnUpdateQueueNum;
        _character.HealthUpdated += OnHealthUpdated;
        
        _avatarFrame.sprite = character.Config.Avatar;
        var c = _character.Config.ColorId;
        _playerColorIdFrame.color = new Color(c.r, c.g, c.b, _playerColorIdFrame.color.a);
        
        var healthBar = GetComponentInChildren<HealthBar>();
        healthBar.Init(_character);
        
        var effectStatusesPanel = GetComponentInChildren<StatusEffectsPanel>();
        effectStatusesPanel.Init(_character);
    }

    private void OnUpdateQueueNum(byte number)
    {
        _turnOrderField.text = number.ToString();
    }

    private void OnTapped(object sender, EventArgs e)
    {
        if(!_character.PhotonView.IsMine || _gameState.CurrentRoundPhase == GameState.RoundPhases.BattleSimulationPhase)
            return;
        _character.SelectCharacterRequest?.Invoke(_character);
    }
    
    private void OnHealthUpdated(CharacterView hero, float value, float oldVHPValue)
    {
        if (!_character.Model.IsDead) 
            return;

        _tapGesture.Tapped -= OnTapped;
        _photonView.RPC(nameof(HandleAvatarFrame), RpcTarget.AllViaServer);
    }

    [PunRPC]
    private void HandleAvatarFrame() => _avatarFrame.color = new Color(1, 1, 1, .5f);
}