﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using static SpellsPlanningPhase;

public class StatusEffectsPanel : MonoBehaviour, IPunObservable
{
    private static string GetGuidKey(PlannedSpell spell, string heroId) => $"{spell.GUID}_{spell.Config.name}_{heroId}";

    [SerializeField] private Image _iconHolderRef;

    private List<Sprite> _icons;
    private Dictionary<string, GameObject> _iconsMap = new Dictionary<string, GameObject>();
    private Dictionary<string, GameObject> _iconsAtOtherPlayerMap = new Dictionary<string, GameObject>();
    private CharacterView _hero;
    private PhotonView _photonView;

    private void OnDestroy()
    {
        _hero.StatusEffectAdded -= OnBuffAdded;
        _hero.StatusEffectRemoved -= OnBuffRemoved;
        _hero.HealthUpdated -= OnHealthUpdated;
    }

    public void Init(CharacterView hero)
    {
        _icons = hero.LevelField.BattleController.BattleConfig.EffectsStatusesIcons;
        _hero = hero;
        _photonView = GetComponent<PhotonView>();

        _hero.HealthUpdated += OnHealthUpdated;
        _hero.StatusEffectAdded += OnBuffAdded;
        _hero.StatusEffectRemoved += OnBuffRemoved;
    }

    private void OnBuffAdded(PlannedSpell spell, string heroId, Sprite icon)
    {
        //MagicBattleArena.Logger.Log($"OnBuffAdded: {spell.Config.name}");
        var key = GetGuidKey(spell, heroId);
        if (!_iconsMap.ContainsKey(key))
        {
            var iconHolder = Instantiate(_iconHolderRef);
            iconHolder.sprite = icon;
            iconHolder.transform.SetParent(transform);
            _iconsMap.Add(key, iconHolder.gameObject);
            
            _photonView.RPC(nameof(AddIconAtOther), RpcTarget.Others, icon.name, spell.GUID);
        }
    }

    private void OnBuffRemoved(PlannedSpell spell, string heroId)
    {
        var key = GetGuidKey(spell, heroId);
        if (_iconsMap.ContainsKey(key))
        {
            _photonView.RPC(nameof(RemoveIconAtOther), RpcTarget.Others, spell.GUID);
            Destroy(_iconsMap[key]);
            _iconsMap.Remove(key);
        }
    }
    
    private void OnHealthUpdated(CharacterView hero, float value, float oldHPValue)
    {
        if (!hero.Model.IsDead) 
            return;
        
        _hero.HealthUpdated -= OnHealthUpdated;
        _photonView.RPC(nameof(SetDeathIcon), RpcTarget.AllViaServer);
    }

    [PunRPC]
    private void SetDeathIcon()
    {
        var iconHolder = Instantiate(_iconHolderRef, transform, true);
        iconHolder.sprite = _hero.LevelField.BattleController.BattleConfig.DeathStatusIcon;
    }
    
    [PunRPC]
    private void AddIconAtOther(string iconName, string guid)
    {
        var icon = _icons.FirstOrDefault(i => i.name == iconName);
        
        var iconHolder = Instantiate(_iconHolderRef);
        iconHolder.sprite = icon;
        iconHolder.transform.SetParent(transform);
        
        _iconsAtOtherPlayerMap.Add(guid, iconHolder.gameObject);
    }
    
    [PunRPC]
    private void RemoveIconAtOther(string guid)
    {
        if (_iconsAtOtherPlayerMap.TryGetValue(guid, out var icon))
        {
            Destroy(icon);
            _iconsAtOtherPlayerMap.Remove(guid);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}
