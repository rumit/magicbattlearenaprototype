﻿using System;
using System.Linq;
using MagicBattleArena.Network;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class TeamInfoPanel : MonoBehaviour
{
    [Inject] private CommonBattleConfig _battleConfig;
    [SerializeField] private Image _teamColorIdFrame;
    [SerializeField] private CharInfoItem[] CharItems;

    public void Init(bool isLocalPlayer)
    {
        var c = isLocalPlayer ? _battleConfig.TeamSettings.PlayerTeamColor : _battleConfig.TeamSettings.EnemyTeamColor;
        _teamColorIdFrame.color = new Color(c.r, c.g, c.b, _teamColorIdFrame.color.a);

        var team = isLocalPlayer ? Player.LocalPlayer.Team : Player.EnemyPlayer.Team;
        for (var i = 0; i < team.Count; i++)
        {
            var ch = team[i];
            CharItems[i].Init(ch);
        }
    }
}
