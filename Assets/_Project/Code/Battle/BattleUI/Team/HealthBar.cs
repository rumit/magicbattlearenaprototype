﻿using System;
using DG.Tweening;
using MagicBattleArena.Network;
using Photon.Pun;
using TMPro;
using TouchScript.Examples.Cube;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour, IPunObservable
{
    private static readonly Color fullHealthColor = new Color(0, 1, 0, 1);
    private static readonly Color middleHealthColor = new Color(1f, 0.78f, 0f);
    private static readonly Color emptyHealthColor = new Color(1, 0, 0, 1);
    
    [SerializeField] private Image _hpProgressLine;
    [SerializeField] private TextMeshProUGUI _hpProgressValueField;
    [SerializeField] private TextMeshProUGUI _hpProgressValueShadowField;
    
    private CharacterView _hero;
    private PhotonView _photonView;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
    }

    private void OnDestroy()
    {
        _hero.HealthUpdated -= OnHealthUpdated;
    }

    public void Init(CharacterView hero)
    {
        _hero = hero;
        _hero.HealthUpdated += OnHealthUpdated;
        OnHealthUpdated(_hero, 0, _hero.Config.HP);
    }
    
    private void OnHealthUpdated(CharacterView hero, float value, float oldHPValue)
    {
        _photonView.RPC(nameof(UpdateHealthBar), RpcTarget.AllViaServer, _hero.Model.HP, _hero.Config.HP);
    }
    
    [PunRPC]
    private void UpdateHealthBar(float currentHealth, float maxHealth)
    {
        var percent = currentHealth / maxHealth;
        _hpProgressLine.transform.DOScaleX(percent, 0);
        _hpProgressLine.color = percent > 0.5f
            ? Color.Lerp(middleHealthColor, fullHealthColor, (percent - 0.5f) * 2)
            : Color.Lerp(emptyHealthColor, middleHealthColor, percent * 2);
        
        _hpProgressValueField.text = 
        _hpProgressValueShadowField.text = $"{Mathf.Round(currentHealth)}/{maxHealth}";
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}
