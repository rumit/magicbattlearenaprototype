﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening.Core;
using LevelField;
using MagicBattleArena;
using MagicBattleArena.Network;
using Newtonsoft.Json;
using Photon.Pun;
using Selectors;
using UnityEngine;
using static SpellActions;
using Logger = MagicBattleArena.Logger;

public class BuffsManager : MonoBehaviourPun, IPunObservable
{
    public enum BuffsTags
    {
        None,
        ActionPointsModifier,
        DamageModifier,
        Invisibility,
        MoveRestriction,
        ProtectorsDutyBuff,
        SpellsRestriction,
        TrapActor,
        NoEffectsAccept,
        ShieldAction,
        Hibernation,
        SuppressPresence
    }

    private BattleBaseController _battleController;
    private PhotonView _photonView;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
    }

    public void Init(BattleBaseController battleController)
    {
        _battleController = battleController;
    }

    public List<T> CreateFieldCellsBuffsByType<T>(IExecutorContext context, int liveTime, FieldPartTypes fieldPartType)
        where T : IBuff, new()
    {
        var result = new List<T>();
        var map = context.BattleController.LevelField.NavigationMap;
        var nodes = map.GetNodesByWorldPositions(context.Spell.AoeCellsData.Select(p => p.Position).ToList());
        switch (fieldPartType)
        {
            case FieldPartTypes.Player:
                nodes = nodes.Where(n => n.BelongsPlayer).ToList();
                break;
            case FieldPartTypes.Enemy:
                nodes = nodes.Where(n => !n.BelongsPlayer).ToList();
                break;
        }
        
        foreach (var node in nodes)
        {
            var buff = new T
            {
                BattleController = context.BattleController,
                Owner = node,
                Spell = context.Spell,
                LivetimeInRounds = liveTime,
            };
            result.Add(buff);
        }
        return result;
    }
    
    public List<T> CreateHeroBuffsByType<T>(ITargetsSelector targetSelector, IExecutorContext context, int liveTime)
        where T : IBuff, new()
    {
        var result = new List<T>();
        if (targetSelector.TargetType == TargetTypes.Self)
        {
            var actor = targetSelector.GetTarget(context.Spell.Config.Owner);
            var buff = new T
            {
                BattleController = context.BattleController,
                Owner = actor,
                Spell = context.Spell,
                LivetimeInRounds = liveTime
            };
            result.Add(buff);
        }
        else
        {
            var targets = targetSelector.GetTargets(context.Spell);
            foreach (var target in targets)
            {
                var buff = new T
                {
                    BattleController = context.BattleController,
                    Owner = target,
                    Spell = context.Spell,
                    LivetimeInRounds = liveTime
                };
                result.Add(buff);
            }
        }
        return result;
    }
    
    public T CreateHeroBuffByType<T>(ITargetsSelector targetSelector, IExecutorContext context, int liveTime) where T : IBuff, new()
    {
        CharacterView actor;
        if (targetSelector.TargetType == TargetTypes.RandomEnemy)
            actor = targetSelector.GetRandomTarget(context.Spell);
        else
            actor = targetSelector.GetTarget(context.Spell.Config.Owner);

        var buff = new T
        {
            BattleController = context.BattleController,
            Owner = actor,
            Spell = context.Spell,
            LivetimeInRounds = liveTime

        };
        return buff;
    }
    
    public T CreateHeroBuffByType<T>(CharacterView actor, IExecutorContext context, int liveTime) where T : IBuff, new()
    {
        var buff = new T
        {
            BattleController = context.BattleController,
            Owner = actor,
            Spell = context.Spell,
            LivetimeInRounds = liveTime

        };
        return buff;
    }

    public void FinishAllBuffsAtHero(CharacterView hero)
    {
        for (var i = 0; i < hero.Buffs.Count; i++)
            hero.Buffs[i].End();
    }

    public void AddHeroBuff(CharacterView hero, IBuff buff)
    {
        if(!hero.Model.IsDead) hero.AddBuff(buff);
    }
    
    public void RemoveHeroBuff(CharacterView hero, IBuff buff)
    {
        _photonView.RPC(nameof(_RemoveHeroBuff), RpcTarget.AllViaServer, hero.Config.name, buff.GUID);
    }

    public void AddFieldCellBuff(Node cell, IBuff buff)
    {
        if(cell.State == Node.States.CollapsedState)
            return;
        cell.AddBuff(buff);
    }
    
    public void RemoveFieldCellBuff(Node cell, IBuff buff)
    {
        var cellWorld = _battleController.LevelField.NavigationMap.GetWorldPositionByNode(cell);
        _photonView.RPC(nameof(_RemoveFieldCellBuff), RpcTarget.AllViaServer, cellWorld, buff.GUID);
    }

    #region Photon
    [PunRPC]
    private void _RemoveHeroBuff(string heroId, string buffGuid)
    {
        var hero = Player.GetHeroById(heroId);
        var buff = hero.Buffs.FirstOrDefault(b => b.GUID == buffGuid);
        if (buff != null)
            hero.RemoveBuff(buff);
    }
    
    [PunRPC]
    private void _RemoveFieldCellBuff(Vector3 position, string buffGuid)
    {
        var cell = _battleController.LevelField.NavigationMap.GetNodeByWorldPosition(position);
        var buff = cell.Buffs.FirstOrDefault(b => b.GUID == buffGuid);
        if (buff != null)
            cell.RemoveBuff(buff);
    }
    #endregion

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}