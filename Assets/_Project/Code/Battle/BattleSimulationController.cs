﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using ExitGames.Client.Photon;
using LevelField;
using Newtonsoft.Json;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using static BattleSimulationPhase;
using static SpellActions.DamageModifierAction;
using Logger = MagicBattleArena.Logger;
using Player = MagicBattleArena.Network.Player;

[DisallowMultipleComponent]
public class BattleSimulationController : MonoBehaviourPun, IPunObservable
{
    #region Static
    private static bool MatchSimulationPhaseToSpellType(SimulationsPhases phase, SpellConfig.ExecuteTypes spellType)
    {
        switch (phase)
        {
            case SimulationsPhases.CommonSpells when spellType == SpellConfig.ExecuteTypes.Light:
            case SimulationsPhases.LightSpells when spellType == SpellConfig.ExecuteTypes.Common || spellType == SpellConfig.ExecuteTypes.Ultra:
                return false;
            default: return true;
        }
    }
    #endregion

    public Action<SpellConfig> SpellApplied;
    
    private BattleBaseController _battleController;
    private BattleSimulationPhase _battleSimulationPhase;
    public Dictionary<CharacterView, BattleSimulationExecutionData> ExecutionsMap { get; private set; }

    private void Awake() => _battleController = GetComponent<BattleBaseController>();

    public void Init(BattleSimulationPhase battleSimulationPhase)
    {
        _battleSimulationPhase = battleSimulationPhase;
        ExecutionsMap = new Dictionary<CharacterView, BattleSimulationExecutionData>();
    }
    
    public void AddData(CharacterView character, BattleSimulationExecutionData data)
    {
        if(!ExecutionsMap.ContainsKey(character))
            ExecutionsMap.Add(character, data);
    }
    
    public void DoNextSpellSimulation(CharacterView character, SimulationsPhases phase)
    {
        photonView.RPC(nameof(RunSpellSimulation), RpcTarget.AllViaServer, character.Config.name, (int)phase);
    }
    
    public void DoNextMovementSimulation(CharacterView character)
    {
        photonView.RPC(nameof(RunMovementSimulation), RpcTarget.AllViaServer, character.Config.name);
    }

    public void Attack(AttackData attack)
    {
        var data = JsonConvert.SerializeObject(attack, Formatting.None,
            new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
        photonView.RPC(nameof(_Attack), RpcTarget.AllViaServer, data);
    }

    public void ModifyActionPoints(CharacterView target, int actionPoints)
    {
        if(target.PhotonView.IsMine)
            target.ModifyActionPoints(actionPoints);
        else
            photonView.RPC(nameof(_ModifyActionPoints), RpcTarget.Others, target.Config.name, actionPoints);
    }
    
    public void Teleport(CharacterView actor, Node node)
    {
        var nodeWorld = _battleController.LevelField.NavigationMap.GetWorldPositionByNode(node);
        photonView.RPC(nameof(_Teleport), RpcTarget.AllViaServer, actor.Config.name, nodeWorld);
    }

    public void RemoveObstacle(Vector3 pos) => photonView.RPC(nameof(_RemoveObstacle), RpcTarget.AllViaServer, pos);

    public void RemoveNegativeEffects(string[] heroes)
    {
        photonView.RPC(nameof(_RemoveNegativeEffects), RpcTarget.AllViaServer, heroes as object);
    }

    #region Photon events && callbacks
    [PunRPC] private async void RunSpellSimulation(string charId, int phase)
    {
        var character = Player.GetLocalHeroById(charId);
        if (character == null)
            return;

        ExecutionsMap.TryGetValue(character, out var execData);
        var spellConfig = execData.Spell?.Config;
        var hasSpellRestrict = character.ContainsBuffTag(BuffsManager.BuffsTags.SpellsRestriction);
        if (character.Model.IsDead || hasSpellRestrict || spellConfig == null 
            || !MatchSimulationPhaseToSpellType((SimulationsPhases)phase, spellConfig.ExecuteType))
        {
            await UniTask.DelayFrame(1);
            photonView.RPC(nameof(CurrentActorComplete), RpcTarget.MasterClient);
            return;
        }

        if(!character.ContainsBuffTag(BuffsManager.BuffsTags.SuppressPresence))
            photonView.RPC(nameof(HighlightingActorAtOtherPlayer), RpcTarget.Others, character.Config.name, true);
        
        character.SelectCharacterRequest?.Invoke(character);
        spellConfig.Executor?.Run(new ExecutorContext(_battleController, character, execData.Spell));
        SpellApplied?.Invoke(spellConfig);
    }

    [PunRPC] private async void RunMovementSimulation(string charId)
    {
        var character = Player.GetLocalHeroById(charId);
        if (character == null)
            return;
        
        ExecutionsMap.TryGetValue(character, out var execData);
        
        var moveRestrict = character.ContainsBuffTag(BuffsManager.BuffsTags.MoveRestriction);
        if (character.Model.IsDead || moveRestrict || execData.Path == null)
        {
            if (execData.Path != null && execData.Path.Count > 0)
            {
                var endPathNode = execData.Path.Last();
                RemoveObstacle(_battleController.LevelField.NavigationMap.GetWorldPositionByNode(endPathNode));
            }
            await UniTask.DelayFrame(1);
            photonView.RPC(nameof(CurrentActorComplete), RpcTarget.MasterClient);
            return;
        }
        
        //Logger.Log($"Move Next => {character}, Path count: {execData.Path?.Count}");
        character.Movement.MoveTo(execData.Path, () =>
        {
            photonView.RPC(nameof(SetHeroPositionAtAllPlayers), RpcTarget.AllViaServer, character.Config.name);
            //if (character.PhotonView.IsMine)
            photonView.RPC(nameof(CurrentActorComplete), RpcTarget.MasterClient);
        });
    }
    
    public void CompleteCurrentSpellAction() => photonView.RPC(nameof(CurrentActorComplete), RpcTarget.MasterClient);

    [PunRPC] private void CurrentActorComplete()
    {
        var isNotComlpete = _battleSimulationPhase.TryNextActor();
        if (isNotComlpete == false)
            photonView.RPC(nameof(HandleActorsSelectionOnSimulationPhaseCompleted), RpcTarget.AllViaServer);
    }
    
    [PunRPC] private void HandleActorsSelectionOnSimulationPhaseCompleted()
    {
        foreach (var enemy in Player.GetLivingEnemies())
            enemy.Interactor.ToggleHighlighting(false);

        var actors = Player.GetLivingHeroes();
        if(actors.Count <= 0)
            return;
        var actor = actors.OrderBy(c => c.QueueNumber).Last();
        if (actor == null) 
            return;
        actor.SelectCharacterRequest(actor);
    }
    
    [PunRPC] private void HighlightingActorAtOtherPlayer(string charId, bool value)
    {
        var actor = Player.GetHeroById(charId);
        actor.Interactor.ToggleHighlighting(value);
    }
    
    [PunRPC] private void SetHeroPositionAtAllPlayers(string heroId)
    {
        var hero = Player.GetHeroById(heroId);
        var pos = hero.transform.position;
        hero.Movement.SetPosition(pos);
        _battleController.LevelField.NavigationMap.SetObstacle(hero, pos);
    }
    
    [PunRPC] private void _Teleport(string actorId, Vector3 point)
    {
        var map = _battleController.LevelField.NavigationMap;
        var actor = Player.GetHeroById(actorId);
        map.RemoveObstacle(actor.transform.position);
        actor.Movement.Teleport(point);
        map.SetObstacle(actor, point);
    }
    
    [PunRPC] private void _Attack(string data)
    {
        var attack = JsonConvert.DeserializeObject<AttackData>(data);
        
        var actor = Player.GetHeroById(attack.Actor);
        var target = Player.GetHeroById(attack.Target);
        
        var dmgFromActorModified = attack.OriginDamageValue;
        var resultDamage = attack.OriginDamageValue;
        
        List<DamageModifierData> damageDoneReducers = null;
        List<DamageModifierData> damageDoneMultipliers = null;
        if (actor != null)
        {
            damageDoneReducers = actor.TryGetDamageModifierByType(DamageTypes.DamageDoneReducer);
            damageDoneMultipliers = actor.TryGetDamageModifierByType(DamageTypes.DamageDoneMultiplier);
        }

        if (damageDoneReducers != null)
        {
            float r = 0;
            foreach (var dmgDoneReducer in damageDoneReducers)
                r += DamageModifierData.ApplyModifier(dmgDoneReducer, attack.OriginDamageValue);
            dmgFromActorModified = resultDamage = attack.OriginDamageValue - r;
        }

        if (damageDoneMultipliers != null)
        {
            float r = 0;
            foreach (var dmgDoneMultiplier in damageDoneMultipliers)
                r += DamageModifierData.ApplyModifier(dmgDoneMultiplier, attack.OriginDamageValue);
            dmgFromActorModified = resultDamage += r;
        }
        
        var dmgReducers = target.TryGetDamageModifierByType(DamageTypes.TakenDamageReducer);
        if (dmgReducers != null)
        {
            float r = 0;
            foreach (var dmgReducer in dmgReducers)
                r += DamageModifierData.ApplyModifier(dmgReducer, dmgFromActorModified);
            resultDamage = dmgFromActorModified - r;
        }

        attack.FromActorModifiedDamageValue = dmgFromActorModified;
        attack.ResultDamageValue = attack.IgnoreProtectiveEffects ? dmgFromActorModified : resultDamage;

        var protectorsDutyBuff = target.TryGetBuff<ProtectorsDutyBuff>();
        if (protectorsDutyBuff != null && protectorsDutyBuff.HasLinkedInArea(attack))
            return;

        if (target.PhotonView.IsMine)
            target.TakeDamage(attack, actor);
    }

    [PunRPC] private void _ModifyActionPoints(string heroId, int actionPoints)
    {
        var hero = Player.GetHeroById(heroId);
        hero.ModifyActionPoints(actionPoints);
    }

    [PunRPC] private void _RemoveObstacle(Vector3 pos) => _battleController.LevelField.NavigationMap.RemoveObstacle(pos);

    [PunRPC] private void _RemoveNegativeEffects(string[] heroes)
    {
        var buffsManager = _battleController.BuffsManager;
        foreach (var heroId in heroes)
        {
            var hero = Player.GetHeroById(heroId);
            var negativeBuffs = hero.Buffs.Where(b => b is INegativeEffect).ToList();
            if (negativeBuffs.Count > 0)
                negativeBuffs.ForEach(b => { buffsManager.RemoveHeroBuff(hero, b); });
        }
    }
    #endregion

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}