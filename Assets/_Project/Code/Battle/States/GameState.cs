﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using ExitGames.Client.Photon;
using LevelField;
using MagicBattleArena;
using MagicBattleArena.Network;

public class GameState
{
    public static T GetConcreteRoundPhase<T>(IRoundPhase phase) where T : class, IRoundPhase
    {
        return phase as T;
    }
    
    
    public Action<Action, LevelFieldView> BattleInitialized;
    public Action RoundChanged;
    public Action<AbstractRoundPhase<IRoundPhase>, AbstractRoundPhase<IRoundPhase>> RoundPhaseChanged;
    public Action BattleSimulationCompleted;
    public Action HeroDeath;
    public Action<Player> BattleComplete;

    public enum RoundPhases
    {
        MovementPhase,
        SpellsPhase,
        BattleSimulationPhase
    }
    
    public enum States
    {
        None,
        During,
        Over
    }

    public States CurrentState { get; private set; }
    public int CurrentRoundNumber { get; private set; }
    public RoundPhases CurrentRoundPhase { get; private set; }

    public GameState()
    {
        RoundPhaseChanged += OnRoundPhaseChanged;
        HeroDeath += OnHeroDeath;
    }

    public void Dispose()
    {
        RoundPhaseChanged -= OnRoundPhaseChanged;
        HeroDeath -= OnHeroDeath;
    }

    public void Init()
    {
        CurrentRoundPhase = RoundPhases.MovementPhase;
        CurrentState = States.During;
        StartNextRound();
    }

    public void StartNextRound()
    {
        CurrentRoundNumber++;
        RoundChanged?.Invoke();
        //Logger.Log($"GameState CurrentRoundNumber: {CurrentRoundNumber}");
    }

    private void OnRoundPhaseChanged(AbstractRoundPhase<IRoundPhase> prevPhase, AbstractRoundPhase<IRoundPhase> newPhase)
    {
        CurrentRoundPhase = newPhase.Type;
    }

    private async void OnHeroDeath()
    {
        Player winner = null;

        if (Player.GetLivingHeroes().Count <= 0)
            winner = Player.EnemyPlayer;
        
        if (Player.GetLivingEnemies().Count <= 0)
            winner = Player.LocalPlayer;

        if (winner == null) 
            return;

        CurrentState = States.Over;
        await UniTask.Delay(TimeSpan.FromSeconds(4));
        
        BattleComplete?.Invoke(winner);
    }
}
