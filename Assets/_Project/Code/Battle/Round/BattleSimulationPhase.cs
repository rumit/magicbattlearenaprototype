﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using LevelField;
using MagicBattleArena;
using MagicBattleArena.Network;
using Photon.Pun;

public class BattleSimulationPhase : AbstractRoundPhase<IRoundPhase>
{
    public enum SimulationsPhases
    {
        None,
        LightSpells,
        Movement,
        CommonSpells
    }
    
    private readonly Dictionary<SimulationsPhases, Action> _displayingPhasesMap = new Dictionary<SimulationsPhases, Action>();

    private SimulationsPhases _currentSimulationsPhase = SimulationsPhases.None;
    
    private Queue<CharacterView> _actorsQueue;
    private BattleSimulationController _simulationController;
    private int _totalRoundPhases;

    public BattleSimulationPhase(BattleBaseController battleBaseController, CommonBattleConfig config)
        : base(battleBaseController, config)
    {
        Type = GameState.RoundPhases.BattleSimulationPhase;

        _simulationController = _battleBaseController.BattleSimulator;

        _displayingPhasesMap.Add(SimulationsPhases.LightSpells, StartLightSpellsPhase);
        _displayingPhasesMap.Add(SimulationsPhases.Movement, StartMovementPhase);
        _displayingPhasesMap.Add(SimulationsPhases.CommonSpells, StartCommonSpellsPhase);
        _totalRoundPhases = (int) SimulationsPhases.CommonSpells;
    }

    public override async void Start()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(.5f));
        ToNextSimulationPhase();
    }
    
    private void ToNextSimulationPhase()
    {
        var nextNum = (int) _currentSimulationsPhase + 1;
        //_battleBaseController.DebugOutput.Print($"{_currentSimulationsPhase} Complete!, nextNum: {nextNum}, total: {_totalRoundPhases}");
        //Logger.Log($"simulation phase completed: {_currentSimulationsPhase}, next simulation phase: {(SimulationsPhases) nextNum}");
        if (nextNum > _totalRoundPhases)
        {
            _battleBaseController.CompleteBattleSimulation();
            return;
        }
        
        EnqueueActors();
        _currentSimulationsPhase = (SimulationsPhases) nextNum;
        _displayingPhasesMap[_currentSimulationsPhase].Invoke();
    }
    
    private void EnqueueActors()
    {
        _actorsQueue = new Queue<CharacterView>();
        var sorted = Player.GetAllHeroes().OrderBy(c => c.QueueNumber);
        foreach (var c in sorted)
        {
            _actorsQueue.Enqueue(c);
        }
    }
    
    public bool TryNextActor()
    {
        //Logger.Log($"#Test phase TryNextActor: {_currentSimulationsPhase}, actorsQueue count: {_actorsQueue.Count}");
        if (_actorsQueue.Count <= 0)
        {
            ToNextSimulationPhase();
            return false;
        }
        _displayingPhasesMap[_currentSimulationsPhase].Invoke();
        return true;
    }

    private void StartLightSpellsPhase()
    {
        //Logger.Log($"#Test phase StartLightSpellsPhase: {_currentSimulationsPhase}, actorsQueue count: {_actorsQueue.Count}");
        var next = _actorsQueue.Dequeue();
        _simulationController.DoNextSpellSimulation(next, SimulationsPhases.LightSpells);
    }
    
    private void StartMovementPhase()
    {
        //Logger.Log($"#Test phase StartMovementPhase: {_currentSimulationsPhase}, actorsQueue count: {_actorsQueue.Count}");
        var next = _actorsQueue.Dequeue();
        _simulationController.DoNextMovementSimulation(next);
    }
    
    private void StartCommonSpellsPhase()
    {
        //Logger.Log($"#Test phase StartCommonSpellsPhase: {_currentSimulationsPhase}, actorsQueue count: {_actorsQueue.Count}");
        var next = _actorsQueue.Dequeue();
        _simulationController.DoNextSpellSimulation(next, SimulationsPhases.CommonSpells);
    }
}

public struct BattleSimulationExecutionData
{
    public List<Node> Path;
    public SpellsPlanningPhase.PlannedSpell Spell;

    public override string ToString()
    {
        var path = Path == null ? "null" : $"\n{string.Join("\n", Path)}" ;
        return $"[Path:{path},\n\n{Spell}]";
    }
}