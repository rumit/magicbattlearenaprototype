﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using MagicBattleArena.Network;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public abstract class PlannableRoundPhase : AbstractRoundPhase<IRoundPhase>
{
    public Action<int> TickedUp;

    protected readonly List<CharacterView> _completedCharacters = new List<CharacterView>();
    protected bool _isLocalPlayerComplete;
    private int _remainingTime;

    protected PlannableRoundPhase(BattleBaseController battleBaseController, CommonBattleConfig config)
        : base(battleBaseController, config)
    {
        _battleBaseController.CurrentCharacterPlanningComplete += OnCurrentCharacterPlanningComplete;
        _battleBaseController.SelectCharacter += OnCharacterSelected;
    }

    public override void Start()
    {
        _remainingTime = _config.RoundPhasesDuration[Type];
        _battleBaseController.StartCoroutine(PhaseTick());
    }
    
    private IEnumerator PhaseTick()
    {
        while (_remainingTime > 0 )
        {
            yield return new WaitForSeconds(1);
            _remainingTime--;
            TickedUp?.Invoke(_remainingTime);
        }
        
        if (!_isLocalPlayerComplete)
            OnCompleteLocalPlayer();
    }
    
    protected void AddCompletedCharacter(CharacterView character)
    {
        if(_completedCharacters.Contains(character))
            return;
        
        _completedCharacters.Add(character);
        var liveHeroes = Player.GetLivingHeroes();
        if(_completedCharacters.Count >= liveHeroes.Count)
            OnCompleteLocalPlayer();
    }

    public override void CompletePhase()
    {
        _battleBaseController.StopCoroutine(PhaseTick());
        _battleBaseController.CurrentCharacterPlanningComplete -= OnCurrentCharacterPlanningComplete;
        _battleBaseController.SelectCharacter -= OnCharacterSelected;
        Logger.Log($"CompletePhase: {this}");
    }

    protected virtual void OnCompleteLocalPlayer()
    {
        _battleBaseController.CurrentCharacterPlanningComplete -= OnCurrentCharacterPlanningComplete;
        _isLocalPlayerComplete = true;
        //Logger.Log($"{this} Local Player Complete!");
        _battleBaseController.LocalPlayerPlanningComplete();
    }
    
    protected virtual void OnCharacterSelected(CharacterView character)
    {
        //Logger.Log($"completedCharacters: {string.Join(",", _completedCharacters)}");
        if (!_completedCharacters.Contains(character))
            _battleBaseController.NextCharacterSelected?.Invoke();

        _battleBaseController.SelectPlayerCharacter(character);
    }
    
    protected void RemoveGhostViews()
    {
        foreach (var ghost in _battleBaseController.GhostsMap.Values)
            GameObject.Destroy(ghost.gameObject);
        _battleBaseController.GhostsMap.Clear();
    }

    protected abstract void OnCurrentCharacterPlanningComplete(Action<bool> callback);
}
