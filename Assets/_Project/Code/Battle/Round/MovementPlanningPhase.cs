﻿using System;
using System.Collections.Generic;
using System.Linq;
using MagicBattleArena.Network;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class MovementPlanningPhase : PlannableRoundPhase
{
    private readonly Dictionary<CharacterView, List<LevelField.Node>> _reachableCellsMap;
    private readonly Dictionary<CharacterView, List<LevelField.Node>> _chosenPathsMap;

    private List<LevelField.Node> _selectedPath;

    public MovementPlanningPhase(BattleBaseController battleBaseController, CommonBattleConfig config) : base(battleBaseController, config)
    {
        Type = GameState.RoundPhases.MovementPhase;
        
        _reachableCellsMap = new Dictionary<CharacterView, List<LevelField.Node>>();
        foreach (var charachter in _team)
            _reachableCellsMap.Add(charachter, null);
        
        _chosenPathsMap = new Dictionary<CharacterView, List<LevelField.Node>>();
        foreach (var charachter in _team)
            _chosenPathsMap.Add(charachter, null);
    }

    public override void Start()
    {
        var living = Player.GetLivingHeroes();
        var suitableHeroes = new List<CharacterView>();
        foreach (var hero in living)
        {
            if (hero.ContainsBuffTag(BuffsManager.BuffsTags.MoveRestriction) || hero.Model.AP <= 0)
                AddCompletedCharacter(hero);
            else
                suitableHeroes.Add(hero);
        }

        if (suitableHeroes.Count <= 0)
            return;

        var heroToSelect = suitableHeroes.First();
        heroToSelect.SelectCharacterRequest?.Invoke(heroToSelect);

        _battleBaseController.LevelField.FieldTapped += OnFieldTapped;
        base.Start();
    }

    public List<LevelField.Node> GetPlannedPath(CharacterView character)
    {
        _chosenPathsMap.TryGetValue(character, out var result);
        return result;
    }

    protected override void OnCompleteLocalPlayer()
    {
        _battleBaseController.LevelField.FieldTapped -= OnFieldTapped;
        _battleBaseController.SelectCharacter -= OnCharacterSelected;
        base.OnCompleteLocalPlayer();
    }

    public override void CompletePhase()
    {
        _map.DeselectPlayerCells();
        var heroes = Player.GetLivingHeroes();
        foreach (var hero in heroes)
            hero.ReachableCellsShown = false;
        base.CompletePhase();
    }

    protected override void OnCharacterSelected(CharacterView hero)
    {
        ClearReachableCells(hero);
        _selectedPath = null;

        if (!hero.PhotonView.IsMine || _completedCharacters.Contains(hero) || _isLocalPlayerComplete)
        {
            _battleBaseController.BattleBaseUI.ToggleCompleteActionConfirmBtn?.Invoke(false);
            return;
        }

        _battleBaseController.BattleBaseUI.ToggleCompleteActionConfirmBtn?.Invoke(true);
        //Logger.Log($"OnCharacterSelected, Selected: {_battleBaseController.SelectedCharacter.Config.name}, To select: {hero.Config.name}");
        if (hero == _battleBaseController.SelectedCharacter)
        {
            if (hero.ReachableCellsShown)
            {
                hero.ReachableCellsShown = false;
                _reachableCellsMap[hero] = null;
            }
            else
                AddReachableCellsForCharacter(hero);
        }
        else
            AddReachableCellsForCharacter(hero);

        base.OnCharacterSelected(hero);
        
    }

    protected override void OnCurrentCharacterPlanningComplete(Action<bool> callback)
    {
        var character = _battleBaseController.SelectedCharacter;
        
        if (_selectedPath == null)
        {
            AddCompletedCharacter(character);
            _map.DeselectPlayerCells();
            callback.Invoke(true);
            return;
        }

        _chosenPathsMap[character] = _selectedPath;
        var endPathNode = _selectedPath.Last();
        var pos = _map.GetWorldPositionByNode(endPathNode);
        
        var ghostView = GameObject.Instantiate(character.Config.GhostView, pos, Quaternion.identity);
        ghostView.Init(character.Config);
        ghostView.transform.LookAt(Vector3.zero);
        
        _map.DeselectPlayerCell(endPathNode);
        _map.SetObstacle(character, endPathNode);

        if (!_battleBaseController.GhostsMap.ContainsKey(character))
            _battleBaseController.GhostsMap.Add(character, ghostView);

        AddCompletedCharacter(character);
        _map.DeselectPlayerCells();
        callback.Invoke(true);
    }

    private void OnFieldTapped(LevelField.Node node, NavigationMap map)
    {
        if (node.HasObstacle || !node.BelongsPlayer || _isLocalPlayerComplete) 
            return;
        
        var character = _battleBaseController.SelectedCharacter;
        var reachableCells = _reachableCellsMap[character];
        if(reachableCells == null || !reachableCells.Contains(node))
            return;
            
        var startPoint = _map.GetNodeByWorldPosition(character.Model.Position);
        _selectedPath = Pathfinding.FindPath(startPoint, node);
        if(_selectedPath.Count <= 0)
            return;
        
        _map.DeselectPlayerCells();
        
        character.ReachableCellsShown = false;
        _reachableCellsMap[character] = null;
        _map.HighlightPath(_selectedPath);
        //Logger.Log($"path: {string.Join(",", path)}");
        //character.Movement.MoveTo(_selectedPath, () => {});
    }

    private void ClearReachableCells(CharacterView hero)
    {
        hero.ReachableCellsShown = false;
        _map.DeselectPlayerCells();
        foreach (var character in Player.LocalPlayer.Team) _reachableCellsMap[character] = null;
    }

    private void AddReachableCellsForCharacter(CharacterView hero)
    {
        var cells = _map.SelectAvailableForMovementCells(hero.Model.AP, _map.GetNodeByWorldPosition(hero.transform.position));
        _reachableCellsMap[hero] = cells;
        hero.ReachableCellsShown = true;
    }
}