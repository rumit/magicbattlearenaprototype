﻿using System;
using System.Collections.Generic;
using System.Linq;
using MagicBattleArena.Network;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class SpellsPlanningPhase : PlannableRoundPhase
{
    public class PlannedSpell
    {
        public SpellConfig Config;
        public List<AoeCellData> AoeCellsData;
        public string GUID;
        
        public bool IsInArea(Vector3 position)
        {
            var positions = AoeCellsData.Select(c => c.Position);
            return positions.Any(p => p.Equals(position));
        }

        public override string ToString()
        {
            var aoeZone = AoeCellsData == null ? "null" : $"\n{string.Join("\n", AoeCellsData)}";
            return $"[Config: {Config}, AoeZone: {aoeZone}]";
        }
    }
    
    public Action<SpellConfig> SpellSelected;
    
    private readonly Dictionary<CharacterView, PlannedSpell> _plannedSpellResultMap = new Dictionary<CharacterView, PlannedSpell>();
    private readonly Dictionary<CharacterView, List<AoeCellData>> _presentableAoeCellsMap = new Dictionary<CharacterView, List<AoeCellData>>();
    
    private AbstractSpellAoeView _aoeView;
    private SpellConfig _currentSpellConfig;

    public SpellsPlanningPhase(BattleBaseController battleBaseController, CommonBattleConfig config) : base(battleBaseController, config)
    {
        Type = GameState.RoundPhases.SpellsPhase;
        SpellSelected += OnSpellSelected;
    }

    public override void Start()
    {
        var heroToSelect = Player.GetLivingHeroes().First();
        heroToSelect.SelectCharacterRequest?.Invoke(heroToSelect);
        base.Start();
    }

    public override void CompletePhase()
    {
        RemoveGhostViews();
        SpellSelected -= OnSpellSelected;
        _battleBaseController.SelectCharacter -= OnCharacterSelected;
        if(_aoeView != null)
            GameObject.Destroy(_aoeView.gameObject);
        base.CompletePhase();
    }

    public PlannedSpell GetPlannedSpell(CharacterView character)
    {
        _plannedSpellResultMap.TryGetValue(character, out var result);
        return result;
    }

    public bool IsSpellAlreadyPlanned(SpellConfig config)
    {
        return _plannedSpellResultMap.Values.Any(s => s.Config == config);
    }

    private void OnSpellSelected(SpellConfig config)
    {
        if(IsSpellAlreadyPlanned(config))
            return;
        
        if (config.AOESettings.Layout == null)
            throw new Exception($"{config.Name} missing AOE layout!");

        if (_aoeView != null)
            GameObject.Destroy(_aoeView.gameObject);

        var aoe = new GameObject("SpellAOE");
        _currentSpellConfig = config;
        
        if(config.AOESettings.PlacementArea == SpellConfig.PlacementAreaOfAOE.Self)
            _aoeView = aoe.AddComponent<SelfAppliedAoeView>();
        else if (config.AOESettings.IsRandom)
            _aoeView = aoe.AddComponent<RandomAoeView>();
        else
            _aoeView = aoe.AddComponent<DraggableAoeView>();
        
        _aoeView.Init(_battleBaseController, config);
    }

    protected override void OnCharacterSelected(CharacterView character)
    {
        if (_completedCharacters.Contains(character))
        {
            if(character == _battleBaseController.SelectedCharacter)
                return;
            
            var plannedSpell = _plannedSpellResultMap[character];
            _battleBaseController.BattleBaseUI.ToggleSpellCards?.Invoke(false, plannedSpell.Config);
            AddPresentationAoeView(character);
        }
        else
        {
            if (_aoeView != null)
                GameObject.Destroy(_aoeView.gameObject);
            _battleBaseController.BattleBaseUI.ToggleSpellCards?.Invoke(true, null);
        }
        base.OnCharacterSelected(character);
    }

    private void AddPresentationAoeView(CharacterView character)
    {
        if (_aoeView != null)
            GameObject.Destroy(_aoeView.gameObject);
        
        var aoe = new GameObject("SpellPresentationAOE");
        _aoeView = aoe.AddComponent<PresentationAoeView>();
        var aoeCellsData = _presentableAoeCellsMap[character];
        var plannedSpell = _plannedSpellResultMap[character];
        _aoeView.Init(aoeCellsData, plannedSpell.Config.AOESettings.CellRef, character.Config.ColorId);
    }

    protected override void OnCurrentCharacterPlanningComplete(Action<bool> callback)
    {
        if(_aoeView == null)
            return;
        
        var character = _battleBaseController.SelectedCharacter;
        var plannedSpell = new PlannedSpell
        {
            Config = _currentSpellConfig,
            AoeCellsData = _aoeView.GetAoeCellsData(),
            GUID = Guid.NewGuid().ToString()
        };
        
        //Logger.Log($"GUID: {plannedSpell.GUID}");

        if(!_plannedSpellResultMap.ContainsKey(character))
            _plannedSpellResultMap.Add(character, plannedSpell);

        if(!_presentableAoeCellsMap.ContainsKey(character))
            _presentableAoeCellsMap.Add(character, _aoeView.GetPresentableAoeCellsData());

        AddCompletedCharacter(character);
        AddPresentationAoeView(character);
        
        _battleBaseController.BattleBaseUI.ToggleSpellCards?.Invoke(false, plannedSpell.Config);
        callback.Invoke(true);
    }
}
