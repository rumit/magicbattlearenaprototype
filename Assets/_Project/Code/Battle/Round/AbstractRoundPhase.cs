﻿using System;
using System.Collections.Generic;
using MagicBattleArena;
using MagicBattleArena.Network;

public abstract class AbstractRoundPhase<T> : IRoundPhase where T : IRoundPhase
{
    public GameState.RoundPhases Type { get; protected set; }
    
    protected readonly CommonBattleConfig _config;
    protected readonly BattleBaseController _battleBaseController;
    protected NavigationMap _map;
    protected readonly List<CharacterView> _team;


    protected AbstractRoundPhase(BattleBaseController battleBaseController, CommonBattleConfig config)
    {
        _battleBaseController = battleBaseController;
        _map = _battleBaseController.LevelField.NavigationMap;
        _team = Player.LocalPlayer.Team;
        _config = config;
    }

    public abstract void Start();

    public virtual void CompletePhase() { }

    public override string ToString()
    {
        return $"[Type: {Type}]";
    }
}

public interface IRoundPhase
{
    GameState.RoundPhases Type { get; }
    void Start();
}
