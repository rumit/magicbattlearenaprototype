﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using ExitGames.Client.Photon.StructWrapping;
using LevelField;
using Newtonsoft.Json.Converters;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using static GameState;
using Logger = MagicBattleArena.Logger;
using Player = MagicBattleArena.Network.Player;

[DisallowMultipleComponent]
public class BattleBaseController : MonoBehaviourPun, IPunObservable
{
    public Action<CharacterView> SelectCharacter;
    public Action NextCharacterSelected;
    public Action<Action<bool>> CurrentCharacterPlanningComplete;

    [Inject] public GameState GameState { get; }
    [Inject] public CommonBattleConfig BattleConfig { get; }
    [Inject] public readonly DebugLogUI DebugOutput;
    
    public readonly Dictionary<CharacterView, GhostView> GhostsMap = new Dictionary<CharacterView, GhostView>();
    public BattleSimulationController BattleSimulator { get; private set; }
    public BuffsManager BuffsManager { get; private set; }
    public FieldObjectsEffects FieldObjectsEffects { get; private set; }
    public SpellsFxController SpellsFxController { get; private set; }
    public CharacterView SelectedCharacter { get; private set; }
    public LevelFieldView LevelField { get; private set; }

    public AbstractRoundPhase<IRoundPhase> CurrentRoundPhaseController { get; private set; }
    public BattleBaseUI BattleBaseUI { get; private set; }
    
    public Dictionary<CharacterView, List<Node>> PathsHash { get; private set; }
    private Dictionary<CharacterView, SpellsPlanningPhase.PlannedSpell> _spellsHash;
    private int _numberCompletedPlayers;
    private BattleInitializer _battleInitializer;

    private void Awake()
    {
        new StringEnumConverter();

        BattleSimulator = GetComponent<BattleSimulationController>();
        SpellsFxController = GetComponent<SpellsFxController>();
        FieldObjectsEffects = FindObjectOfType<FieldObjectsEffects>();
        BuffsManager = FindObjectOfType<BuffsManager>();
    }

    private void OnDestroy()
    {
        GhostsMap.Clear();
        GameState.Dispose();
        RemoveListeners();
    }

    public async void Init(BattleInitializer initializer, BattleBaseUI battleBaseUI)
    {
        _battleInitializer = initializer;
        AddListeners();
        
        GameState.Init();
        BattleBaseUI = battleBaseUI;
        LevelField = initializer.LevelField;

        await UniTask.DelayFrame(1);
        
        SelectPlayerCharacter(Player.LocalPlayer.Team.First());
        BattleBaseUI.Init();
        Raffle();
        BuffsManager.Init(this);
    }

    public void CompleteBattleSimulation() => photonView.RPC(nameof(_CompleteBattleSimulation), RpcTarget.AllViaServer);

    public void LeaveBattle() => photonView.RPC(nameof(LeaveBattleAll), RpcTarget.AllViaServer);

    private void AddListeners()
    {
        GameState.RoundChanged += OnRoundChanged;
        GameState.BattleComplete += OnBattleComplete;
    }

    private void RemoveListeners()
    {
        GameState.RoundChanged -= OnRoundChanged;
        GameState.BattleComplete -= OnBattleComplete;
    }

    private void Raffle()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        
        var evenNumbers = new byte[] {2, 4, 6};
        var oddNumbers = new byte[] {1, 3, 5};
        var all = new [] {evenNumbers, oddNumbers};
        evenNumbers.Shuffle();
        oddNumbers.Shuffle();

        var rand = UnityEngine.Random.Range(0, 1);
        var localPlayerNumbers = all[rand];
        var otherPlayerNumbers = rand == 0 ? all[1] : all[0];

        photonView.RPC(nameof(NotifyRaffleResult), RpcTarget.AllViaServer, localPlayerNumbers, otherPlayerNumbers);
    }
    
    private async void OnRoundChanged()
    {
        if(GameState.CurrentState == States.Over)
            return;
        
        PathsHash = new Dictionary<CharacterView, List<Node>>();
        _spellsHash = new Dictionary<CharacterView, SpellsPlanningPhase.PlannedSpell>();
        
        await UniTask.WaitUntil(() => LevelField != null);
        //TEMP
        //if (GameState.CurrentRoundNumber == 1 && photonView.IsMine)
        //{
            //var node = LevelField.NavigationMap.GetNodeByWorldPosition(character.Model.Position);
            //var nodes = LevelField.NavigationMap.GetRandomNodesOnFieldPart(LevelField.NavigationMap.PlayerNodes,
            // var nodes = new []
            //  {
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(1, 3, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(0, 3, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(-1, 3, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(0, 4, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(-1, 4, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(-2, 5, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(-2, 4, 0)),
            //      LevelField.NavigationMap.GetNodeByPosition(new Vector3Int(-3, 4, 0))
            //  };
            //SpellConfig.AllowedNodesStatesForPlacement.AllExcludeCharacters, 8);
            //foreach (var n in nodes) FieldObjectsEffects.CollapseCell(n);
        //}
        ToNextRoundPhase();
    }

    public void SelectPlayerCharacter(CharacterView character) => SelectedCharacter = character;
    
    public void LocalPlayerPlanningComplete()
    {
        if(!PhotonNetwork.IsConnected)
            return;
        photonView.RPC(nameof(NotifyMasterPlannablePhaseCompleted), RpcTarget.MasterClient);
    }

    private async void ToNextRoundPhase(AbstractRoundPhase<IRoundPhase> prevPhaseController = null)
    {
        if (Player.GetLivingHeroes().Count <= 0)
            return;
        
        if (prevPhaseController == null)
        {
            var c = await CreatePlanningPhase<MovementPlanningPhase>(1);
            CurrentRoundPhaseController = c?.Unwrap<MovementPlanningPhase>();
        }
        else
        {
            switch (prevPhaseController.Type)
            {
                case RoundPhases.MovementPhase:
                    prevPhaseController.CompletePhase();
                    var c = await CreatePlanningPhase<SpellsPlanningPhase>();
                    CurrentRoundPhaseController = c?.Unwrap<SpellsPlanningPhase>();
                    break;
                
                case RoundPhases.SpellsPhase:
                    prevPhaseController.CompletePhase();
                    ClearGhostsMap();
                    CurrentRoundPhaseController = new BattleSimulationPhase(this, BattleConfig);
                    break;
                
                case RoundPhases.BattleSimulationPhase:
                    CurrentRoundPhaseController = null;
                    break;
            }
        }
        
        if(CurrentRoundPhaseController == null)
            return;

        GameState.RoundPhaseChanged?.Invoke(prevPhaseController, CurrentRoundPhaseController);
        //Logger.Log($"#Test Current Round #{GameState.CurrentRoundNumber}, prev controller: {prevPhaseController}," +
        //           $" current controller:{CurrentRoundPhaseController}");
        if(prevPhaseController != null) HandleRoundPhaseData(prevPhaseController, CurrentRoundPhaseController);

        if (GameState.CurrentRoundPhase == RoundPhases.BattleSimulationPhase)
        {
            if(PhotonNetwork.IsMasterClient)
                CurrentRoundPhaseController.Start();
        }
        else CurrentRoundPhaseController.Start();
    }

    private void ClearGhostsMap()
    {
        foreach (var heroGhost in GhostsMap.Keys.ToList())
            BattleSimulator.RemoveObstacle(heroGhost.transform.position);
        GhostsMap.Clear();
    }
    
    private void HandleRoundPhaseData(IRoundPhase prevPhase, IRoundPhase newPhase)
    {
        switch (prevPhase.Type)
        {
            case RoundPhases.MovementPhase:
                var movementPhase = GetConcreteRoundPhase<MovementPlanningPhase>(prevPhase);
                foreach (var character in Player.LocalPlayer.Team)
                    PathsHash.Add(character, movementPhase.GetPlannedPath(character));
                break;
            
            case RoundPhases.SpellsPhase:
                var spellsPhase = GetConcreteRoundPhase<SpellsPlanningPhase>(prevPhase);
                foreach (var character in Player.LocalPlayer.Team)
                    _spellsHash.Add(character, spellsPhase.GetPlannedSpell(character));
                break;
        }

        if (newPhase.Type == RoundPhases.BattleSimulationPhase)
        {
            //DebugOutput.Clear();
            BattleSimulator.Init((BattleSimulationPhase)CurrentRoundPhaseController);
            foreach (var character in Player.LocalPlayer.Team)
            {
                var data = new BattleSimulationExecutionData {Path = PathsHash[character], Spell = _spellsHash[character]};
                BattleSimulator.AddData(character, data);
            }
        }
    }
    
    private async UniTask<T> CreatePlanningPhase<T> (float delay = 0) where T : AbstractRoundPhase<IRoundPhase>
    {
        var phase = (T)Activator.CreateInstance(typeof(T), this, BattleConfig);
        await UniTask.Delay(TimeSpan.FromSeconds(delay));
        return phase;
    }

    private void CurseRandomSpells()
    {
        var config = BattleConfig.GetSpellConfigById("ForbiddenMagicSpell");
        
        var map = LevelField.NavigationMap;
        var enemyNodes = map.GetRandomNodesOnFieldPart(map.EnemyNodes, config.AOESettings.AllowedPlacementOn,
            BattleConfig.NumberCursedCellsAtOnce);
        var playerNodes = map.GetRandomNodesOnFieldPart(map.PlayerNodes, config.AOESettings.AllowedPlacementOn,
            BattleConfig.NumberCursedCellsAtOnce);
        var allNodes = enemyNodes.Concat(playerNodes).ToList();
        
        // Logger.Log($"#Test cell CurseRandomSpells =============================");
        // foreach (var node in allNodes)
        //     Logger.Log($"#Test cell node: {node}");
        // Logger.Log($"#Test cell =============================");
        
        var aoe = new List<AoeCellData>();
        foreach (var node in allNodes)
        {
            aoe.Add(new AoeCellData {ImpactRange = new Vector2(-1, -1), Position = map.GetWorldPositionByNode(node)});
        }

        var spell = new SpellsPlanningPhase.PlannedSpell {GUID = Guid.NewGuid().ToString(), Config = config, AoeCellsData = aoe};
        var context = new ExecutorContext(this, null, spell);
        var curseCellsAction = new SpellActions.CurseOfCellsAction();
        curseCellsAction.InitValues(1);
        curseCellsAction.Apply(context);
    }

    private void OnBattleComplete(Player player)
    {
        GameState.BattleComplete -= OnBattleComplete;
        
        _battleInitializer.ToggleHeroCameras(true);
        BattleBaseUI.gameObject.SetActive(false);
        var screen = Instantiate(BattleConfig.GameOverScreenRef, BattleBaseUI.transform.parent);
        screen.Init(this, player);
    }

    #region Photon
    [PunRPC] private async void NotifyRaffleResult(byte[] localPlayerNumbers, byte[] otherPlayerNumbers)
    {
        var playerTeam = Player.LocalPlayer.Team;
        var enemyTeam = Player.EnemyPlayer.Team;
        if (PhotonNetwork.IsMasterClient)
        {
            for (var i = 0; i < playerTeam.Count; i++)
                playerTeam[i].SetQueueNumber(localPlayerNumbers[i]);
            
            for (var i = 0; i < enemyTeam.Count; i++)
                enemyTeam[i].SetQueueNumber(otherPlayerNumbers[i]);
        }
        else
        {
            await UniTask.WaitUntil(() => BattleBaseUI != null && BattleBaseUI.IsInited);
            
            for (var i = 0; i < playerTeam.Count; i++) playerTeam[i].SetQueueNumber(otherPlayerNumbers[i]);
            for (var i = 0; i < enemyTeam.Count; i++) enemyTeam[i].SetQueueNumber(localPlayerNumbers[i]);
        }
    }

    [PunRPC] private void _CompleteBattleSimulation()
    {
        GameState.BattleSimulationCompleted?.Invoke();
        //TEMP
        // if (GameState.CurrentRoundNumber == 1 && photonView.IsMine)
        // {
        //      var character = Player.GetHeroById("CaptainKidd");
        //      var node = LevelField.NavigationMap.GetNodeByWorldPosition(character.Model.Position);
        //      FieldObjectsEffects.CollapseCell(node);
        // }
        
        if (photonView.IsMine && GameState.CurrentRoundNumber >= BattleConfig.RoundWhenCurseOfCellsBegins)
            UniTask.Delay(TimeSpan.FromSeconds(2f)).ContinueWith(CurseRandomSpells);

        GameState.StartNextRound();
    }
    
    [PunRPC] private void NotifyMasterPlannablePhaseCompleted()
    {
        _numberCompletedPlayers++;
        if (_numberCompletedPlayers < 2) 
            return;
        _numberCompletedPlayers = 0;
        photonView.RPC(nameof(ToNextRoundPhaseRPC), RpcTarget.AllViaServer);
    }

    [PunRPC] private void ToNextRoundPhaseRPC() => ToNextRoundPhase(CurrentRoundPhaseController);

    [PunRPC] private async void LeaveBattleAll()
    {
        Player.Players.Clear();
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();

        await UniTask.Delay(TimeSpan.FromSeconds(1));
        SceneManager.LoadScene(Scenes.GetSceneName(Scenes.ScenesEnum.LobbyScene));
    }
    #endregion

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}