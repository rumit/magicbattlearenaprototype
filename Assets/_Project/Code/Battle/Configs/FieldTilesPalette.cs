﻿
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class FieldTilesPalette : SerializedScriptableObject
{
    public FieldTile[] Tiles;

    public FieldTile GetPlayerDefaultTile()
    {
        return Tiles.FirstOrDefault(t => t.IsPlayerTile && !t.IsSelectedTile);
    }
    
    public FieldTile GetEnemyDefaultTile()
    {
        return Tiles.FirstOrDefault(t => !t.IsPlayerTile && !t.IsSelectedTile);
    }
    
    public FieldTile GetPlayerSelectedTile()
    {
        return Tiles.FirstOrDefault(t => t.IsPlayerTile && t.IsSelectedTile);
    }
    
    public FieldTile GetEnemySelectedTile()
    {
        return Tiles.FirstOrDefault(t => !t.IsPlayerTile && t.IsSelectedTile);
    }
}
