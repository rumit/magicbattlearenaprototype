﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

public class CommonBattleConfig : SerializedScriptableObject
{
    public TeamSettings TeamSettings;
    
    [Space(10)]
    public SerializedDictionary<GameState.RoundPhases, string> RoundPhasesNames;
    
    [Space(10)]
    public SerializedDictionary<GameState.RoundPhases, int> RoundPhasesDuration;
    
    [Space(10)]
    [ReadOnly]
    [SerializeField] private SerializedDictionary<CharConfig, RenderTexture> _heroesPreviewsHash;
    
    [Space(10)] 
    [ReadOnly]
    [SerializeField] private List<SpellConfig> _spells;
    
    [ReadOnly]
    [Space(10)]
    public List<Sprite> EffectsStatusesIcons;
    
    [Space(10)]
    [PreviewField(ObjectFieldAlignment.Left, Height = 50)]
    public Sprite DeathStatusIcon;
    
    [Space(10)] 
    public CrackedCell CrackedCell;

    [Space(10)] public int NumberCursedCellsAtOnce;
    [Space(5)] public int RoundWhenUltSpellsBegin = 4;
    [Space(5)] public int RoundWhenCurseOfCellsBegins = 4;
    
    [Space(15)] public SpellFXUnitView ExplodeTrapFxRef;
    [Space(5)] public GameOverScreen GameOverScreenRef;
    [FormerlySerializedAs("PingRef")] [Space(5)] public Pin PinRef;
    
    public SpellConfig GetSpellConfigById(string id) => _spells.FirstOrDefault(s => s.name == id);
    public RenderTexture GetHeroPreviewTexture(CharConfig config) => _heroesPreviewsHash[config];
    public List<RenderTexture> GetHeroPreviewsTextures() => _heroesPreviewsHash.Values.ToList();
}

[Serializable]
public class CrackedCell
{
    public GameObject TopPart;
    public GameObject CollapsedPlayerCell;
    public GameObject CollapsedEnemyCell;
}

[Serializable]
public class TeamSettings
{
    public Color PlayerTeamColor;
    public Color EnemyTeamColor;
}