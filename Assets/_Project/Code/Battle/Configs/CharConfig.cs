﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

[HideMonoScript]
public class CharConfig : SerializedScriptableObject
{
    public string Name;
    [PreviewField(ObjectFieldAlignment.Left, Height = 170)] public Sprite Avatar;
    [TextArea(0, 10)] public string Description;
    
    [Space(10)]
    public Color ColorId;
    
    [Space(10)]
    public CharacterView View;
    
    [Space(10)]
    public GhostView GhostView;
    
    [Space(10)]
    public float HP;
    
    [Space(10)]
    public int AP;
    
    [Space(10)]
    public SpellConfig[] Spells;
    
    [Space(10)]
    [ReadOnly]
    public SerializedDictionary<SpellConfig, SpellAnimationsIds> SpellAnimationsMap;
    
    public override string ToString()
    {
        return $"[Name: {Name}]";
    }
}