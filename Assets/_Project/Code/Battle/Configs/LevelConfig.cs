﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

[HideMonoScript]
public class LevelConfig : SerializedScriptableObject
{
    //[ReadOnly] 
    public readonly List<CharConfig> LeftTeam = new List<CharConfig>(3);
    //[ReadOnly] 
    public readonly List<CharConfig> RightTeam = new List<CharConfig>(3);
    
    [Space(5)]
    public FieldSize BattleFieldSize;

    [Space(10)]
    public FieldTilesPalette FieldTilesPalette;

    [Serializable]
    public class FieldSize
    {
        public float X = 17;
        public float Y = 13;
        public float HeightReductCoeff = 0.35f;
    }
    
}
