﻿
using UnityEngine;
using UnityEngine.Tilemaps;

public class FieldTile : Tile
{
    [Space(10)]
    public bool IsPlayerTile;
    public bool IsSelectedTile;

    public override string ToString()
    {
        return $"Name: {name}, IsPlayerTile: {IsPlayerTile}, gameObject: {gameObject}";
    }
}
