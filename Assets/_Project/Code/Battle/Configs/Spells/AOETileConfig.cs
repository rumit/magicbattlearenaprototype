﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;

public class AOETileConfig : Tile
{
    [Space(10)] public Vector2 ImpactRange = new Vector2(-1, -1);
}
