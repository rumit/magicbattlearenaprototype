﻿using System;
using System.Collections.Generic;
using LevelField;
using Sirenix.OdinInspector;
using UnityEngine;

[HideMonoScript]
public class SpellConfig : SerializedScriptableObject
{
    public enum ExecuteTypes
    {
        Light,
        Common,
        Ultra
    }
    
    public enum PlacementAreaOfAOE
    {
        Player,
        Enemy,
        Overall,
        Self
    }
    
    public enum AllowedNodesStatesForPlacement
    {
        All,
        Live,
        Collapsed,
        AllExcludeCharacters
    }

    [PreviewField(ObjectFieldAlignment.Left, Height = 100)]
    public Sprite Icon;

    [PropertySpace(5)]
    public string Name;
    
    [PropertySpace(5)]
    public CharConfig Owner;
    
    [PropertySpace(5)]
    [TextArea(0, 10)]public string Description;
    
    [PropertySpace(5)]
    [TableList]
    public StatusIcon StatusEffectIcons;

    [PropertySpace(5)] public ExecuteTypes ExecuteType;

    [PropertySpace(5)]
    public int Cooldown;

    [PropertySpace(5)] public AOEViewSettings AOESettings;
    
    [PropertySpace(10)]
    [TypesDropdown(EnablePresets = true, ExpandAllMenuItems = true)]
    public IExecutor Executor;

    [Serializable]
    public class AOEViewSettings
    {
        [PreviewField(ObjectFieldAlignment.Left, Height = 50)]
        public Sprite Icon;
        
        [TypesDropdown(EnablePresets = true, FlattenTreeView = true, PresetsPath = "_Project/Prefabs/Spells/AOE")]
        public AOELayout Layout;

        public PlacementAreaOfAOE PlacementArea;

        public AllowedNodesStatesForPlacement AllowedPlacementOn;

        [PropertySpace(5)] 
        [HideIf(nameof(PlacementArea), PlacementAreaOfAOE.Self)]
        public bool IsRandom;

        [ShowIf(nameof(IsRandom))] 
        //[HideIf(nameof(PlacementArea), PlacementAreaOfAOE.Enemy)]
        public int CountOnPlayerField;
        
        [ShowIf(nameof(IsRandom))] 
        //[HideIf(nameof(PlacementArea), PlacementAreaOfAOE.Player)]
        public int CountOnEnemyField;

        [PropertySpace(5)] public AOECell CellRef;
        
        [FoldoutGroup("Additional", Expanded = false)]
        [PropertySpace(10)]
        public AdditionalAoeArea AdditionalAreaOnOppositeSide;
    }

    public override string ToString()
    {
        return $"[Name: {Name}]";
    }
    
    public class StatusIcon
    {
        [PreviewField(ObjectFieldAlignment.Left, Height = 50)]
        public Sprite IconForSelf;
        [PreviewField(ObjectFieldAlignment.Left, Height = 50)]
        public Sprite IconForOthers;
    }

    [Serializable]
    public class AdditionalAoeArea
    {
        [TypesDropdown(EnablePresets = true, FlattenTreeView = true, PresetsPath = "_Project/Prefabs/Spells/AOE")]
        public AOELayout Layout;
        public bool IsRandom;
        [ShowIf(nameof(IsRandom))]
        public int Count;
    }
}