﻿using MagicBattleArena;

public class SpellsRestrictionBuff : AbstractHeroBuff, INegativeEffect
{
    public override void Start()
    {
        Tag = BuffsManager.BuffsTags.SpellsRestriction;
        base.Start();
    }
}
