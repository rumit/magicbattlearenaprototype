﻿public class ActionPointsModifier : AbstractHeroBuff, INegativeEffect
{
    private int _actionPointsCount;
    
    public void Start(int actionPointsCount)
    {
        Tag = BuffsManager.BuffsTags.ActionPointsModifier;
        base.Start();
        _actionPointsCount = actionPointsCount;
        BattleController.BattleSimulator.ModifyActionPoints(GetOwner<CharacterView>(), _actionPointsCount);
    }

    public override void End()
    {
        base.End();
        BattleController.BattleSimulator.ModifyActionPoints(GetOwner<CharacterView>(), -_actionPointsCount);
    }
}
