﻿
using System;
using MagicBattleArena;
using static SpellActions.InvisibilityAction;

public class InvisibilityBuff : AbstractHeroBuff
{
    public bool ShowStatusesDuringInvisibility { get; private set; }
    private InvisibilityModes _invisibilityModes;

    public void Start(SpellActions.Conditions endCondition, InvisibilityModes invisibilityModes, bool showStatusesDuringInvisibility)
    {
        Tag = BuffsManager.BuffsTags.Invisibility;
        if(endCondition != SpellActions.Conditions.None)
            _endCondition = endCondition;
        
        _invisibilityModes = invisibilityModes;
        ShowStatusesDuringInvisibility = showStatusesDuringInvisibility;
        
        base.Start();
        
        var owner = GetOwner<CharacterView>();
        
        switch (_invisibilityModes)
        {
            case InvisibilityModes.ForAll:
                owner.SetVisibility(false);
                break;
            case InvisibilityModes.ForEnemies:
                owner.SetVisibilityForEnemies(false);
                break;
        }
    }

    public override void End()
    {
        base.End();
        
        var owner = GetOwner<CharacterView>();
        switch (_invisibilityModes)
        {
            case InvisibilityModes.ForAll:
                owner.SetVisibility(true);
                break;
            case InvisibilityModes.ForEnemies:
                owner.SetVisibilityForEnemies(true);
                break;
        }
        //Logger.Log($"OnDamageTaken: {owner.Config.name}");
        for (var i = 0; i < owner.Buffs.Count; i++)
        {
            var buff = owner.Buffs[i];
            if (buff.Spell.Config == Spell.Config)
            { 
                if(buff != this) buff.End();
            }
        }
    }
}
