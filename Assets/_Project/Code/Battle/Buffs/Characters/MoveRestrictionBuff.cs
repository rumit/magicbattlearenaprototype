﻿public class MoveRestrictionBuff : AbstractHeroBuff, INegativeEffect
{
    private int _damageProRound;
    private SpellsPlanningPhase.PlannedSpell _spell;

    public void Start(IExecutorContext context, int damage)
    {
        Tag = BuffsManager.BuffsTags.MoveRestriction;
        Start();
        
        _spell = context.Spell;
        _damageProRound = damage;
        
        var data = new AttackData
        {
            Target = GetOwner<CharacterView>().Config.name,
            OriginDamageValue = _damageProRound,
            FromActorModifiedDamageValue = _damageProRound,
            ResultDamageValue = _damageProRound,
        };
        BattleController.BattleSimulator.Attack(data);
    }

    public void Start(IExecutorContext context)
    {
        Tag = BuffsManager.BuffsTags.MoveRestriction;
        base.Start();
        _spell = context.Spell;
        //var owner = GetOwner<CharacterView>();
        //var ownerContains = owner.ContainsBuffTag(BuffsManager.BuffsTags.MoveRestriction);
        //context.BattleController.DebugOutput.Print($"MoveRestrictionBuff > {this}, ownerContains: {ownerContains}");
    }

    protected override void OnRoundCanged()
    {
        if (_damageProRound <= 0)
        {
            base.OnRoundCanged();
            return;
        }
        base.OnRoundCanged();

        //var currentRoundNum = BattleController.GameState.CurrentRoundNumber;
        // BattleController.DebugOutput.Print($"Owner: {_owner.Config.name}," +
        //                                    $" currentRoundNum: {currentRoundNum}," +
        //                                    $" startRoundNumber: {_startRoundNumber}," +
        //                                    $" LivetimeInRounds: {LivetimeInRounds}," +
        //                                    $" damageProRound: {_damageProRound}");
        
        if (BattleController.GameState.CurrentRoundNumber - _startRoundNumber < LivetimeInRounds)
        {
            var data = new AttackData {Target = _owner.Config.name, OriginDamageValue = _damageProRound};
            BattleController.BattleSimulator.Attack(data);
        }
    }

    public override void End()
    {
        base.End();
        var hero = GetOwner<CharacterView>();
        hero.StatusEffectRemoved?.Invoke(_spell, hero.Config.name);
    }
}
