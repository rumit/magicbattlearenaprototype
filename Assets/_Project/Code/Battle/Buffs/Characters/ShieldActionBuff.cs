﻿using System;
using Cysharp.Threading.Tasks;
using MagicBattleArena;
using MagicBattleArena.Network;

public class ShieldActionBuff : AbstractHeroBuff
{
    private float _attackerDamageMultiplier;

    public void Start(float attackerDamageMultiplier)
    {
        Tag = BuffsManager.BuffsTags.ShieldAction;
        base.Start();

        _attackerDamageMultiplier = attackerDamageMultiplier;
    }

    protected override async void OnDamageTaken(CharacterView receiver, CharacterView actor, AttackData data)
    {
        if(actor == null || actor.PhotonView.IsMine || data.IgnoreProtectiveEffects)
           return;
        
        var value = data.FromActorModifiedDamageValue * _attackerDamageMultiplier;
       //Logger.Log($"ShieldActionBuff actor: {actor.Config.name}, receiver: {receiver.Config.name}, value: {value}");
       
       var attackData = new AttackData
       {
           Actor = receiver.Config.name,
           Target = actor.Config.name,
           OriginDamageValue = value
       };
       
       await UniTask.Delay(TimeSpan.FromSeconds(1));
       BattleController.BattleSimulator.Attack(attackData);
    }

    public override void End()
    {
        base.End();
        Dispose();
    }
    
    public override void Dispose()
    {
        base.Dispose();
        _owner.DamageTaken -= OnDamageTaken;
    }
}
