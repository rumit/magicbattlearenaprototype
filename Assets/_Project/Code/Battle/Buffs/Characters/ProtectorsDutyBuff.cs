﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class ProtectorsDutyBuff : AbstractHeroBuff
{
    private CharacterView _protector;
    private List<CharacterView> _linkedTargets;

    private float _damageReduction;
    private int _count;
    private float _protectorCalcDamage;
    private Dictionary<CharacterView, IBuff> _targetsMap;
    private float _protectorPureDamage;

    public void Start(Dictionary<CharacterView, IBuff> targetsMap, float damageReduction)
    {
        Tag = BuffsManager.BuffsTags.ProtectorsDutyBuff;
        base.Start();

        _protector = GetOwner<CharacterView>();
        _targetsMap = targetsMap;
        _linkedTargets = targetsMap.Keys.ToList();
        _damageReduction = damageReduction;

        foreach (var hero in _linkedTargets)
            hero.DamageTaken += OnProtectedHeroDamage;
    }

    public override void End()
    {
        base.End();
        Dispose();
    }

    public bool HasLinkedInArea(AttackData data)
    {
        var aoe = data.Aoe;
        if (aoe == null)
            return false;
        
        var targetsPositions = _linkedTargets.Select(t => t.Model.Position);
        foreach (var p in targetsPositions)
        {
            if (aoe.Contains(p))
            {
                _protectorPureDamage = data.ResultDamageValue;
                return true;
            }
        }
        return false;
    }

    private void OnProtectedHeroDamage(CharacterView receiver, CharacterView actor, AttackData data)
    {
        if (_count == 0)
            UniTask.DelayFrame(10).ContinueWith(() => DamageProtector(data, actor, receiver));
        
        _count++;
        //var dmg = !data.IgnoreProtectiveEffects ? data.FromActorModifiedValue : data.ResultValue;
        //BattleController.DebugOutput.Print($"from {actor.Config.name} to {receiver.Config.name}, {data.FromActorModifiedDamageValue}");
        _protectorCalcDamage += Mathf.Round(data.FromActorModifiedDamageValue * _damageReduction);
    }

    private void DamageProtector(AttackData data, CharacterView actor, CharacterView receiver)
    {
        data.ResultDamageValue = _protectorCalcDamage + _protectorPureDamage;
        //BattleController.DebugOutput.Print($"from {actor.Config.name}, " +
         //                                  $"calc: {_protectorCalcDamage}, pure dmg: {_protectorPureDamage}, res: {data.ResultDamageValue}");
        _protector.TakeDamage(data, actor);
        _count = 0;
        _protectorPureDamage = _protectorCalcDamage = 0;
        
         if (_targetsMap.ContainsKey(receiver) && receiver.Model.IsDead)
         {
             receiver.RemoveBuff(_targetsMap[receiver]);
             receiver.DamageTaken -= OnProtectedHeroDamage;
             _targetsMap.Remove(receiver);
         }

         var allTargetsAreDead = _linkedTargets.All(t => t.Model.IsDead);
         //Logger.Log($"DamageProtector {receiver}, is all dead : {allTargetsAreDead }");
         if(allTargetsAreDead) End();
    }

    public override void Dispose()
    {
        base.Dispose();
        foreach (var kvp in _targetsMap)
        {
            var hero = kvp.Key;
            var buff = kvp.Value;
            hero.DamageTaken -= OnProtectedHeroDamage;
            hero.RemoveBuff(buff);
        }
    }
}
