﻿public class SuppressPresenceBuff : AbstractHeroBuff
{
    public override void Start()
    {
        Tag = BuffsManager.BuffsTags.SuppressPresence;
        base.Start();
    }
}