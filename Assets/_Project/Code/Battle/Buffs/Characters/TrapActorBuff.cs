﻿using System.Collections.Generic;

public class TrapActorBuff : AbstractHeroBuff
{
    private List<TrapBuff> _traps;
    
    public void Start(List<TrapBuff> traps)
    {
        Tag = BuffsManager.BuffsTags.TrapActor;
        base.Start();
        _traps = traps;
        foreach (var trap in _traps)
            trap.OnEnd += OnTrapEnd;
    }

    private void OnTrapEnd(TrapBuff trap)
    {
        if (TrapBuff.Traps.Contains(trap))
            trap.OnEnd -= OnTrapEnd;

        if (TrapBuff.Traps.Count <= 0)
            End();
    }

    public override void Dispose()
    {
        base.Dispose();
        foreach (var trap in _traps)
            trap.OnEnd -= OnTrapEnd;
    }
}
