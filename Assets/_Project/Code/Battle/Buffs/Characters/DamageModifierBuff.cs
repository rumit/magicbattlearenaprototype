﻿using UnityEngine;
using static SpellActions;
using Logger = MagicBattleArena.Logger;

public class DamageModifierBuff : AbstractHeroBuff
{
    private DamageModifierData _modifierData;

    public void Start(float value, HPValueTypes valueType , DamageModifierAction.DamageTypes damageType)
    {
        Tag = BuffsManager.BuffsTags.DamageModifier;
        var owner = GetOwner<CharacterView>();
        
        base.Start();
        
        var modifierValue = 0;
        var modifierPercent = 0f;
        if (valueType == HPValueTypes.Percent)
            modifierPercent = value;
        else
            modifierValue  = Mathf.RoundToInt(value);

        _modifierData = new DamageModifierData
        {
            GUID = GUID,
            OwnerID = owner.Config.name,
            SpellID = Spell.Config.name,
            ActorID = Spell.Config.Owner.name,
            DamageType = damageType,
            ValueType = valueType,
            ReductionPercent = modifierPercent,
            ReductionValue = modifierValue
        };
        owner.AddDamageModifier(_modifierData);
    }

    public override void End()
    {
        base.End();
        GetOwner<CharacterView>().RemoveDamageModifier(_modifierData.GUID);
    }
}
