﻿
using System;

public abstract class AbstractBuff : IBuff, IDisposable
{
    public BattleBaseController BattleController { get; set; }
    public int LivetimeInRounds { get; set; }

    public IBuffable Owner { get; set; }
    
    public BuffsManager.BuffsTags Tag { get; set; } = BuffsManager.BuffsTags.None;
    
    public string GUID { get; }

    public T GetOwner<T>() where T : IBuffable
    {
        return (T)Owner;
    }
    
    public virtual void Dispose() { }

    public SpellsPlanningPhase.PlannedSpell Spell { get; set; }
    
    protected int _startRoundNumber;

    protected AbstractBuff()
    {
        GUID = Guid.NewGuid().ToString();
    }

    public virtual void Start()
    {
        _startRoundNumber = BattleController.GameState.CurrentRoundNumber;
        BattleController.GameState.RoundChanged += OnRoundCanged;
    }

    public virtual void End()
    {
        BattleController.GameState.RoundChanged -= OnRoundCanged;
    }
    
    protected virtual void OnRoundCanged()
    {
        if (BattleController.GameState.CurrentRoundNumber - _startRoundNumber == LivetimeInRounds)
            End();
    }
}

public interface IBuffable
{

}
