﻿public interface IBuff
{
    BattleBaseController BattleController { get; set; }
    int LivetimeInRounds { get; set; }
    
    IBuffable Owner { get; set; }
    
    BuffsManager.BuffsTags Tag { get; set; }
    
    public string GUID { get; }
    
    SpellsPlanningPhase.PlannedSpell Spell { get; set; }

    void Dispose();

    T GetOwner<T>() where T : IBuffable;
    
    void Start();
    void End();
}
