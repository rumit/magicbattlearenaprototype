﻿using LevelField;

public class AbstractFieldCellBuff : AbstractBuff
{
    public override void Start()
    {
        base.Start();
        BattleController.BuffsManager.AddFieldCellBuff(GetOwner<Node>(), this);
    }

    public override void End()
    {
        BattleController.BuffsManager.RemoveFieldCellBuff(GetOwner<Node>(), this);
        base.End();
    }
}
