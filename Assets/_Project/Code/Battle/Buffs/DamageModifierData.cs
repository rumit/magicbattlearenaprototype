﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEngine;

[Serializable]
public class DamageModifierData
{
    public static float ApplyModifier(DamageModifierData data, float originValue)
    {
        var result = originValue;
        //HPValueTypes.TryParse(data.ValueType, out HPValueTypes valueType);
        //DamageModifierAction.DamageTypes.TryParse(data.DamageType, out DamageModifierAction.DamageTypes damageType);
        
        if (data.ValueType == SpellActions.HPValueTypes.Percent)
        {
            switch (data.DamageType)
            {
                case SpellActions.DamageModifierAction.DamageTypes.TakenDamageReducer:
                case SpellActions.DamageModifierAction.DamageTypes.DamageDoneReducer:
                    result = originValue * data.ReductionPercent;
                    break;
                case SpellActions.DamageModifierAction.DamageTypes.TakenDamageMultiplier:
                case SpellActions.DamageModifierAction.DamageTypes.DamageDoneMultiplier:
                    result = originValue * data.ReductionPercent;
                    break;
            }
        }
        else
        {
            switch (data.DamageType)
            {
                case SpellActions.DamageModifierAction.DamageTypes.TakenDamageReducer:
                case SpellActions.DamageModifierAction.DamageTypes.DamageDoneReducer:
                    result = data.ReductionValue;
                    break;
                case SpellActions.DamageModifierAction.DamageTypes.TakenDamageMultiplier:
                case SpellActions.DamageModifierAction.DamageTypes.DamageDoneMultiplier:
                    result = data.ReductionValue;
                    break;
            }
        }
        return Mathf.Round(result);
    }
    
    
    public string GUID;
    public string SpellID;
    public string ActorID;
    public string OwnerID;
    public int ReductionValue;
    public float ReductionPercent;
    
    [JsonConverter(typeof(StringEnumConverter))]
    public SpellActions.HPValueTypes ValueType;
    
    [JsonConverter(typeof(StringEnumConverter))]
    public SpellActions.DamageModifierAction.DamageTypes DamageType;

    public override string ToString()
    {
        return $"GUID: {GUID}," +
               $" SpellID: {SpellID}," +
               $" ActorID: {ActorID}," +
               $" OwnerID: {OwnerID}," +
               $" ReductionValue: {ReductionValue}," +
               $" ReductionPercent: {ReductionPercent}," +
               $" ValueType: {ValueType}," +
               $" DamageType: {DamageType}";
    }
}


