﻿using System;
using System.Collections.Generic;
using LevelField;
using MagicBattleArena;

public class TrapBuff: AbstractFieldCellBuff
{
    public static readonly List<TrapBuff> Traps = new List<TrapBuff>();

    public Action<TrapBuff> OnEnd;
        
    public void Start(SpellActions.TrapsAction.TrapConfig trapConfig)
    {
        base.Start();
        
        if(!Traps.Contains(this)) Traps.Add(this);
        BattleController.FieldObjectsEffects.SetTrap(GetOwner<Node>(), trapConfig);
    }

    public override void End()
    {
        base.End();
        
        if(Traps.Contains(this))Traps.Remove(this);
        OnEnd?.Invoke(this);
        
        //Logger.Log($"TrapBuff End: {this}");
        var pos = BattleController.LevelField.NavigationMap.GetWorldPositionByNode(GetOwner<Node>());
        BattleController.FieldObjectsEffects.RemoveTrap(pos, Spell.GUID);
    }

    public override void Dispose()
    {
        base.Dispose();
        //Logger.Log($"Traps.Count: {Traps.Count}");
        if(Traps.Count > 0)
            Traps.Clear();
    }
}
