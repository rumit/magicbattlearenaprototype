﻿using LevelField;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class CursedCellsBuff : AbstractFieldCellBuff
{
    private Node _node;

    public override void Start()
    {
        base.Start();
        
        _node = GetOwner<Node>();
        BattleController.FieldObjectsEffects.CurseCell(_node);
        BattleController.GameState.BattleSimulationCompleted += OnBattleSimulationCompleted;
    }

    private void OnBattleSimulationCompleted()
    {
        if (BattleController.GameState.CurrentRoundNumber - _startRoundNumber == LivetimeInRounds)
            End();
        // BattleController.DebugOutput.Print($"CursedCell CurrentRoundNumber:" +
        //                                    $" {BattleController.GameState.CurrentRoundNumber}," +
        //                                    $" LivetimeInRounds: {LivetimeInRounds}," +
        //                                    $" StartRoundNumber: {_startRoundNumber} buff: {this}");
    }

    protected override void OnRoundCanged() { }

    public override void End()
    {
        base.End();

        BattleController.GameState.BattleSimulationCompleted -= OnBattleSimulationCompleted;
        BattleController.FieldObjectsEffects.CollapseCell(_node);
    }
}