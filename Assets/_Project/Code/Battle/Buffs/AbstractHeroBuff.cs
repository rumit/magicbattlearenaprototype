﻿using System;
using MagicBattleArena;

public abstract class AbstractHeroBuff : AbstractBuff
{
    protected SpellActions.Conditions _endCondition;
    private bool _needCheckSelfDamage;
    protected CharacterView _owner;

    public override void Start()
    {
        _owner = GetOwner<CharacterView>();
        if (_owner == null || _owner.ContainsBuffTag(BuffsManager.BuffsTags.NoEffectsAccept))
            return;
        
        base.Start();
        
        _owner.DamageTaken += OnDamageTaken;
        BattleController.BuffsManager.AddHeroBuff(_owner, this);

        switch (_endCondition)
        {
            case SpellActions.Conditions.None:
                break;
            case SpellActions.Conditions.SelfDamageTaken:
                _needCheckSelfDamage = true;
                break;
        }
    }

    protected virtual void OnDamageTaken(CharacterView receiver, CharacterView actor, AttackData data)
    {
        if(_needCheckSelfDamage || _owner.Model.IsDead)
            End();
    }

    public override void End()
    {
        _owner.DamageTaken -= OnDamageTaken;
        
        var key = SpellsFxController.GetFXViewKey(Spell.Config, Spell.GUID);
        if (_owner.Config.name == "SirIronhammer")
        {
            Logger.Log($"#Test Buff End {_owner.Config.name} key: {key}");
        }
        
        BattleController.SpellsFxController.TryRemoveFXView(key);
        BattleController.BuffsManager.RemoveHeroBuff(_owner, this);
        base.End();
    }
    
    public override string ToString()
    {
        return $"[Tag: {Tag}, Owner: {GetOwner<CharacterView>().Config.name}, Spell: {Spell.Config.name}," +
               $" Actor: {Spell.Config.Owner.name}]";
    }
}

public interface INegativeEffect { }
