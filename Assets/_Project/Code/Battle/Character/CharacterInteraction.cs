﻿using System;
using System.Collections.Generic;
using MagicBattleArena.Network;
using TouchScript.Gestures;
using UnityEngine;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class CharacterInteraction : MonoBehaviour
{
    #region Static
    private static CharacterInteraction _selected;
    private static CharacterInteraction _highlighted;
    
    public static void DeselectCurrent()
    {
        if (_selected != null)
            _selected.Deselect();
    }
    #endregion
    

    [SerializeField] private GameObject _selectionView;
    
    public bool IsSelected { get; private set; }
    private TapGesture _tapGesture;
    private CharacterView _owner;
    private BattleBaseController _battleController;

    private void OnDestroy()
    {
        _tapGesture.Tapped -= OnTapped;
        _owner.SelectCharacterRequest -= OnSelectCharacterRequest;
    }

    private void Awake()
    {
        _owner = GetComponent<CharacterView>();
        
        _selectionView.SetActive(false);
        _tapGesture = GetComponentInChildren<TapGesture>();
        _tapGesture.Tapped += OnTapped;
        
        var selectionSprite = _selectionView.GetComponent<SpriteRenderer>();
        var a = selectionSprite.color.a;
        var col = _owner.Config.ColorId;
        selectionSprite.color = new Color(col.r, col.g, col.b, a);
    }

    public void Init(BattleBaseController battleController)
    {
        _battleController = battleController;
        _owner.SelectCharacterRequest += OnSelectCharacterRequest;
    }

    public void Disable()
    {
        _tapGesture.Tapped -= OnTapped;
        var collider = _tapGesture.gameObject.GetComponent<Collider>();
        collider.enabled = false;
    }

    private void OnSelectCharacterRequest(CharacterView character)
    {
        if(!character.PhotonView.IsMine)
            return;
        
        if (_selected != null && this != _selected)
            _selected.Deselect();

        if (this != _selected && !IsSelected)
            Select();
        
        _battleController.SelectCharacter?.Invoke(_owner);
    }
    
    public void ToggleHighlighting(bool value)
    {
        if (_highlighted != null)
            _highlighted.DisableHighlighting();

        if (value)
            EnableHighlighting();
        else
            DisableHighlighting();
    }

    private void EnableHighlighting()
    {
        _selectionView.SetActive(true);
        _highlighted = this;
    }
    
    private void DisableHighlighting()
    {
        _selectionView.SetActive(false);
        _highlighted = null;
    }

    public void Select()
    {
        if (_highlighted != null)
            _highlighted.DisableHighlighting();

        _selectionView.SetActive(true);
        _selected = this;
        IsSelected = true;
    }

    private void Deselect()
    {
        _selectionView.SetActive(false);
        _selected = null;
        IsSelected = false;
    }

    private void OnTapped(object sender, EventArgs e)
    {
        if(!_owner.PhotonView.IsMine || _battleController.GameState.CurrentRoundPhase == GameState.RoundPhases.BattleSimulationPhase)
            return;
        OnSelectCharacterRequest(_owner);
    }
}