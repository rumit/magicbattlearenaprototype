﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Photon.Pun;
using Selectors;
using UnityEngine;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class CharacterMovement : MonoBehaviour, IPunObservable
{
    private const float _velocity = 1f;
    
    public bool InMovement { get; private set; }
    
    private LevelFieldView _levelField;
    private CharacterView _owner;
    private Animator _animator;
    private NavigationMap _navigationMap;
    private PhotonView _photonView;
    private Transform _body;
    private Queue<Vector3> _pointsQueue;
    private ChangingHealthDisplay _changingHealthDisplay;
    private StatusEffectDisplay _statusEffectDisplay;
    
    private Action _movementCompleteCallback;

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
        _body = _animator.transform;
        _photonView = GetComponent<PhotonView>();
    }

    public void Init(CharacterView owner, Vector3 position)
    {
        _owner = owner;
        _changingHealthDisplay = _owner.ChangingHealthDisplay;
        _statusEffectDisplay = _owner.StatusEffectDisplay;
        _levelField = _owner.LevelField;
        _navigationMap = _levelField.NavigationMap;
        
        SetPosition(position);
        _navigationMap.SetObstacle(_owner, position);
        LookAt(Vector3.zero);
    }

    public void HandleDie()
    {
        if(!InMovement)
            return;
        ToggleMovementState(false);
    }

    public void MoveTo(List<LevelField.Node> path, Action callback)
    {
        _levelField.BattleController.BattleSimulator.RemoveObstacle(transform.position);

        _movementCompleteCallback = callback;
        _pointsQueue = new Queue<Vector3>(path.Count);
        for (var i = 0; i < path.Count; i++)
            _pointsQueue.Enqueue(_navigationMap.GetWorldPositionByNode(path[i]));

        ToggleMovementState(true);
        _animator.SetTrigger(BaseAnimations.Walk.ToString());
        _photonView.RPC(nameof(AnimateOtherWalking), RpcTarget.Others, BaseAnimations.Walk.ToString());
        MoveAlongThePath(_pointsQueue.Dequeue());
    }
    
    private void MoveAlongThePath(Vector3 next)
    {
        LookAt(next);
        transform.DOLocalMove(next, _velocity)
            .SetEase(Ease.Linear)
            .OnUpdate(() =>
            {
                _changingHealthDisplay.LookAtFront();
                _statusEffectDisplay.LookAtFront();
            })
            .OnComplete(async() =>
            {
                SetPosition(transform.position);
                if (IsTrapExists() == false)
                {
                    if (_pointsQueue.Count > 0)
                    {
                        // handle last point of path
                        if (_pointsQueue.Count == 1 && _navigationMap.IsPositionOccupied(_owner, _pointsQueue.First()))
                        {
                            ToggleMovementState(false);
                            await UniTask.Delay(TimeSpan.FromSeconds(.2f));
                            LookAt(Vector3.zero);
                        }
                        else
                            MoveAlongThePath(_pointsQueue.Dequeue());
                    }
                    else // movement completed
                    {
                        ToggleMovementState(false);
                        await UniTask.Delay(TimeSpan.FromSeconds(.2f));
                        LookAt(Vector3.zero);
                    }
                } // trapped
                else
                {
                    ToggleMovementState(false, true);
                    await UniTask.Delay(TimeSpan.FromSeconds(.2f));
                    LookAt(Vector3.zero);
                }
            });
    }

    private void ToggleMovementState(bool value, bool isTrapped = false)
    {
        InMovement = value;
        _animator.SetBool(nameof(InMovement), value);
        if (!value && !isTrapped)
        {
            _animator.SetTrigger(BaseAnimations.Idle.ToString());
            _photonView.RPC(nameof(AnimateOtherWalking), RpcTarget.Others, BaseAnimations.Idle.ToString());
            _movementCompleteCallback?.Invoke();
        }
    }

    private bool IsTrapExists()
    {
        var node = _navigationMap.GetNodeByWorldPosition(transform.position);
        if (node.Trap != null)
        {
            var moveBanAction = new SpellActions.MovementRestrictionAction
            {
                Target = _owner,
                Duration = node.Trap.MoveRestrictionDuration,
                DamageProRound = node.Trap.DamageProRound
            };
            
            var context = new ExecutorContext(_levelField.BattleController, _owner, node.Trap.Spell);
            moveBanAction.Apply(context);
            if(_pointsQueue.Count > 0)
                _levelField.BattleController.BattleSimulator.RemoveObstacle(_pointsQueue.Last());
            
            _levelField.BattleController.FieldObjectsEffects.ExplodeTrap(transform.position);
            _movementCompleteCallback?.Invoke();
            return true;
        }
        return false;
    }

    public void SetPosition(Vector3 point)
    {
        var pos = _navigationMap.ConvertToCenterCell(point);
        //Logger.Log($"SetPosition point: {point}, worldPos: {worldPos}, locPos: {locPos}");
        transform.position = _owner.Model.Position = pos;
    }

    public void Teleport(Vector3 point)
    {
        SetPosition(point);
        _body.DOLookAt(Vector3.zero, .2f);
    }
    
    private void LookAt(Vector3 point)
    {
        _photonView.RPC(nameof(LookAtOther), RpcTarget.Others, point);
        _body.DOLookAt(point, .2f);
    }
    
    [PunRPC]
    public void LookAtOther(Vector3 point)
    {
        _body.DOLookAt(point, .2f);
    }

    [PunRPC]
    public void AnimateOtherWalking(string animationType)
    {
        _animator.SetTrigger(animationType);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}