﻿using System;
using TouchScript.Examples.Cube;
using UnityEngine;

public class GhostView : MonoBehaviour
{
    private Material _material;

    private void Awake()
    {
        _material = GetComponentInChildren<SkinnedMeshRenderer>().material;
    }

    public void Init(CharConfig config)
    {
        var colorId = config.ColorId;
        _material.SetColor(ShadersConstants.BaseColor, new Color(colorId.r, colorId.g, colorId.b, _material.color.a));
        //_material.SetColor(ShadersConstants.BaseColor, config.ColorId);
    }
}