﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using MagicBattleArena.Network;
using Photon.Pun;
using TMPro;
using UnityEngine;
using Zenject;
using Logger = MagicBattleArena.Logger;

public class ChangingHealthDisplay : MonoBehaviour, IPunObservable
{
    private const float _topPos = 4;
    private static readonly Color increaseHealthColor = new Color(0, 1, 0, 1);
    private static readonly Color decreaseHealthColor = new Color(1, 0, 0, 1);

    private Camera _camera;
    private Vector3 _defaultPosition;
    private TextMeshPro _textField;
    private PhotonView _photonView;
    private TweenerCore<Vector3, Vector3, VectorOptions> _positionTweener;
    private TweenerCore<Color, Color, ColorOptions> _alphaTweener;
    private CancellationTokenSource _cancellation;
    private CharacterView _owner;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _defaultPosition = transform.localPosition;
        _textField = GetComponent<TextMeshPro>();
        
        _photonView.RPC(nameof(DoFade), RpcTarget.AllViaServer, 0f, 0f, 0f);
    }

    public void Init(Camera camera, CharacterView owner)
    {
        _camera = camera;
        _owner = owner;
        LookAtFront();
    }

    public void Show(float value, float oldHPValue)
    {
        if (value > 0)
        {
            var sum = oldHPValue + value;
            if (sum > _owner.Config.HP)
                value -= sum - _owner.Config.HP;
        }
        
        _photonView.RPC(nameof(DoFade), RpcTarget.AllViaServer, 1f, .4f, value);
        
        _cancellation?.Cancel();
        _positionTweener?.Kill();
        transform.localPosition = _defaultPosition;
        _positionTweener = transform.DOLocalMoveY(_topPos, 1)
            .OnComplete(() =>
            {
                _cancellation = new CancellationTokenSource();
                UniTask.Delay(TimeSpan.FromSeconds(5), cancellationToken: _cancellation.Token)
                    .ContinueWith(() =>
                    {
                        _photonView.RPC(nameof(DoFade), RpcTarget.AllViaServer, 0f, 0f, 0f);
                        transform.localPosition = _defaultPosition;
                    });
            });
    }

    public void LookAtFront()
    {
        transform.LookAt(_camera.transform.position);
        var currEuler = transform.rotation.eulerAngles; 
        transform.eulerAngles = new Vector3(currEuler.x, 180, 0);
    }
    
    [PunRPC]
    private void DoFade(float alphaValue, float duration, float value)
    {
        if(value >= 0 && _owner != null &&  _owner.ContainsBuffTag(BuffsManager.BuffsTags.SuppressPresence) && !_owner.PhotonView.IsMine)
            return;
        
        if (alphaValue > 0)
        {
            _textField.text = value < 0 ? $"{Mathf.Round(value)}" : $"+{Mathf.Round(value)}";
            _textField.color = value < 0 ? decreaseHealthColor : increaseHealthColor;
        }
        
        _alphaTweener.Kill();
        if (duration > 0 && _textField.alpha > 0)
            _textField.alpha = 0;
        _alphaTweener = _textField.DOFade(alphaValue, duration);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}
