﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Photon.Pun;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class StatusEffectDisplay : MonoBehaviour, IPunObservable
{
    private List<Sprite> _icons;
    private PhotonView _photonView;
    private CharacterView _hero;
    private SpriteRenderer _renderer;
    private CancellationTokenSource _cancelToken;
    private Camera _camera;
    private TweenerCore<Vector3, Vector3, VectorOptions> _scaleTweener;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _renderer = GetComponent<SpriteRenderer>();
        transform.localScale = Vector3.zero;
    }
    
    private void OnDestroy()
    {
        _hero.StatusEffectAdded -= OnBuffAdded;
        _cancelToken?.Cancel();
        _scaleTweener?.Kill();
    }

    public void Init(Camera camera, CharacterView hero)
    {
        _camera = camera;
        _icons = hero.LevelField.BattleController.BattleConfig.EffectsStatusesIcons;
        _hero = hero;
        LookAtFront();
        
        _hero.StatusEffectAdded += OnBuffAdded;
    }
    
    public void LookAtFront()
    {
        transform.LookAt(_camera.transform.position);
        var currEuler = transform.rotation.eulerAngles; 
        transform.eulerAngles = new Vector3(currEuler.x, 180, 0);
    }

    private void OnBuffAdded(SpellsPlanningPhase.PlannedSpell spell, string heroId, Sprite icon)
    {
        _photonView.RPC(nameof(SetStatusIcon), RpcTarget.AllViaServer, icon.name);
    }

    [PunRPC]
    private void SetStatusIcon(string iconName)
    {
        if(_hero.ForbidDisplayStatusEffectIcons)
            return;

        _cancelToken?.Cancel();
        _scaleTweener?.Kill();
        
        if(transform.localScale.x > 0)
            transform.localScale = Vector3.zero;
        
        var icon = _icons.FirstOrDefault(i => i.name == iconName);
        _renderer.sprite = icon;

        _scaleTweener = transform.DOScale(0.6f, 1f)
            .SetEase(Ease.OutElastic)
            .OnComplete(() =>
            {
                _cancelToken = new CancellationTokenSource();
                UniTask.Delay(TimeSpan.FromSeconds(5), cancellationToken:_cancelToken.Token)
                    .ContinueWith(() =>
                    {
                        transform.DOScale(0, 0.4f);
                    });
            });

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}