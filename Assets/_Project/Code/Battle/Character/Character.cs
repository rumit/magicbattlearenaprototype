﻿using System;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class Character
{
    public Vector3 Position { get; set; }
    public float HP { get; private set; }
    public int AP { get; private set; }
    public bool IsDead { get; private set; }
    public float TotalDamage { get; private set; }
    
    private CharConfig _сonfig;

    public Character(CharConfig config)
    {
        _сonfig = config;
        //HP = Mathf.Round(_сonfig.HP - _сonfig.HP * .7f) ;// Temp
        HP = _сonfig.HP;
        AP = _сonfig.AP;
    }

    public void ModifyActionPoints(int value)
    {
        AP += value;
        AP = Mathf.Clamp(AP, 0, _сonfig.AP);
    }

    public void IncreaseHealth(float value)
    {
        HP += value;
        HP = Mathf.Clamp(HP, 0, _сonfig.HP);
    }

    public void TakeDamage(float value)
    {
        HP -= value;
        TotalDamage += value;
        HP = Mathf.Clamp(HP, 0, _сonfig.HP);
        if (HP <= 0)
            IsDead = true;
    }

    public bool IsDamageFatal(float value)
    {
        var tmp = HP;
        return tmp - value <= 0;
    }

    public void SetDeadState() => IsDead = true;

    public override string ToString()
    {
        return $"[Name: {_сonfig.Name}, Position: {Position}, HP: {HP}, MaxHP: {_сonfig.HP}, AP:{AP}]";
    }
}
