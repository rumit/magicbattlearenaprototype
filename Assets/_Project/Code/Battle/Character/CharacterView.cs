﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using MagicBattleArena.Network;
using Newtonsoft.Json;
using Photon.Pun;
using UnityEngine;
using static SpellActions.DamageModifierAction;
using Logger = MagicBattleArena.Logger;

public class CharacterView : MonoBehaviour, IPunObservable, IPunInstantiateMagicCallback, IObstacle, IBuffable
{
    public Action<byte> RaffleAction;
    public Action<byte> UpdateQueueNum;
    public Action<CharacterView> SelectCharacterRequest;
    public Action CellUnderfootCollapsed;
    public Action<SpellsPlanningPhase.PlannedSpell, string, Sprite> StatusEffectAdded;
    public Action<SpellsPlanningPhase.PlannedSpell, string> StatusEffectRemoved;
    public Action<CharacterView, float, float> HealthUpdated;
    public Action<CharacterView, CharacterView, AttackData> DamageTaken;

    public readonly List<IBuff> Buffs = new List<IBuff>();
    public readonly Dictionary<string, BuffsManager.BuffsTags> BuffTags = new Dictionary<string, BuffsManager.BuffsTags>();
    public readonly Dictionary<string, DamageModifierData> DamageModifiersMap = new Dictionary<string, DamageModifierData>();

    public CharConfig Config;
    public LevelFieldView LevelField { get; private set; }
    public CharacterMovement Movement { get; private set; }
    public CharacterInteraction Interactor { get; private set; }
    public ChangingHealthDisplay ChangingHealthDisplay { get; private set; }
    public StatusEffectDisplay StatusEffectDisplay { get; private set; }
    public PhotonView PhotonView { get; private set; }
    public Character Model { get; private set; }

    [NonSerialized] public bool ReachableCellsShown;
    public bool ForbidDisplayStatusEffectIcons { get; private set; }
    public Animator Animator { get; private set; }
    public byte QueueNumber { private set; get; }

    private BattleBaseController _battleController;
    private bool _needFallingAtDeath;

    private void Awake()
    {
        Animator = GetComponentInChildren<Animator>();
        PhotonView = GetComponent<PhotonView>();
        ChangingHealthDisplay = GetComponentInChildren<ChangingHealthDisplay>();
        StatusEffectDisplay = GetComponentInChildren<StatusEffectDisplay>();
    }

    private void OnDestroy()
    {
        if(_battleController == null)
            return;
        
        _battleController.GameState.RoundChanged -= OnRoundChanged;
        HealthUpdated -= OnHealthUpdated;
        CellUnderfootCollapsed -= OnCellUnderfootCollapsed;
    }

    public void Init(LevelFieldView levelField, Vector3 position, Camera unitsCamera)
    {
        LevelField = levelField;
        _battleController = LevelField.BattleController;
        ChangingHealthDisplay.Init(unitsCamera, this);

        Model = new Character(Config);
        Movement = GetComponent<CharacterMovement>();
        Movement.Init(this, position);
        LevelField.NavigationMap.SetObstacle(this, transform.position);
        
        StatusEffectDisplay.Init(unitsCamera, this);
        
        Interactor = GetComponent<CharacterInteraction>();
        Interactor.Init(_battleController);
        AddListeners();
    }

    // not local player
    private async void Init()
    {
        LevelField = FindObjectOfType<LevelFieldView>();
        Player.EnemyPlayer.AddTeamMember(this);
        
        await UniTask.WaitUntil(() => LevelField.NavigationMap != null);

        Movement = GetComponent<CharacterMovement>();
        Model = new Character(Config);
        Movement.Init(this, transform.position);
        
        Interactor = GetComponent<CharacterInteraction>();
        Interactor.Init(LevelField.BattleController);

        LevelField.NavigationMap.SetObstacle(this, transform.position);
        _battleController = LevelField.BattleController;
        ChangingHealthDisplay.Init(LevelField.UnitsCamera, this);
        StatusEffectDisplay.Init(LevelField.UnitsCamera, this);

        AddListeners();
    }
    
    private void AddListeners()
    {
        HealthUpdated += OnHealthUpdated;
        _battleController.GameState.RoundChanged += OnRoundChanged;
        CellUnderfootCollapsed += OnCellUnderfootCollapsed;
    }

    #region DamageModifier
    public void AddDamageModifier(DamageModifierData modifier)
    {
        var sdata = JsonConvert.SerializeObject(modifier);
        PhotonView.RPC(nameof(_AddDamageModifier), RpcTarget.AllViaServer, sdata);
    }
    
    public void RemoveDamageModifier(string key)
    {
        PhotonView.RPC(nameof(_RemoveDamageModifier), RpcTarget.AllViaServer, key);
    }

    public List<DamageModifierData> TryGetDamageModifierByType(DamageTypes type)
    {
        return DamageModifiersMap.Where(m => m.Value.DamageType == type).Select(v => v.Value).ToList();
    }
    #endregion
    
    public void AddBuff(IBuff buff)
    {
        if(Model.IsDead)
            return;

        if (buff.Tag != BuffsManager.BuffsTags.None)
            PhotonView.RPC(nameof(AddBuffTag), RpcTarget.AllViaServer, $"{buff.Spell.GUID}_{buff.Tag}", buff.Tag.ToString());

        if (buff is InvisibilityBuff invisibilityBuff && !invisibilityBuff.ShowStatusesDuringInvisibility)
            PhotonView.RPC(nameof(SetForbidDisplayStatusEffectIcons), RpcTarget.AllViaServer, true);

        Buffs.Add(buff);
    }

    public void RemoveBuff(IBuff buff)
    {
        if(!PhotonNetwork.IsConnected)
            return;
        
        StatusEffectRemoved?.Invoke(buff.Spell, Config.name);
        if (buff is InvisibilityBuff)
            PhotonView.RPC(nameof(SetForbidDisplayStatusEffectIcons), RpcTarget.AllViaServer, false);
        
        if (buff.Tag != BuffsManager.BuffsTags.None)
            PhotonView.RPC(nameof(RemoveBuffTag), RpcTarget.AllViaServer, $"{buff.Spell.GUID}_{buff.Tag}");

        if(Buffs.Contains(buff))
            Buffs.Remove(buff);

        var key = DamageModifiersMap.FirstOrDefault(m => m.Key == buff.GUID).Key;
        if(key != null)
            RemoveDamageModifier(key);
    }

    public bool ContainsBuffTag(BuffsManager.BuffsTags tag) => BuffTags.ContainsValue(tag);

    public T TryGetBuff<T>() where T : IBuff => (T) Buffs.FirstOrDefault(b => b is T);
    
    public void IncreaseHealthByPercent(float value)
    {
        var oldHPValue = Model.HP;
        var updateValue = Model.HP * value;
        Model.IncreaseHealth(updateValue);
        HealthUpdated?.Invoke(this, Mathf.Round(updateValue), oldHPValue);
    }
    
    public void IncreaseHealthByValue(float value)
    {
        var oldHPValue = Model.HP;
        Model.IncreaseHealth(value);
        HealthUpdated?.Invoke(this, Mathf.Round(value), oldHPValue);
    }
    
    public void ModifyActionPoints(int value) => Model.ModifyActionPoints(value);

    public void TakeDamage(AttackData data, CharacterView actor = null)
    {
        if(Model.IsDead)
            return;

        var oldHPValue = Model.HP;
        var resultValue = data.ResultDamageValue;

        if(resultValue <= 0)
            return;
        
        if (ContainsBuffTag(BuffsManager.BuffsTags.NoEffectsAccept) && !Model.IsDamageFatal(resultValue))
        {
            ChangingHealthDisplay.Show(-0.01f, oldHPValue);
            return;
        }

        Model.TakeDamage(resultValue);
        resultValue = -Mathf.Round(resultValue);
        //Logger.Log($"TakeDamage hero: {Config.name}, origin: {-value}, result: {resultValue}");
        DamageTaken?.Invoke(this, actor, data);
        PlayAnimation(BaseAnimations.Damage.ToString());
        HealthUpdated?.Invoke(this, resultValue, oldHPValue);
    }

    public void PlayAnimation(string animName)
    {
        Animator.SetTrigger(animName);
        PhotonView.RPC(nameof(PlayAnimationAtOtherPlayer), RpcTarget.Others, Config.name, animName);
    }

    public void SetVisibility(bool value)
    {
        Animator.gameObject.SetActive(value);
        Animator.speed = value ? 1 : 0;
        SetVisibilityForEnemies(value);
    }
    
    public void SetVisibilityForEnemies(bool value)
    {
        PhotonView.RPC(nameof(SetVisibilityAtOther), RpcTarget.Others, Config.name, value);
    }

    public void SetQueueNumber(byte number)
    {
        QueueNumber = number;
        RaffleAction?.Invoke(QueueNumber);
    }

    public SpellConfig GetSpellConfigById(string id) => Config.Spells.FirstOrDefault(s => s.name == id);
    
    public void ApplyFallingDown(Vector3 pos) => PhotonView.RPC(nameof(_ApplyFallingDown), RpcTarget.AllViaServer, pos);

    private void OnCellUnderfootCollapsed()
    {
        CellUnderfootCollapsed -= OnCellUnderfootCollapsed;
        _needFallingAtDeath = true;
        if(PhotonView.IsMine)
            TakeDamage(new AttackData {Target = Config.name, ResultDamageValue = 10000 });
    }
    
    private void OnHealthUpdated(CharacterView hero, float updateValue, float oldHPValue)
    {
        if(Mathf.Abs(updateValue) <= Config.HP)
            ChangingHealthDisplay.Show(updateValue, oldHPValue);

        if (Model.IsDead) PhotonView.RPC(nameof(Die), RpcTarget.AllViaServer);
    }

    private void UpdateQueueNumber()
    {
        if(_battleController.GameState.CurrentRoundNumber == 1)
            return;
        
        if (QueueNumber < 6)
            QueueNumber += 1;
        else
            QueueNumber = 1;
        UpdateQueueNum?.Invoke(QueueNumber);
    }
    
    private void OnRoundChanged() => UpdateQueueNumber();

    #region Photon
    [PunRPC] private void _AddDamageModifier(string modifierStr)
    {
        var modifier = JsonConvert.DeserializeObject<DamageModifierData>(modifierStr);
        if (!DamageModifiersMap.ContainsKey(modifier.GUID))
            DamageModifiersMap.Add(modifier.GUID, modifier);
    }
    
    [PunRPC] private void _RemoveDamageModifier(string key)
    {
        if (DamageModifiersMap.ContainsKey(key))
            DamageModifiersMap.Remove(key);
    }

    [PunRPC] private void AddBuffTag(string guid, string value)
    {
        if(!BuffTags.ContainsKey(guid) && BuffsManager.BuffsTags.TryParse(value, true, out BuffsManager.BuffsTags enumValue))
            BuffTags.Add(guid, enumValue);
    }
    
    [PunRPC] private void RemoveBuffTag(string guid)
    {
        if(BuffTags.ContainsKey(guid)) BuffTags.Remove(guid);
    }

    [PunRPC] private void SetVisibilityAtOther(string id, bool value)
    {
        var other = Player.GetHeroById(id);
        other.Animator.gameObject.SetActive(value);
        other.Animator.speed = value ? 1 : 0;
    }

    [PunRPC] private void PlayAnimationAtOtherPlayer(string charId, string animName)
    {
        var actor = Player.GetHeroById(charId);
        actor.Animator.SetTrigger(animName);
    }
    
    [PunRPC] private void SetForbidDisplayStatusEffectIcons(bool value) => ForbidDisplayStatusEffectIcons = value;
    
    [PunRPC] private async void Die()
    {
        LevelField.NavigationMap.RemoveObstacle(transform.position);
        UniTask.Delay(TimeSpan.FromSeconds(2))
            .ContinueWith(() => _battleController.BuffsManager.FinishAllBuffsAtHero(this));

        if (!PhotonView.IsMine)
            Model.SetDeadState();
        else
        {
            Interactor.Disable();
            Movement.HandleDie();
            HealthUpdated -= OnHealthUpdated;
            if (_needFallingAtDeath)
            {
                _needFallingAtDeath = false;
                PlayAnimation(BaseAnimations.Fall.ToString());
                await UniTask.Delay(TimeSpan.FromSeconds(.35f));
                transform.DOMoveY(-20, 2).SetEase(Ease.InSine);
            }
            else
                PlayAnimation(BaseAnimations.Die.ToString());
        }
        _battleController.GameState.HeroDeath?.Invoke();
    }
    
    [PunRPC] private void _ApplyFallingDown(Vector3 pos)
    {
        Animator.SetTrigger(BaseAnimations.Fall.ToString());
        transform.DOMove(pos, .4f)
            .OnComplete(() =>
            {
                transform.DOMoveY(-20, 1f);
                if(PhotonView.IsMine) TakeDamage(new AttackData {Target = Config.name, ResultDamageValue = 10000});
            });
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (!info.photonView.IsMine) Init();
    }
    #endregion
    
    public override string ToString() => $"{Model}[QueueNumber: {QueueNumber}]";
}