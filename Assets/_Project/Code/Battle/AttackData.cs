﻿using System;
using UnityEngine;

[Serializable]
public class AttackData
{
    public string ActorSpell;
    public string Actor;
    public string Target;
    public float OriginDamageValue;
    public float FromActorModifiedDamageValue;
    public float ResultDamageValue;
    public Vector3[] Aoe;
    public bool IgnoreProtectiveEffects;

    public override string ToString()
    {
        return
            $"[Actor: {Actor}," +
            $"[Target: {Target}," +
            $"[ActorSpell: {ActorSpell}," +
            $"[OriginValue: {OriginDamageValue}," +
            $" FromActorModifiedValue: {FromActorModifiedDamageValue}," +
            $" ResultValue: {ResultDamageValue}]," +
            $" IgnoreProtectiveEffects: {IgnoreProtectiveEffects}]";
    }
}
