﻿using System;
using System.Collections.Generic;
using Selectors;
using UnityEngine;
using Zenject.Internal;
using Logger = MagicBattleArena.Logger;

[Preserve]
public class StatusEffectExecutor : ExecutorBase
{
    [SerializeField] protected ITargetsSelector _actor;
    [SerializeField] protected ITargetsSelector _targets;
    [SerializeField] protected bool _applyByCondition;

    public bool IsDeferredApply => _applyByCondition;

    protected override void Activate(IExecutorContext context, Action onComplete)
    {
        if(_applyByCondition)
            return;
        
        //Logger.Log($"StatusEffectExecutor {this} Actor: {context.Actor}");
        var statusIcons = context.Spell.Config.StatusEffectIcons;
        if (_actor != null)
        {
            var actor = _actor.GetTarget(context.Actor.Config);
            if(!actor.Model.IsDead && !actor.ContainsBuffTag(BuffsManager.BuffsTags.NoEffectsAccept))
                actor.StatusEffectAdded?.Invoke(context.Spell, actor.Config.name, statusIcons.IconForSelf);
        }

        if (_targets != null)
        {
            var targets = _targets.GetTargets(context.Spell);
            foreach (var target in targets)
            {
                if(target.ContainsBuffTag(BuffsManager.BuffsTags.NoEffectsAccept))
                    continue;
                target.StatusEffectAdded?.Invoke(context.Spell, target.Config.name, statusIcons.IconForOthers);
            }
        }
    }
    
    public void Run(IExecutorContext context, CharacterView actor, Sprite icon)
    {
        if(!actor.Model.IsDead && !actor.ContainsBuffTag(BuffsManager.BuffsTags.NoEffectsAccept))
            actor.StatusEffectAdded?.Invoke(context.Spell, actor.Config.name, icon);
    }
}