﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MagicBattleArena;
using MagicBattleArena.Network;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject.Internal;
using Logger = MagicBattleArena.Logger;

[Preserve]
public class SpellExecutor : ExecutorBase
{
    [PropertySpace(5)] 
    [SerializeField] private BaseSpellFXCreator _spellFxCreator;
    [PropertySpace(10)] 
    [SerializeField] private StatusEffectExecutor _statusEffectExecutor;
    [PropertySpace(10)] 
    [SerializeField] private float _animationDelay;
    
    [PropertySpace(10)] 
    [TypesDropdown(EnablePresets = true, ExpandAllMenuItems = true)]
    [SerializeField] private List<SpellActions.ISpellAction> _spellActions;
    
    public BaseSpellFXCreator SpellFXCreator => _spellFxCreator;
    public StatusEffectExecutor StatusEffectsExecutor => _statusEffectExecutor;
    
    [Preserve] protected override async void Activate(IExecutorContext context, Action onComplete)
    {
        var config = context.Spell.Config;
        var actor = context.Actor;

        config.Owner.SpellAnimationsMap.TryGetValue(config, out var result);
        context.BattleController.BattleBaseUI.SpellsPanel.HighlightCard(config);

        if (_animationDelay > 0)
        {
            UniTask.Delay(TimeSpan.FromSeconds(_animationDelay))
                .ContinueWith(() =>
                {
                    actor.PlayAnimation(result.ToString());
                });
        }
        else
            actor.PlayAnimation(result.ToString());

        await UniTask.Delay(TimeSpan.FromSeconds(.5f));
        
        _spellFxCreator.Init(context);
        _statusEffectExecutor?.Run(context);

        if (_spellActions == null) 
            return;
        
        //context.BattleController.DebugOutput.Print($"------ {config.name} ({_spellActions.Count}) ------");
        for (var i = 0; i < _spellActions.Count; i++)
        {
            var action = _spellActions[i];
            //context.BattleController.DebugOutput.Print($"action: {action}, i: {i}");
            if (action == null)
                continue;
            if (action.ApplyDelay > 0)
                await UniTask.Delay(TimeSpan.FromSeconds(action.ApplyDelay));
            action.Apply(context);
        }
        //context.BattleController.DebugOutput.Print($"----------------------");
        //Logger.Log($"TargetDamageExecutor Damage: {Damage}, context: {context}");
    }
}