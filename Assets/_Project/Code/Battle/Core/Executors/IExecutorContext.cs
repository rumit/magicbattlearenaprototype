﻿
public interface IExecutorContext
{
    BattleBaseController BattleController { get; }
    SpellsPlanningPhase.PlannedSpell Spell { get; }
    CharacterView Actor { get; }
}

public class ExecutorContext : IExecutorContext
{
    public BattleBaseController BattleController { get; }
    public SpellsPlanningPhase.PlannedSpell Spell { get; }
    public CharacterView Actor { get; }

    public ExecutorContext(BattleBaseController controller, CharacterView actor, SpellsPlanningPhase.PlannedSpell spell)
    {
        BattleController = controller;
        Actor = actor;
        Spell = spell;
    }
}
