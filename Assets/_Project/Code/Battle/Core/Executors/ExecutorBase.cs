﻿using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using MagicBattleArena;

public interface IExecutor
{
    void Run(IExecutorContext context, Action onComplete = null);
}

public abstract class ExecutorBase : IExecutor
{
    public void Run(IExecutorContext context, Action onComplete = null)
    {
        Activate(context, onComplete);
    }

    protected abstract void Activate(IExecutorContext context, Action onComplete);

    public static T GetExecutor<T>(IExecutor executor) where T : IExecutor
    {
        return (T)executor;
    }
}
