﻿
using System;
using System.Collections.Generic;
using System.Linq;
using MagicBattleArena.Network;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Selectors
{
    public enum TargetTypes
    {
        Self,
        Heroes,
        Enemies,
        RandomEnemy
    }
    
    public interface ITargetsSelector
    {
        List<CharacterView> GetTargets(SpellsPlanningPhase.PlannedSpell spell);
        CharacterView GetTarget(CharConfig config);
        CharacterView GetRandomTarget(SpellsPlanningPhase.PlannedSpell spell);
        TargetTypes TargetType { get; }
    }
    
    public class TargetsSelector : ITargetsSelector
    {
        [SerializeField] private TargetTypes _targetType;
        
        [HideIf(nameof(_targetType), TargetTypes.Self)]
        [SerializeField] private bool _inAoeArea = true;
        public TargetTypes TargetType => _targetType;
        
        [HideIf(nameof(_targetType), TargetTypes.Self)]
        [SerializeField] private CharConfig _exclude;

        public List<CharacterView> GetTargets(SpellsPlanningPhase.PlannedSpell spell)
        {
            List<CharacterView> result = null;
            switch (_targetType)
            {
                case TargetTypes.Heroes: 
                    result = _inAoeArea
                        ? Player.LocalPlayer.Team
                            .Where(h => !h.Model.IsDead && spell.IsInArea(h.transform.position) && h.Config != _exclude).ToList()
                        : Player.LocalPlayer.Team.Where(h => !h.Model.IsDead && h.Config != _exclude).ToList();
                    break;
                    
                case TargetTypes.Enemies: 
                    result = _inAoeArea
                        ? Player.EnemyPlayer.Team
                            .Where(h => !h.Model.IsDead && spell.IsInArea(h.transform.position) && h.Config != _exclude).ToList()
                        : Player.EnemyPlayer.Team.Where(h => !h.Model.IsDead && h.Config != _exclude).ToList();
                    break;
            }
            return result;
        }
        
        public CharacterView GetTarget(CharConfig config)
        {
            return Player.GetLocalHeroByConfig(config);
        }
        
        public CharacterView GetRandomTarget(SpellsPlanningPhase.PlannedSpell spell)
        {
            CharacterView result = null;
            switch (_targetType)
            {
                case TargetTypes.RandomEnemy:
                    
                    var enemies = _inAoeArea
                        ? Player.EnemyPlayer.Team.
                            Where(h => !h.Model.IsDead && spell.IsInArea(h.transform.position) && h.Config != _exclude).ToList()
                        : Player.EnemyPlayer.Team.Where(h => !h.Model.IsDead && h.Config != _exclude).ToList();

                    result = enemies.Count <= 0 ? null : enemies.GetRandomElements(1).First();
                    break;
            }
            return result;
        }
    }
}