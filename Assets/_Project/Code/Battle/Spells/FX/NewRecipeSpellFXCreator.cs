﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class NewRecipeSpellFXCreator : BaseSpellFXCreator
{
    public override async void Init(IExecutorContext context)
    {
        var actor = context.Actor;
        var ypos = SecondaryFxUnitRef.transform.position.y;
        var position = new Vector3 (actor.Model.Position.x, ypos, actor.Model.Position.z);
        var content = new object []
        {
            position, actor.Config.name, context.Spell.Config.name
        };
        var options = _invisibleForEnemies
            ? new RaiseEventOptions {TargetActors = new[] {PhotonNetwork.LocalPlayer.ActorNumber}}
            : new RaiseEventOptions {Receivers = ReceiverGroup.All};
        
        PhotonNetwork.RaiseEvent(SpellsFxController.SecondaryFXCode, content, options, SendOptions.SendReliable);
        
        await UniTask.Delay(TimeSpan.FromSeconds(.2f));

        var areaCellsPositions = context.Spell.AoeCellsData.Select(c => c.Position).ToList();
        var ordered = areaCellsPositions.OrderBy(p => p.x);
        var min = ordered.First();
        var max = ordered.Last();
        var pos = Vector3.Lerp(min, max, .5f);
        
        var node = context.BattleController.LevelField.NavigationMap.GetNodeByWorldPosition(pos);
        if (node.Obstacle is CharacterView)
            node = node.Neighbours.FirstOrDefault(n => n.Obstacle == null);
        
        context.BattleController.BattleSimulator.Teleport(actor, node);
        
        await UniTask.Delay(TimeSpan.FromSeconds(.5f));
        
        base.Init(context);
    }
}
