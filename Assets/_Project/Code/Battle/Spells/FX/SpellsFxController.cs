﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Triggers;
using DG.Tweening;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Logger = MagicBattleArena.Logger;
using Player = MagicBattleArena.Network.Player;

[DisallowMultipleComponent]
public class SpellsFxController : MonoBehaviour, IOnEventCallback, IPunObservable
{
    public const byte CommonEventCode = 1;
    public const byte SecondaryFXCode = 2;
    public const byte SingleShotFXCode = 3;
    public const byte MultiTargetShotFXCode = 4;

    public static string GetFXViewKey(SpellConfig config, string spellGUID, Vector3? pos = null)
    {
        return pos != null ? $"{config.name}_{spellGUID}_{((Vector3) pos).x}-{((Vector3) pos).z}" : $"{config.name}_{spellGUID}";
    }

    
    private BattleSimulationController _battleSimulationController;
    private Dictionary<string, SpellFXUnitView> _fXViesMap;
    
    private PresentationAoeView _aoePresentationView;
    private PhotonView _photonView;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _fXViesMap = new Dictionary<string, SpellFXUnitView>();
        _battleSimulationController = GetComponent<BattleSimulationController>();
        
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDestroy()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    public void OnEvent(EventData photonEvent)
    {
        switch (photonEvent.Code)
        {
            case SpellsFxController.CommonEventCode:
                AddCommonView((object[]) photonEvent.CustomData);
                break;
            case SpellsFxController.SecondaryFXCode:
                AddSecondaryView((object[]) photonEvent.CustomData);
                break;
            case SpellsFxController.SingleShotFXCode:
                AddSingleShotView((object[]) photonEvent.CustomData);
                break;
            case SpellsFxController.MultiTargetShotFXCode:
                AddMultiTargetShotView((object[]) photonEvent.CustomData);
                break;
        }
    }

    private void AddCommonView(object[] data)
    {
        var positions = (Vector3[])data[0];
        //var impactRanges = (Vector2[])data[1];
        var actorId = (string)data[2];
        var spellId = (string)data[3];
        var spellGUID = (string)data[4];
        var placementType = (BaseSpellFXCreator.FXPlacementTypes)(byte)data[5];
        //MagicBattleArena.Logger.Log($"OnEvent aoeCellsData:{string.Join(",", aoeCellsData)}");
        
        var actor = Player.GetHeroById(actorId);
        var spellConfig = actor.GetSpellConfigById(spellId);

        AddPresentationAoeView(positions, spellConfig.AOESettings.CellRef, actor.Config.ColorId);

        switch (placementType)
        {
            case BaseSpellFXCreator.FXPlacementTypes.EveryCell:
                PlaceFXsAtEveryCell(positions, actor, spellConfig, spellGUID);
                break;
            case BaseSpellFXCreator.FXPlacementTypes.CenterOfArea:
                PlaceFXAtCenterArea(positions, actor, spellConfig, spellGUID);
                break;
        }
    }
    
    private void AddPresentationAoeView(Vector3[] positions, AOECell cellRef, Color color)
    {
        if (_aoePresentationView != null)
            Destroy(_aoePresentationView.gameObject);
        
        var aoe = new GameObject("SpellPresentationAOE");
        _aoePresentationView = aoe.AddComponent<PresentationAoeView>();
        _aoePresentationView.Init(positions, cellRef, color);
    }
    
    private void AddSecondaryView(object[] data)
    {
        var pos = (Vector3)data[0];
        var actorId = (string)data[1];
        var spellId = (string)data[2];
        
        var actor = Player.GetHeroById(actorId);
        var spellConfig = actor.GetSpellConfigById(spellId);
        var spellExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor);
        var actorIsContainer = spellExecutor.SpellFXCreator.ActorIsContainer;
        
        var fxRef = spellExecutor.SpellFXCreator.SecondaryFxUnitRef;
        var fxView = Instantiate(fxRef, pos, fxRef.transform.rotation, actorIsContainer ? actor.transform : null);
        fxView.Init();
    }

    private void AddSingleShotView(object[] data)
    {
        var aoePositions = (Vector3[])data[0];
        var actorPos = (Vector3)data[1];
        var targetPos = (Vector3)data[2];
        var actorId = (string)data[3];
        var spellId = (string)data[4];

        var actor = Player.GetHeroById(actorId);
        var spellConfig = actor.GetSpellConfigById(spellId);
        
        AddPresentationAoeView(aoePositions, spellConfig.AOESettings.CellRef, actor.Config.ColorId);

        var spellExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor);
        var fxView = Instantiate(spellExecutor.SpellFXCreator.SpellFxUnitRef, actorPos, Quaternion.identity);
        fxView.transform.LookAt(targetPos);
        fxView.Init();
        fxView.transform.DOMove(targetPos, 0.5f)
            .OnComplete(() =>
            {
                CompleteFXPresentation(actor, spellConfig);
            });
    }

    private void AddMultiTargetShotView(object[] data)
    {
        var startPos = (Vector3)data[0];
        var targetsPositions = (Vector3[])data[1];
        var actorId = (string)data[2];
        var spellId = (string)data[3];
        
        var actor = Player.GetHeroById(actorId);
        var spellConfig = actor.GetSpellConfigById(spellId);
        
        AddPresentationAoeView(targetsPositions, spellConfig.AOESettings.CellRef, actor.Config.ColorId);
        //Array.ForEach(targetsPositions, p => p.y = 0.5f);
        
        var spellExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor);
        var fxView = Instantiate(spellExecutor.SpellFXCreator.SpellFxUnitRef, startPos, Quaternion.identity);
        fxView.Init(startPos, targetsPositions, remove =>
        {
            Destroy(fxView.gameObject);
            CompleteFXPresentation(actor, spellConfig);
        });
    }

    private void PlaceFXAtCenterArea(Vector3[] positions, CharacterView actor, SpellConfig spellConfig, string spellGUID)
    {
        var spellExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor);
        var fxRef = spellExecutor.SpellFXCreator.SpellFxUnitRef;
        var actorIsContainer = spellExecutor.SpellFXCreator.ActorIsContainer;
        
        var ypos = fxRef.transform.position.y;
        var ordered = positions.OrderBy(p => p.x);
        var min = ordered.First();
        var max = ordered.Last();
        var tmp = Vector3.Lerp(min, max, .5f);
        var pos = new Vector3(tmp.x, ypos, tmp.z);
        
        var isLoop = fxRef.GetComponent<ParticleSystem>().main.loop;
        var fxView = Instantiate(fxRef, pos, fxRef.transform.rotation, actorIsContainer ? actor.transform : null);
        var key = GetFXViewKey(spellConfig, spellGUID);
        fxView.Init(key, remove =>
        {
            if ((!remove || isLoop) && !_fXViesMap.ContainsKey(fxView.Id))
                _fXViesMap.Add(fxView.Id, fxView);

            if (remove && !isLoop && fxView != null && fxView.gameObject != null)
                Destroy(fxView.gameObject);

            CompleteFXPresentation(actor, spellConfig);
        });
    }

    private void PlaceFXsAtEveryCell(Vector3[] positions, CharacterView actor, SpellConfig spellConfig, string spellGUID)
    {
        var spellExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor);
        var fxRef = spellExecutor.SpellFXCreator.SpellFxUnitRef;
        var actorIsContainer = spellExecutor.SpellFXCreator.ActorIsContainer;
        var usePositionInKey = spellExecutor.SpellFXCreator.UsePositionInKey;

        var isLoop = fxRef.GetComponent<ParticleSystem>().main.loop;
        var count = 0;
        foreach (var pos in positions)
        {
            // if (positions.Length == 1)
            //     Debug.DrawRay(pos, Vector3.up * 10, Color.magenta, 1000);
            var fxView = Instantiate(fxRef, pos, fxRef.transform.rotation, actorIsContainer ? actor.transform : null);
            var key = usePositionInKey ? GetFXViewKey(spellConfig, spellGUID, pos) : GetFXViewKey(spellConfig, spellGUID);
            fxView.Init(key, remove =>
            {
                if ((!remove || isLoop) && !_fXViesMap.ContainsKey(fxView.Id))
                {
                    _fXViesMap.Add(fxView.Id, fxView);
                    //_fXViesMap.TryGetValue(fxView.Id, out var result);
                    //Logger.Log($"FXView key: {fxView.Id} -> {result}");
                }
                
                if (remove && !isLoop && fxView != null && fxView.gameObject != null)
                    Destroy(fxView.gameObject);

                count++;

                if (count >= positions.Length)
                    CompleteFXPresentation(actor, spellConfig);
            });
        }
    }
    
    private void CompleteFXPresentation(CharacterView actor, SpellConfig spellConfig)
    {
        if (_aoePresentationView != null)
            Destroy(_aoePresentationView.gameObject);
        //var executor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor).SpellFXCreator;
        if (actor.PhotonView.IsMine)
        {
            CharacterInteraction.DeselectCurrent();
            _battleSimulationController.CompleteCurrentSpellAction();
        }
    }
    
    public void RemoveFXView(string id)
    {
        _fXViesMap.TryGetValue(id, out var result);
        if (result != null)
        {
            Destroy(result.gameObject);
            _fXViesMap.Remove(id);
        }
    }
    
    public void TryRemoveFXView(string id)
    {
        _photonView.RPC(nameof(_TryRemoveFXView), RpcTarget.AllViaServer, id);
    }
    
    [PunRPC]
    private void _TryRemoveFXView(string id)
    {
        //var log = _battleController.DebugOutput;
        _fXViesMap.TryGetValue(id, out var result);
        // if (id.Contains("HibernationSpell"))
        // {
        //     Logger.Log($"FXView: {id}");
        //     foreach (var kvp in _fXViesMap)
        //     {
        //         //Logger.Log($"FXView key: {id} -> {result}");
        //         Logger.Log($"FXView key: {kvp.Key} -> {kvp.Value}");
        //         Logger.Log($"FXView is keys equals: {kvp.Key == id}");
        //     }
        //     Logger.Log($"FXView ================");
        // }
        if (result != null)
        {
            Destroy(result.gameObject);
            _fXViesMap.Remove(id);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { }
}