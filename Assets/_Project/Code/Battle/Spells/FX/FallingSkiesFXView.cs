﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class FallingSkiesFXView : SpellFXUnitView
{
    [SerializeField] private GameObject _arrowRef;
    
    public override void Init(Vector3 startPosition, Vector3[] targetPositions, Action<bool> callback)
    {
        _cancelToken?.Cancel();
        _cancelToken = new CancellationTokenSource();
        
        for (var i = 0; i < targetPositions.Length; i++)
        {
            var targetPoint = targetPositions[i];
            var arrow = Instantiate(_arrowRef, startPosition, Quaternion.identity);
            arrow.transform.LookAt(targetPoint);
            arrow.transform.DOMove(targetPoint, 1f)
                .SetEase(Ease.OutCubic)
                .OnComplete(() => Destroy(arrow));
        }
        
        UniTask.Delay(TimeSpan.FromSeconds(_liveTime), cancellationToken:_cancelToken.Token)
            .ContinueWith(() => callback.Invoke(true));
    }
}
