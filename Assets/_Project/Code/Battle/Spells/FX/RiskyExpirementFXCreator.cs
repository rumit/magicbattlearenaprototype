﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class RiskyExpirementFXCreator : BaseSpellFXCreator
{
    public override void Init(IExecutorContext context)
    {
        var actor = context.Actor;
        var ypos = SecondaryFxUnitRef.transform.position.y;
        var position = new Vector3 (actor.Model.Position.x, ypos, actor.Model.Position.z);
        var content = new object []
        {
            position, actor.Config.name, context.Spell.Config.name
        };
        var options = _invisibleForEnemies
            ? new RaiseEventOptions {TargetActors = new[] {PhotonNetwork.LocalPlayer.ActorNumber}}
            : new RaiseEventOptions {Receivers = ReceiverGroup.All};
        PhotonNetwork.RaiseEvent(SpellsFxController.SecondaryFXCode, content, options, SendOptions.SendReliable);
        
        base.Init(context);
    }
}