﻿using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class MultitargetShotFXCreator : BaseSpellFXCreator
{
    public override void Init(IExecutorContext context)
    {
        var actor = context.Actor;
        var ypos = _spellFxUnitRef.transform.position.y;
        
        var actorPosition = new Vector3 (actor.Model.Position.x, ypos, actor.Model.Position.z);
        var targetsPositions = context.Spell.AoeCellsData.Select(c => c.Position).ToList();

        var content = new object []
        {
            actorPosition, targetsPositions.ToArray(), actor.Config.name, context.Spell.Config.name
        };
        var options = _invisibleForEnemies
            ? new RaiseEventOptions {TargetActors = new[] {PhotonNetwork.LocalPlayer.ActorNumber}}
            : new RaiseEventOptions {Receivers = ReceiverGroup.All};
        PhotonNetwork.RaiseEvent(SpellsFxController.MultiTargetShotFXCode, content, options, SendOptions.SendReliable);
    }
}