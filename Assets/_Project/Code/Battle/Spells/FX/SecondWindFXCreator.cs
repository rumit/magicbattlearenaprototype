﻿using System.Linq;
using Cysharp.Threading.Tasks;
using ExitGames.Client.Photon;
using LevelField;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Logger = MagicBattleArena.Logger;

public class SecondWindFXCreator : BaseSpellFXCreator
{
    public override void Init(IExecutorContext context)
    {
        var map = context.BattleController.LevelField.NavigationMap;
        Vector3 position = default;
        AoeCellData cellToRemove = null;
        foreach (var cellData in context.Spell.AoeCellsData)
        {
            var p = cellData.Position;
            if (map.GetNodeByWorldPosition(p).BelongsPlayer)
                continue;
            
            position = new Vector3(p.x, SecondaryFxUnitRef.transform.position.y, p.z);
            //Logger.Log($"Cell on enemy field: {position}");
            cellToRemove = cellData;
            break;
        }
        
        var content = new object [] { position, context.Actor.Config.name, context.Spell.Config.name };
        var options = _invisibleForEnemies
            ? new RaiseEventOptions {TargetActors = new[] {PhotonNetwork.LocalPlayer.ActorNumber}}
            : new RaiseEventOptions {Receivers = ReceiverGroup.All};
        PhotonNetwork.RaiseEvent(SpellsFxController.SecondaryFXCode, content, options, SendOptions.SendReliable);

        if(cellToRemove != null) 
            context.Spell.AoeCellsData.Remove(cellToRemove);
        
        base.Init(context);
        
        if(cellToRemove != null) 
            context.Spell.AoeCellsData.Add(cellToRemove);
    }
}