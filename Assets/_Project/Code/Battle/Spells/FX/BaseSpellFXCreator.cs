﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Logger = MagicBattleArena.Logger;
using Player = MagicBattleArena.Network.Player;

public class BaseSpellFXCreator
{
    public enum FXPlacementTypes : byte
    {
        EveryCell,
        CenterOfArea,
        AtActor
    }

    public bool UsePositionInKey;
    [SerializeField] protected bool _invisibleForEnemies;
    [SerializeField] protected bool _actorIsContainer;
    [SerializeField] protected FXPlacementTypes _placementType;
    [Space(10)]
    [SerializeField] protected SpellFXUnitView _spellFxUnitRef;
    [SerializeField] protected SpellFXUnitView _secondaryFxUnitRef;

    public SpellFXUnitView SpellFxUnitRef => _spellFxUnitRef;
    public SpellFXUnitView SecondaryFxUnitRef => _secondaryFxUnitRef;
    public bool ActorIsContainer => _actorIsContainer;
    
    
    public virtual void Init(IExecutorContext context)
    {
        var actor = context.Actor;
        var spell = context.Spell;
        var cells = spell.AoeCellsData;
        var impactRanges = cells.Select(d => d.ImpactRange).ToArray();

        var ypos = SpellFxUnitRef.transform.position.y;
        var positions = new Vector3[cells.Count];
        for (var i = 0; i < cells.Count; i++)
        {
            var p = cells[i].Position;
            positions[i] = new Vector3(p.x, ypos, p.z);
        }
        var content = new object []
        {
            positions, impactRanges, actor.Config.name, spell.Config.name, spell.GUID, (byte)_placementType
        };
        var options = _invisibleForEnemies
            ? new RaiseEventOptions {TargetActors = new[] {PhotonNetwork.LocalPlayer.ActorNumber}}
            : new RaiseEventOptions {Receivers = ReceiverGroup.All};
        
        PhotonNetwork.RaiseEvent(SpellsFxController.CommonEventCode, content, options, SendOptions.SendReliable);
    }
}
