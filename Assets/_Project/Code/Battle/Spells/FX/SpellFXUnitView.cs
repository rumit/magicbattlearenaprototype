﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using TouchScript.Examples.Cube;
using UnityEngine;

public class SpellFXUnitView : MonoBehaviour
{
    public string Id { get; protected set; }

    [SerializeField] protected float _liveTime;
    
    protected ParticleSystem _particleSystem;
    protected CancellationTokenSource _cancelToken;

    protected void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        if (_liveTime <= 0)
            _liveTime = 2;
    }

    protected void OnDestroy()
    {
        _cancelToken?.Cancel();
    }

    public virtual void Init(string guid, Action<bool> callback)
    {
        _cancelToken?.Cancel();
        _cancelToken = new CancellationTokenSource();
        
        Id = guid;
        _particleSystem.Play();
        
        if (_particleSystem.main.loop)
        {
            UniTask.Delay(TimeSpan.FromSeconds(2.5f), cancellationToken:_cancelToken.Token)
                .ContinueWith(() => callback.Invoke(true));
        }
        else
        {
            UniTask.Delay(TimeSpan.FromSeconds(_liveTime), cancellationToken:_cancelToken.Token)
                .ContinueWith(() => callback.Invoke(true));
            //UniTask.WaitUntil(() => !_particleSystem.IsAlive(), cancellationToken:_cancelToken.Token)
             //   .ContinueWith(() => callback.Invoke(true));
        }
    }

    public virtual void Init(Vector3 startPosition, Vector3[] targetPositions, Action<bool> callback) { }

    public void Init()
    {
        _cancelToken?.Cancel();
        _cancelToken = new CancellationTokenSource();
        
        _particleSystem.Play();
        
        UniTask.Delay(TimeSpan.FromSeconds(_liveTime), cancellationToken:_cancelToken.Token)
            .ContinueWith(() => Destroy(gameObject));
        
        //UniTask.WaitUntil(() => !_particleSystem.IsAlive(), cancellationToken:_cancelToken.Token)
         //   .ContinueWith(() => Destroy(gameObject));
    }
}
