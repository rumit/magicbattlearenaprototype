﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class HibernationFXView : SpellFXUnitView
{
    [SerializeField] private float _timeToPause;
    
    public override async void Init(string guid, Action<bool> callback)
    {
        Id = guid;
        _particleSystem.Play();
        await UniTask.WaitUntil(() => _particleSystem.time >= _timeToPause);
        _particleSystem.Pause();
        callback.Invoke(false);
    }
}
