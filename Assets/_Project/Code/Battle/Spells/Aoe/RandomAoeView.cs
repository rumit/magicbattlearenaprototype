﻿using System;
using System.Collections.Generic;
using System.Linq;
using LevelField;
using MagicBattleArena.Network;
using UnityEngine;
using UnityEngine.Tilemaps;
using static SpellConfig;
using Logger = MagicBattleArena.Logger;

public class RandomAoeView : AbstractSpellAoeView
{
    public override void Init(BattleBaseController battleController, SpellConfig config)
    {
        base.Init(battleController, config);

        var countOnEnemyField = Config.AOESettings.CountOnEnemyField;
        var countOnPlayerField = Config.AOESettings.CountOnPlayerField;
        if (countOnEnemyField <= 0 && countOnPlayerField <= 0)
        {
            throw new Exception($"{config.Name} has wrong randomization setup!");
        }
        
        var aoeCellsData = new List<AoeCellData>();
        if (countOnEnemyField > 0)
        {
            var nodes = _map.GetRandomNodesOnFieldPart(_map.EnemyNodes, config.AOESettings.AllowedPlacementOn, countOnEnemyField);
            aoeCellsData = GetRandomCellsData(nodes);
            AddAoeCells(_layoutTilesMap.First(), _map.EnemyNodes);
        }
        
        if (countOnPlayerField > 0)
        {
            var nodes = _map.GetRandomNodesOnFieldPart(_map.PlayerNodes, config.AOESettings.AllowedPlacementOn, countOnPlayerField);
            aoeCellsData = aoeCellsData.Concat(GetRandomCellsData(nodes)).ToList();
            AddAoeCells(_layoutTilesMap.First(), _map.PlayerNodes);
        }
        
        _aoeAreaData = _additionalAoeAreaData.Count > 0 ? aoeCellsData.Concat(_additionalAoeAreaData).ToList() : aoeCellsData;
    }

    private List<AoeCellData> GetRandomCellsData(List<LevelField.Node> nodes)
    {
        var firstCellData = _layoutTilesMap.First();
        var aoeAreaData = new List<AoeCellData>();
        foreach (var node in nodes)
        {
            var pos = _map.GetWorldPositionByNode(node);
            var aoeCellData = new AoeCellData {Position = pos, ImpactRange = firstCellData.Value.ImpactRange};
            aoeAreaData.Add(aoeCellData);
        }
        return aoeAreaData;
    }
}