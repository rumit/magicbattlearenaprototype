﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

[Serializable]
public class AoeCellData
{
    public static Vector3 GetAoeAreaCenter(List<Vector3> positions, float ypos)
    {
        var ordered = positions.OrderBy(p => p.x);
        var min = ordered.First();
        var max = ordered.Last();
        var tmp = Vector3.Lerp(min, max, .5f);
        return new Vector3(tmp.x, ypos, tmp.z);
    }
    
    public Vector2 ImpactRange;
    public Vector3 Position;

    public override string ToString()
    {
        return $"[Tile: {ImpactRange}, Position: {Position}]";
    }
}
