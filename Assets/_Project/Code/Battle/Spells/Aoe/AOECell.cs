﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AOECell : MonoBehaviour
{
    private SpriteRenderer _renderer;
    public AOETileConfig Tile { get; private set; }
    public Vector3Int LayoutPosition { get; private set; }

    public bool IsActive => _renderer.enabled;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    public void Init(AOETileConfig tile, Vector3Int layoutPosition, Color color)
    {
        Tile = tile;
        LayoutPosition = layoutPosition;
        _renderer.material.SetColor(ShadersConstants.Color, color);
    }
    
    public void Init(Color color)
    {
        _renderer.material.SetColor(ShadersConstants.Color, color);
    }

    public void Show()
    {
        _renderer.enabled = true;
    }
    
    public void Hide()
    {
        _renderer.enabled = false;
    }

    public override string ToString()
    {
        var tile = Tile == null ? "null" : Tile.name;
        return $"[Tile: {tile}, Position: {transform.position}]";
    }
}
