﻿using System.Collections.Generic;
using System.Linq;
using LevelField;
using UnityEngine;
using UnityEngine.Tilemaps;
using static SpellsPlanningPhase;
using Logger = MagicBattleArena.Logger;

public abstract class AbstractSpellAoeView : MonoBehaviour
{
    public SpellConfig Config { get; private set; }
    
    protected NavigationMap _map;
    protected BattleBaseController _battleController;
    protected Dictionary<Vector3Int, AOETileConfig> _layoutTilesMap;
    protected SpellConfig.PlacementAreaOfAOE _placementArea;
    protected SpellConfig.AllowedNodesStatesForPlacement _allowedPlacement;
    protected CharacterView _character;

    protected readonly List<AOECell> _aoeCells = new List<AOECell>();
    protected List<AoeCellData> _aoeAreaData = new List<AoeCellData>();
    protected List<AoeCellData> _additionalAoeAreaData = new List<AoeCellData>();

    public virtual void Init(BattleBaseController battleController, SpellConfig config)
    {
        Config = config;
        _battleController = battleController;
        _map = battleController.LevelField.NavigationMap;
        _character = _battleController.SelectedCharacter;
        _placementArea = Config.AOESettings.PlacementArea;
        _allowedPlacement = Config.AOESettings.AllowedPlacementOn;
        
        var layout = Instantiate(config.AOESettings.Layout);
        _layoutTilesMap = layout.TilesMap;
        Destroy(layout.gameObject);

        CheckAdditional();
    }
    
    public virtual void Init(List<AoeCellData> cellsData, AOECell aoeCellRef, Color color) { }

    public List<AoeCellData> GetAoeCellsData()
    {
        return _aoeAreaData;
    }
    
    public List<AoeCellData> GetPresentableAoeCellsData()
    {
        var result = new List<AoeCellData>();
        var activeCells = _aoeCells.Where(c => c.IsActive);
        foreach (var cell in activeCells)
        {
            var cellData = new AoeCellData {Position = cell.transform.localPosition, ImpactRange = cell.Tile.ImpactRange};
            result.Add(cellData);
        }
        return result;
    }

    private void CheckAdditional()
    {
        var additionalViewSettings = Config.AOESettings.AdditionalAreaOnOppositeSide;
        if (additionalViewSettings.Layout == null) 
            return;
        
        var layout = Instantiate(additionalViewSettings.Layout);
        var layoutTilesMap = layout.TilesMap;
        Destroy(layout.gameObject);
            
        if (additionalViewSettings.IsRandom)
        {
            var partData = layoutTilesMap.First();
            
            var nodes = _placementArea == SpellConfig.PlacementAreaOfAOE.Player || _placementArea == SpellConfig.PlacementAreaOfAOE.Self
                ? _map.EnemyNodes
                : _map.PlayerNodes;
            
            var randomNodes = nodes.GetRandomElements(additionalViewSettings.Count);
            var aoeAreaData = new List<AoeCellData>();
            foreach (var node in randomNodes)
            {
                var pos = _map.GetWorldPositionByNode(node);
                var aoeCellData = new AoeCellData {Position = pos, ImpactRange = partData.Value.ImpactRange};
                aoeAreaData.Add(aoeCellData);
            }
            _additionalAoeAreaData = aoeAreaData;
            
            AddAoeCells(partData, nodes);
        }
    }

    protected virtual void AddAoeCells(KeyValuePair<Vector3Int, AOETileConfig> layoutData, List<Node> nodes)
    {
        foreach (var node in nodes)
        {
            var position = _map.CellToWorld(layoutData.Key) + _map.GetWorldPositionByNode(node);
            var aoeCell = Instantiate(Config.AOESettings.CellRef, position, Quaternion.Euler(new Vector3(90, 0, 0)), transform);
            aoeCell.Init(layoutData.Value, layoutData.Key, _character.Config.ColorId);
            if(!_aoeCells.Contains(aoeCell)) 
                _aoeCells.Add(aoeCell);
        }
    }

    protected virtual AOECell AddAoeCell(KeyValuePair<Vector3Int, AOETileConfig> layoutData, Vector3 targetPosition)
    {
        var position = _map.CellToWorld(layoutData.Key) + targetPosition;
        
        var node = _map.GetNodeByWorldPosition(position);
        if (node == null)
            return null;
        
        var aoeCell = Instantiate(Config.AOESettings.CellRef, position, Quaternion.Euler(new Vector3(90, 0, 0)), transform);
        aoeCell.Init(layoutData.Value, layoutData.Key, _character.Config.ColorId);
        if(!_aoeCells.Contains(aoeCell)) 
            _aoeCells.Add(aoeCell);
        return aoeCell;
    }
}