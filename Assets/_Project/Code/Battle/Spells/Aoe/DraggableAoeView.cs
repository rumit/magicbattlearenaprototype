﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using LevelField;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.Tilemaps;
using static SpellConfig;
using Logger = MagicBattleArena.Logger;

public class DraggableAoeView : AbstractSpellAoeView
{
    private TransformGesture _transformGesture;
    private bool _dragged;
    
    private Vector3 _touchPoint;
    
    private List<AOECell> _draggableCells = new List<AOECell>();

    private void OnDestroy()
    {
        _transformGesture.TransformStarted -= OnStartDrag;
        _transformGesture.Transformed -= OnDrag;
        _transformGesture.TransformCompleted -= OnDragComplete;
    }

    public override void Init(BattleBaseController battleController, SpellConfig config)
    {
        base.Init(battleController, config);
        
        _transformGesture = battleController.LevelField.TransformGesture;
        _transformGesture.TransformStarted += OnStartDrag;
        _transformGesture.Transformed += OnDrag;
        _transformGesture.TransformCompleted += OnDragComplete;
        
        SetDraggableCells();
        UpdateAoeAreaData();
        if(_allowedPlacement == AllowedNodesStatesForPlacement.Collapsed) 
        {
            _touchPoint = GetNearestCollapsedCellPosition();
            Drag();
        }
    }

    private void SetDraggableCells()
    {
        var center = GetPointOnCenterField();
        foreach (var layoutData in _layoutTilesMap)
        {
            var cell = AddAoeCell(layoutData, center);
            if(cell != null)
                _draggableCells.Add(cell);
        }
    }

    private Vector3 GetNearestCollapsedCellPosition()
    {
        Node node = null;
        List<Node> nodes = _placementArea switch
        {
            PlacementAreaOfAOE.Player => _map.PlayerNodes,
            PlacementAreaOfAOE.Enemy => _map.EnemyNodes,
            _ => null
        };

        var centerNode = _map.GetCenterPartOfMap(nodes);
        var min = float.MaxValue;
        foreach (var n in nodes)
        {
            if(n.State != Node.States.CollapsedState)
                continue;
            var dist = (n.Position - centerNode.Position).sqrMagnitude;
            if (dist < min)
            {
                min = dist;
                node = n;
            }
        }
        return _map.GetWorldPositionByNode(node);
    }

    private Vector3 GetPointOnCenterField()
    {
        var fieldSize = _battleController.LevelField.Config.BattleFieldSize;
        var offset = fieldSize.X * .25f;
        var pt0 = _map.WorldToCell(new Vector3(offset, 0, 0));
        var pt1 = _map.WorldToCell(new Vector3(-offset, 0, 0));
        
        if (_allowedPlacement == AllowedNodesStatesForPlacement.AllExcludeCharacters)
        {
            var n = _map.GetNodeByPosition(pt0);
            if (n.Obstacle is CharacterView)
            {
                var nearestFree = _map.GetNearestFreeNode(n);
                if (nearestFree != default)
                    pt0 = nearestFree.Position;
            }
            
            n = _map.GetNodeByPosition(pt1);
            if (n.Obstacle is CharacterView)
            {
                var nearestFree = _map.GetNearestFreeNode(n);
                if (nearestFree != default)
                    pt1 = nearestFree.Position;
            }
        }
        var nodes = _placementArea == PlacementAreaOfAOE.Player ? _map.PlayerNodes : _map.EnemyNodes;
        
        Vector3 result = default;
        var cellBounds = _map.GetCellBounds(_map.PlayerNodes.First().Position).size;
        foreach (var node in nodes.Where(node => node.Position.Equals(pt0) || node.Position.Equals(pt1)))
        {
            var p = _map.GetWorldPositionByNode(node);
            result = new Vector3(p.x, 0, -(cellBounds.z * .5f));
            break;
        }
        return result;
    }

    private void OnStartDrag(object sender, EventArgs e)
    {
        var worldPoint = _transformGesture.GetScreenPositionHitData().Point;
        var cellPos = _map.WorldToCell(worldPoint);
        var cellWorldPos = _map.CellToWorld(cellPos);
        var aoeCell = _draggableCells.FirstOrDefault(c => c.transform.position.Equals(cellWorldPos));
        
        if(aoeCell != null)
            _dragged = true;
    }

    private bool HasAreaCenterObstacle(List<AOECell> aoeCells)
    {
        var positions = new List<Vector3>();
        foreach (var cell in aoeCells)
            positions.Add(_map.CellToWorld(cell.LayoutPosition) + _touchPoint);

        var areaCenter = AoeCellData.GetAoeAreaCenter(positions, 0);
        var centerNode = _map.GetNodeByWorldPosition(areaCenter);
        return centerNode?.Obstacle is CharacterView;
    }
    
    private bool HasAreaCenterCollapsedCell()
    {
        var centerNode = _map.GetNodeByWorldPosition(_touchPoint);
        return centerNode != null && centerNode.State == Node.States.CollapsedState;
    }

    private void OnDrag(object sender, EventArgs e)
    {
        if(!_dragged)
            return;

        var tmp = _transformGesture.GetScreenPositionHitData().Point;
        _touchPoint = _map.CellToWorld(_map.WorldToCell(tmp));
        if(_touchPoint.Equals(Vector3.zero))
            return;

        Drag();
    }

    private void Drag()
    {
        var hiddenCount = 0;
        foreach (var cell in _draggableCells)
        {
            var cellWorldPos = _map.CellToWorld(cell.LayoutPosition) + _touchPoint;
            var node = _map.GetNodeByWorldPosition(cellWorldPos);
            if (node == null)
                hiddenCount++;
            else if (_placementArea == PlacementAreaOfAOE.Player && !node.BelongsPlayer ||
                     _placementArea == PlacementAreaOfAOE.Enemy && node.BelongsPlayer)
                hiddenCount++;
        }

        if (hiddenCount >= _draggableCells.Count)
            return;

        switch (_allowedPlacement)
        {
            case AllowedNodesStatesForPlacement.All:
            case AllowedNodesStatesForPlacement.Live:
                break;
            case AllowedNodesStatesForPlacement.Collapsed:
                if(!HasAreaCenterCollapsedCell())
                    return;
                break;
            case AllowedNodesStatesForPlacement.AllExcludeCharacters:
                if(HasAreaCenterObstacle(_draggableCells) || HasAreaCenterCollapsedCell())
                    return;
                break;
        }

        SetAOECells();
    }

    private void SetAOECells()
    {
        foreach (var cell in _draggableCells)
        {
            var cellWorldPos = _map.CellToWorld(cell.LayoutPosition) + _touchPoint;
            cell.transform.position = cellWorldPos;
            var node = _map.GetNodeByWorldPosition(cellWorldPos);
            if (node == null)
                cell.Hide();
            else
            {
                if (_placementArea == PlacementAreaOfAOE.Player && !node.BelongsPlayer ||
                    _placementArea == PlacementAreaOfAOE.Enemy && node.BelongsPlayer)
                {
                    cell.Hide();
                }
                else cell.Show();
            }
        }
    }

    private void OnDragComplete(object sender, EventArgs e)
    {
        _dragged = false;
        UpdateAoeAreaData();
    }

    private void UpdateAoeAreaData()
    {
        var aoeData = new List<AoeCellData>();
        foreach (var cell in _draggableCells)
        {
            if(cell == null || !cell.IsActive)
                continue;
            var cellData = new AoeCellData {Position = cell.transform.localPosition, ImpactRange = cell.Tile.ImpactRange};
            aoeData.Add(cellData);
        }
        _aoeAreaData = _additionalAoeAreaData.Count > 0 ? aoeData.Concat(_additionalAoeAreaData).ToList() : aoeData;
    }
}