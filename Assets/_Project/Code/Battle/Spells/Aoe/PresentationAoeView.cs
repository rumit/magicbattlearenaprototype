﻿using System.Collections.Generic;
using UnityEngine;
using static SpellsPlanningPhase;

public class PresentationAoeView : AbstractSpellAoeView
{
    public override void Init(List<AoeCellData> cellsData, AOECell aoeCellRef, Color color)
    {
        foreach (var data in cellsData)
        {
            var position = data.Position;
            var aoeCell = Instantiate(aoeCellRef, position, Quaternion.Euler(new Vector3(90, 0, 0)), transform);
            aoeCell.Init(color);
        }
    }
    
    public void Init(Vector3[] positions, AOECell aoeCellRef, Color color)
    {
        foreach (var p in positions)
        {
            var aoeCell = Instantiate(aoeCellRef, new Vector3(p.x, 0, p.z), Quaternion.Euler(new Vector3(90, 0, 0)), transform);
            aoeCell.Init(color);
        }
    }
}
