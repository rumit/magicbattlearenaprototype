﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelfAppliedAoeView : AbstractSpellAoeView
{
    public override void Init(BattleBaseController battleController, SpellConfig config)
    {
        base.Init(battleController, config);

        List<AoeCellData> aoeAreaData;
        var heroNodePos = _map.GetNodeByWorldPosition(_character.Model.Position).Position;
        if (_layoutTilesMap.Count == 1)
        {
            var cellWorldPos = _map.CellToWorld(heroNodePos);
            var cell = AddAoeCell(_layoutTilesMap.First(), cellWorldPos);
            var cellData = new AoeCellData {Position = cell.transform.localPosition, ImpactRange = cell.Tile.ImpactRange};
            aoeAreaData = new List<AoeCellData> {cellData};
        }
        else
        {
            if (config.ExecuteType == SpellConfig.ExecuteTypes.Common)
            {
                battleController.PathsHash.TryGetValue(_character, out var path);
                if (path?.Count > 0) heroNodePos = path.Last().Position;
            }
            aoeAreaData = new List<AoeCellData>();
            foreach (var kvp in _layoutTilesMap)
            {
                var cellWorldPos = _map.CellToWorld(heroNodePos);
                var cell = AddAoeCell(kvp, cellWorldPos);
                if(cell == null)
                    continue;
                aoeAreaData.Add(new AoeCellData {Position = cell.transform.localPosition, ImpactRange = cell.Tile.ImpactRange});
            }
        }
        _aoeAreaData = _additionalAoeAreaData.Count > 0 ? aoeAreaData.Concat(_additionalAoeAreaData).ToList() : aoeAreaData;
    }
}
