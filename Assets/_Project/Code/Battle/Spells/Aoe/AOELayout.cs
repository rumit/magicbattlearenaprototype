﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Logger = MagicBattleArena.Logger;

public class AOELayout : MonoBehaviour
{
    public Dictionary<Vector3Int, AOETileConfig> TilesMap { get; } = new Dictionary<Vector3Int, AOETileConfig>();
    
    private void Awake()
    {
        var tilemap = GetComponentInChildren<Tilemap>();
        tilemap.CompressBounds();
        var bounds = tilemap.cellBounds.allPositionsWithin;
        foreach (var p in bounds)
        {
            var tile = tilemap.GetTile<AOETileConfig>(p);
            if(tile != null)
                TilesMap.Add(p, tile);
        }
    }
}
