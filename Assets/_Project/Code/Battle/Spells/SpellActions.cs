﻿using System;
using System.Collections.Generic;
using System.Linq;
using MagicBattleArena.Network;
using Selectors;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Scripting;
using static SpellsPlanningPhase;
using Logger = MagicBattleArena.Logger;

public class SpellActions
{
    private static List<CharacterView> GetLivingHeroesInAoeArea(PlannedSpell spell)
    {
        var result = new List<CharacterView>();
        var allLiving = Player.GetAllHeroes().Where(h => !h.Model.IsDead);
        foreach (var hero in allLiving)
        {
            if (spell.IsInArea(hero.transform.position)) 
                result.Add(hero);
        }
        return result;
    }
    
    
    public interface ISpellAction
    {
        float ApplyDelay { get; set; }
        void Apply(IExecutorContext context);
    }

    public enum FieldPartTypes
    {
        All,
        Player,
        Enemy
    }
    
    public enum Conditions
    {
        None,
        SelfDamageTaken
    }

    public abstract class AbstractSpellAction : ISpellAction
    {
        [SerializeField] public float ApplyDelay { get; set; }
        protected BuffsManager BuffsManager { get; private set; }

        public virtual void Apply(IExecutorContext context)
        {
            BuffsManager = context.BattleController.BuffsManager;
        }
    }

    public class CurseOfCellsAction : AbstractSpellAction
    {
        [SerializeField] private FieldPartTypes _fieldPartType;
        
        [Header("В раундах")]
        [SerializeField] private int _duration;

        public void InitValues(int duration, FieldPartTypes fieldPartType = FieldPartTypes.All)
        {
            _duration = duration;
            _fieldPartType = fieldPartType;
        }
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            var buffs = BuffsManager.CreateFieldCellsBuffsByType<CursedCellsBuff>(context, _duration, _fieldPartType);
            foreach (var b in buffs)
                b.Start();
        }
    }
    
    public class TrapsAction : AbstractSpellAction
    {
        [Header("В раундах")]
        [SerializeField] private int _lifetime;
        [PropertySpace(10)]
        [SerializeField] private TrapConfig _trapConfig;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            var trapBuffs = BuffsManager.CreateFieldCellsBuffsByType<TrapBuff>(context, _lifetime, FieldPartTypes.Enemy);
            _trapConfig.Spell = context.Spell;
            foreach (var b in trapBuffs)
                b.Start(_trapConfig);

            var trapActorBuff = BuffsManager.CreateHeroBuffByType<TrapActorBuff>(context.Actor, context, -1);
            trapActorBuff.Start(trapBuffs);
        }
        
        [Serializable]
        public class TrapConfig
        {
            [Range(0, 100)] public int DamageProRound;
            [Header("В раундах")]
            public int MoveRestrictionDuration;
            public SpellConfig BindedSpellConfig;
            [NonSerialized] public PlannedSpell Spell;
        }
    }
    
    [Preserve] public class HealingAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _targetSelector;
        
        [SerializeField] private HPValueTypes _valueType;

        [SerializeField] 
        [ShowIf(nameof(_valueType), HPValueTypes.Percent)]
        [Range(0, 1)]
        private float _percent;
        
        [SerializeField]
        [HideIf(nameof(_valueType), HPValueTypes.Percent | HPValueTypes.RandomInt)]
        private int _value;
        
        [SerializeField]
        [ShowIf(nameof(_valueType), HPValueTypes.RandomInt)]
        private RangeIntSelector _valueMinMax;

        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            List<CharacterView> heroes;
            if (_targetSelector.TargetType == TargetTypes.Self)
            {
                var hero = _targetSelector.GetTarget(context.Actor.Config);
                heroes = new List<CharacterView> {hero};
            }
            else
                heroes = _targetSelector.GetTargets(context.Spell);

            foreach (var hero in heroes)
            {
                switch (_valueType)
                {
                    case HPValueTypes.Percent:
                        hero.IncreaseHealthByPercent(_percent);
                        break;
                    case HPValueTypes.Value:
                        hero.IncreaseHealthByValue(_value);
                        break;
                    case HPValueTypes.RandomInt:
                        var value = UnityEngine.Random.Range(_valueMinMax.Min, _valueMinMax.Max);
                        hero.IncreaseHealthByValue(value);
                        break;
                }
            }
        }
    }

    public class MovementRestrictionAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _targetSelector;
        
        [HideIf(nameof(_targetSelector))]public CharacterView Target;
        
        [Header("В раундах")]public int Duration;
        
        [Range(0, 100)]
        public int DamageProRound;

        [SerializeField] private bool _useProbability;
        
        [Range(0, 1)]
        [ShowIf(nameof(_useProbability))]
        [SerializeField] private float _probability = 1f;
        
        public override void Apply(IExecutorContext context)
        {
            if (_useProbability)
            {
                var rand = UnityEngine.Random.Range(0, 1f);
                context.BattleController.DebugOutput.Print($"MovementRestrictionAction rand > {rand}");
                if(rand > _probability)
                    return;
            }
            
            base.Apply(context);
            
            var spellConfig = context.Spell.Config;
            var buffs = new List<MoveRestrictionBuff>();
            if (_targetSelector == null)
            {
                buffs.Add(BuffsManager.CreateHeroBuffByType<MoveRestrictionBuff>(Target, context, Duration));
                Target.StatusEffectAdded?.Invoke(context.Spell, Target.Config.name, spellConfig.StatusEffectIcons.IconForOthers);
            }
            else
                buffs = BuffsManager.CreateHeroBuffsByType<MoveRestrictionBuff>(_targetSelector, context, Duration);

            
            var executor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor);
            var statusIcon = spellConfig.StatusEffectIcons.IconForOthers == null
                ? spellConfig.StatusEffectIcons.IconForSelf
                : spellConfig.StatusEffectIcons.IconForOthers;
            foreach (var buff in buffs)
            {
                executor.StatusEffectsExecutor.Run(context, buff.GetOwner<CharacterView>(), statusIcon);
                if(DamageProRound > 0)
                    buff.Start(context, DamageProRound);
                else
                    buff.Start(context);
            }
        }
    }

    [Preserve] public class SpellsRestrictionAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _actors;
        [Header("В раундах")]
        [SerializeField] private int _duration;
        
        [SerializeField] private bool _useProbability;
        
        [Range(0, 1)]
        [ShowIf(nameof(_useProbability))]
        [SerializeField] private float _probability = 1f;
        
        public override void Apply(IExecutorContext context)
        {
            if (_useProbability)
            {
                var rand = UnityEngine.Random.Range(0, 1f);
                //context.BattleController.DebugOutput.Print($"MovementRestrictionAction rand > {rand}");
                if(rand > _probability)
                    return;
            }
            
            base.Apply(context);

            if (_actors.TargetType == TargetTypes.Enemies || _actors.TargetType == TargetTypes.Heroes)
            {
                var buffs = BuffsManager.CreateHeroBuffsByType<SpellsRestrictionBuff>(_actors, context, _duration);
                foreach (var b in buffs)
                {
                    b.Start();
                    TrySetStatusEffect(b, context);
                }
            }
            else if(_actors.TargetType == TargetTypes.RandomEnemy)
            {
                var buff = BuffsManager.CreateHeroBuffByType<SpellsRestrictionBuff>(_actors, context, _duration);
                buff.Start();
                
                if(buff.Owner == null)
                    return;
                
                TrySetStatusEffect(buff, context);
            }
        }

        private void TrySetStatusEffect(IBuff buff, IExecutorContext context)
        {
            var spellConfig = context.Spell.Config;
            var statusEffectsExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor).StatusEffectsExecutor;
            if(!statusEffectsExecutor.IsDeferredApply)
                return;
            
            var statusIcon = spellConfig.StatusEffectIcons.IconForOthers == null
                ? spellConfig.StatusEffectIcons.IconForSelf
                : spellConfig.StatusEffectIcons.IconForOthers;
            statusEffectsExecutor.Run(context, buff.GetOwner<CharacterView>(), statusIcon);
        }
    }
    
    [Preserve] public class NoEffectsAcceptAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _actors;
        [Header("В раундах")]
        [SerializeField] private int _duration;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            var buffs = BuffsManager.CreateHeroBuffsByType<NoEffectsAcceptBuff>(_actors, context, _duration);
            foreach (var b in buffs)
                b.Start();
        }
    }
    
    [Preserve] public class NegativeEffectsRemoveAction : AbstractSpellAction
    {
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            var heroes = SpellActions.GetLivingHeroesInAoeArea(context.Spell);
            if(heroes.Count <= 0)
                return;
            context.BattleController.BattleSimulator.RemoveNegativeEffects(heroes.Select(h => h.Config.name).ToArray());
        }
    }

    [Preserve] public class InvisibilityAction : AbstractSpellAction
    {
        public enum InvisibilityModes
        {
            ForAll,
            ForEnemies
        }
        
        [SerializeField] private ITargetsSelector _actor;
        [Header("В раундах")]
        [SerializeField] private int _duration;
        
        [SerializeField] private InvisibilityModes _invisibilityMode;
        [SerializeField] private Conditions _endCondition;
        
        public bool ShowStatusesDuringInvisibility;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            var buff = BuffsManager.CreateHeroBuffByType<InvisibilityBuff>(_actor, context, _duration);
            buff.Start(_endCondition, _invisibilityMode, ShowStatusesDuringInvisibility);
        }
    }

    [Preserve] public class DamageAtAreaAction : AbstractSpellAction
    {
        private AttackData BuildAttackData(CharacterView actor, CharacterView target, PlannedSpell spell, float dmg,
            Vector3[] aoe, bool ignoreProtectiveEffects)
        {
            return new AttackData
            {
                Actor = actor.Config.name,
                ActorSpell = spell.Config.name,
                Target = target.Config.name,
                OriginDamageValue = dmg,
                Aoe = aoe,
                IgnoreProtectiveEffects = ignoreProtectiveEffects
            };
        }
        
        [SerializeField] private ITargetsSelector _targets;
        [SerializeField] private bool _usePercentOfOwnDmg;
        [SerializeField] private bool _multByNumberOfTargets;
        
        [HideIf(nameof(_usePercentOfOwnDmg))]
        [SerializeField] private RangeFloatSelector _baseDamageRange;

        [ShowIf(nameof(_usePercentOfOwnDmg))]
        [Range(0, 1f)]
        [SerializeField] private float _ownerTotalDmgPercent;
        
        [SerializeField] private bool _ignoreProtectiveEffects;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            var simulator = context.BattleController.BattleSimulator;
            
            var spell = context.Spell;
            var aoeCells = spell.AoeCellsData;
            var cellPositions = aoeCells.Select(c => c.Position).ToArray();
            AttackData attack;
            
            if (_targets.TargetType == TargetTypes.Self)
            {
                var target = _targets.GetTarget(context.Actor.Config);
                var dmg = UnityEngine.Random.Range(_baseDamageRange.Min, _baseDamageRange.Max);
                attack = BuildAttackData(context.Actor, target, spell, dmg, cellPositions, _ignoreProtectiveEffects);
                simulator.Attack(attack);
            }
            else
            {
                var targets = _targets.GetTargets(spell);
                foreach (var target in targets)
                {
                    var cell = aoeCells.FirstOrDefault(c => c.Position.Equals(target.transform.position));
                    float dmg;
                    if (_usePercentOfOwnDmg)
                        dmg = context.Actor.Model.TotalDamage * _ownerTotalDmgPercent;
                    else
                    {
                        dmg = cell.ImpactRange.x == -1
                            ? UnityEngine.Random.Range(_baseDamageRange.Min, _baseDamageRange.Max)
                            : UnityEngine.Random.Range(cell.ImpactRange.x, cell.ImpactRange.y);
                        if (_multByNumberOfTargets)
                            dmg *= targets.Count;
                    }
                    attack = BuildAttackData(context.Actor, target, spell, dmg, cellPositions, _ignoreProtectiveEffects);
                    simulator.Attack(attack);
                }
            }
        }
    }

    [Preserve] public class ActionPointsModifierAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _targets;
        [Header("В раундах")]
        [SerializeField] private int _duration;
        [SerializeField] [Range(-3, 0)]private int _actionPointsCount;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            var buffs = BuffsManager.CreateHeroBuffsByType<ActionPointsModifier>(_targets, context, _duration);
            foreach (var buff in buffs)
                buff.Start(_actionPointsCount);
        }
    }

    public class DamageModifierAction : AbstractSpellAction
    {
        public enum DamageTypes
        {
            TakenDamageReducer,
            TakenDamageMultiplier,
            DamageDoneReducer,
            DamageDoneMultiplier
        }
        
        [SerializeField] private ITargetsSelector _actors;
        [Header("В раундах")]
        [SerializeField] private int _duration;

        [SerializeField]
        private HPValueTypes ValueType;

        [SerializeField] 
        [ShowIf(nameof(ValueType), HPValueTypes.Percent)]
        [Range(0, 1)]
        private float _percent;
        
        [SerializeField]
        [HideIf(nameof(ValueType), HPValueTypes.Percent)]
        private int _value;

        [SerializeField] private DamageTypes _damageType;
        [SerializeField] private Conditions _endCondition;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            var buffs = BuffsManager.CreateHeroBuffsByType<DamageModifierBuff>(_actors, context, _duration);
            foreach (var b in buffs)
                b.Start(ValueType == HPValueTypes.Percent ? _percent : _value, ValueType, _damageType);
        }
    }

    [Preserve] public class DarkVortexAction : AbstractSpellAction
    {
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);

            var map = context.BattleController.LevelField.NavigationMap;
            var positions = context.Spell.AoeCellsData.Select(c => c.Position).ToList();
            var aoeCenter = AoeCellData.GetAoeAreaCenter(positions, 0);
            foreach (var p in positions)
            {
                var node = map.GetNodeByWorldPosition(p);
                if (node.Obstacle is CharacterView hero)
                {
                    if(hero.ContainsBuffTag(BuffsManager.BuffsTags.Hibernation))
                        continue;
                    hero.ApplyFallingDown(aoeCenter);
                }
            }
        }
    }

    [Preserve] public class ShieldAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _actors;
        [Space(10)]
        [SerializeField]
        [Range(0, 1)]
        private float _attackerDamageMultiplier;
        
        [Space(10)]
        [Header("В раундах")]
        [SerializeField] private int _duration;

        [Preserve] public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            var buffs = BuffsManager.CreateHeroBuffsByType<ShieldActionBuff>(_actors, context, _duration);
            foreach (var buff in buffs)
                buff.Start(_attackerDamageMultiplier);
        }
    }

    [Preserve] public class HibernationAction : AbstractSpellAction
    {
        [Space(10)]
        [Header("В раундах")]
        [SerializeField] private int _duration;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            var buff = BuffsManager.CreateHeroBuffByType<HibernationBuff>(context.Actor, context, _duration);
            buff.Start();
        }
    }
    
    [Preserve] public class SupressPresenceAction : AbstractSpellAction
    {
        [Space(10)]
        [Header("В раундах")]
        [SerializeField] private int _duration;
        
        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            var buff = BuffsManager.CreateHeroBuffByType<SuppressPresenceBuff>(context.Actor, context, _duration);
            buff.Start();
        }
    }

    [Preserve] public class ProtectorsDutyAction : AbstractSpellAction
    {
        [SerializeField] private ITargetsSelector _targets;
        [SerializeField] private ITargetsSelector _actor;
        [Header("В раундах")]
        [SerializeField] private int _duration;
        
        [SerializeField]
        [Range(0, 1)]
        private float _damageReduction;

        public override void Apply(IExecutorContext context)
        {
            base.Apply(context);
            
            var spellConfig = context.Spell.Config;
            var statusEffectsExecutor = ExecutorBase.GetExecutor<SpellExecutor>(spellConfig.Executor).StatusEffectsExecutor;
            
            var targetsBuffs = BuffsManager.CreateHeroBuffsByType<DamageModifierBuff>(_targets, context, _duration);
            var targetsMap = new Dictionary<CharacterView, IBuff>();
            foreach (var b in targetsBuffs)
            {
                var target = b.GetOwner<CharacterView>();
                statusEffectsExecutor.Run(context, target, spellConfig.StatusEffectIcons.IconForOthers);
                b.Start(_damageReduction, HPValueTypes.Percent, DamageModifierAction.DamageTypes.TakenDamageReducer);
                targetsMap.Add(target, b);
            }
            if(targetsMap.Count <= 0)
                return;

            statusEffectsExecutor.Run(context, context.Actor, spellConfig.StatusEffectIcons.IconForSelf);
            var actorBuff = BuffsManager.CreateHeroBuffByType<ProtectorsDutyBuff>(_actor, context, _duration);
            actorBuff.Start(targetsMap, _damageReduction);
        }
    }

    public enum HPValueTypes
    {
        Percent,
        Value,
        RandomInt,
        RandomPercent
    }

    [Serializable]
    public class RangeFloatSelector
    {
        public float Min;
        public float Max;
    }
    
    [Serializable]
    public class RangeFloatPercentSelector
    {
        [Range(0, 1)]
        public float Min;
        [Range(0, 1)]
        public float Max;
    }

    [Serializable]
    public class RangeIntSelector
    {
        public int Min;
        public int Max;
    }
}