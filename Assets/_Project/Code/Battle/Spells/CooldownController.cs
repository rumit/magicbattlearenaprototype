﻿using System;
public class CooldownController : IDisposable
{
    public int RemainRounds { get; private set; }
    public float Percent { get; private set; }
    
    private GameState _gameState;
    private SpellConfig _config;
    private int _startRoundNum;

    public CooldownController(GameState gameState, SpellConfig config)
    {
        _gameState = gameState;
        _config = config;
        _startRoundNum = gameState.CurrentRoundNumber;
        UpdateState();

        _gameState.RoundChanged += OnRoundChanged;
    }

    private void UpdateState()
    {
        var currentRoundNum = _gameState.CurrentRoundNumber;
        RemainRounds = _config.Cooldown - (currentRoundNum - _startRoundNum);
        Percent = (float)RemainRounds / _config.Cooldown;
    }

    private void OnRoundChanged()
    {
        UpdateState();
    }

    public void Dispose()
    {
        _gameState.RoundChanged -= OnRoundChanged;
    }
}
