﻿using System;
using System.Collections;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using Random = UnityEngine.Random;

public class Levitation : MonoBehaviour
{
    [SerializeField] private float _rangeY = 0.3f;
    [SerializeField] private float _rotation = 10;
    [SerializeField] private float _duration = 3f;

    private void OnDestroy()
    {
        transform.DOKill();
    }

    private IEnumerator  Start()
    {
        yield return new WaitForSeconds(Random.Range(0, 2f));
        
        var y = transform.localPosition.y + _rangeY;
         transform.DOLocalMoveY(y, _duration)
             .OnUpdate(() =>
             {
                 transform.Rotate(new Vector3(0, (transform.rotation.y + _rotation) * Time.deltaTime, 0));
             })
             .SetLoops(-1, LoopType.Yoyo)
             .SetEase(Ease.InOutSine);
    }
}
