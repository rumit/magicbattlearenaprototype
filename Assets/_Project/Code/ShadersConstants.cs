﻿
using UnityEngine;

public static class ShadersConstants
{
    public static readonly int Smoothness = Shader.PropertyToID("_Smoothness");
    public static readonly int Parallax = Shader.PropertyToID("_Parallax");
    public static readonly int BaseColor = Shader.PropertyToID("_BaseColor");
    public static readonly int Color = Shader.PropertyToID("_Color");
    public static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");
}
