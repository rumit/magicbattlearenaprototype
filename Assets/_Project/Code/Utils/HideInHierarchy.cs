﻿using System;
using UnityEngine;

public class HideInHierarchy : MonoBehaviour
{
    [SerializeField] private GameObject[] _objects;
    
    private void OnValidate()
    {

        foreach (var o in _objects)
        {
            o.hideFlags = HideFlags.HideInHierarchy;
        }
    }
}
