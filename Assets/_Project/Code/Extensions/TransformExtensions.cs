﻿using UnityEngine;

public static class TransformExtensions
{
    public static void RemoveAllChilds(this Transform transform)
    {
        if (transform.childCount <= 0) 
            return;
        for (var i = transform.childCount - 1; i >= 0 ; i--)
            GameObject.Destroy(transform.GetChild(i).gameObject);
    }
}
