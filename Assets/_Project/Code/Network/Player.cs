﻿using System.Collections.Generic;
using System.Linq;

namespace MagicBattleArena.Network
{
    public class Player
    {
        public static readonly List<Player> Players = new List<Player>(2);
        public static Player LocalPlayer => Players.FirstOrDefault(p => p.IsLocal);
        public static Player EnemyPlayer => Players.FirstOrDefault(p => !p.IsLocal);

        public Photon.Realtime.Player PhotonPlayer;
        public List<CharConfig> TeamConfigs;
        public readonly List<CharacterView> Team = new List<CharacterView>(3);

        public bool IsLocal => PhotonPlayer.IsLocal;
        public bool IsMasterClient => PhotonPlayer.IsMasterClient;
        
        public Player() { }

        public static CharacterView GetHeroById(string id)
        {
            var all = LocalPlayer.Team.Concat(EnemyPlayer.Team);
            return all.FirstOrDefault(ch => ch.Config.name == id);
        }
        
        public static CharacterView GetLocalHeroById(string id)
        {
            return LocalPlayer.Team.FirstOrDefault(ch => ch.Config.name == id);
        }
        
        public static List<CharacterView> GetLivingHeroes()
        {
            return LocalPlayer.Team.Where(ch => !ch.Model.IsDead).ToList();
        }

        public static List<CharacterView> GetLivingEnemies()
        {
            return EnemyPlayer.Team.Where(ch => !ch.Model.IsDead).ToList();
        }
        
        public static CharacterView GetLocalHeroByConfig(CharConfig config)
        {
            return LocalPlayer.Team.FirstOrDefault(ch => ch.Config == config);
        }
        
        public static List<CharacterView> GetAllHeroes()
        {
            return LocalPlayer.Team.Concat(EnemyPlayer.Team).ToList();
        }
        
        // public static bool HeroBelongsPlayer(CharacterView character)
        // {
        //     return LocalPlayer.Team.Any(h => h == character);
        // }

        public void AddTeamMember(CharacterView character)
        {
            if(!Team.Contains(character)) Team.Add(character);
        }
        
        public override string ToString()
        {
            return $"[Name: {PhotonPlayer.NickName}, BelongsPlayer: {IsLocal}, IsMasterClient: {IsMasterClient}, TeamConfigs: {string.Join(",", TeamConfigs)}]";
        }
    }
}