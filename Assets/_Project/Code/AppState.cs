﻿using System;
using Photon.Pun;
using UnityEngine;

public class AppState : MonoBehaviourPunCallbacks
{
    public static bool IsPaused;

    private void Awake()
    {
        PhotonNetwork.KeepAliveInBackground = 21600;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        IsPaused = !hasFocus;
        HandlePuseState();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        IsPaused = pauseStatus;
        HandlePuseState();
    }

    private void HandlePuseState()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
            return;
        Time.timeScale = IsPaused ? 0 : 1;
    }
}