using UnityEngine;
using Zenject;

public class ProjectContextInstaller : MonoInstaller
{
    [SerializeField] private CommonBattleConfig _commonBattleConfig;

    public override void InstallBindings()
    {
        Container.BindInstance(GetComponent<DebugLogUI>());
        Container.BindInstance(_commonBattleConfig);
    }
}

public static class Scenes
{
    public enum ScenesEnum
    {
        LobbyScene,
        BattleScene
    }
        
    public static string GetSceneName(ScenesEnum scene)
    {
        return scene.ToString();
    }
}